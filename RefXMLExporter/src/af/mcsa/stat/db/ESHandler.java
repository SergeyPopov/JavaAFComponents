package af.mcsa.stat.db;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.node.Node;


public class ESHandler {
  
	public ESHandler() {
		
	}

	
	public static Map<String, Object> putJsonDocument(String title, String content, Date postDate, 
                                                      String[] tags, String author)
       {
         Map<String, Object> jsonDocument = new HashMap<String, Object>();
         jsonDocument.put("title", title);
         jsonDocument.put("conten", content);
         jsonDocument.put("postDate", postDate);
         jsonDocument.put("tags", tags);
         jsonDocument.put("author", author);
         return jsonDocument;
    }
    
	
}
