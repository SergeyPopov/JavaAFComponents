package af.mcsa.stat.db;

import java.net.URL;
import java.net.URLEncoder;
import java.io.*;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;






import af.mcsa.stat.utils.Config;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;

import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import java.security.cert.X509Certificate;


public class ESPostHttpsClient {
	
	public  ESPostHttpsClient() {}
	private final String pathToKeyStore = "C:\\Program Files (x86)\\Java\\jre1.8.0_101\\lib\\security\\cacerts";
	private final String USER_AGENT = "Mozilla/5.0";
	

	
	static {
	    //for localhost testing only
	    javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
	    new javax.net.ssl.HostnameVerifier(){

	        public boolean verify(String hostname,
	                javax.net.ssl.SSLSession sslSession) {
	            if (hostname.equals("localhost")) {
	            	
	            	System.out.println("Host detected as " + hostname.toString());
	            			
	                return true;
	            }
	            return false;
	        }
	    });
	}
	
	public int postsslJsonObject(Object json, String esindex) throws Exception	{

		System.setProperty("javax.net.ssl.keyStore" , pathToKeyStore);
		
		System.out.println("Keystore in use " +   System.getProperty("javax.net.ssl.keyStore"));

		HttpsURLConnection.setDefaultHostnameVerifier ((hostname, session) -> true);
	    
		/* Start of Fix */
		
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() { return null; }
            public void checkClientTrusted(X509Certificate[] certs, String authType) { }
            public void checkServerTrusted(X509Certificate[] certs, String authType) { }

        } };

        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) { return true; }
        };
        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        
        /* End of the fix*/
		
		fixUntrustCertificate();
		
		String es_index = null;
		int status      = 1;
		
        String httpsURL = null;	
        if(esindex == null){
              es_index = Config.ES_MALINGINDEX;
        }else{
     	   es_index = esindex;
        }
        
     try {
    	httpsURL = Config.ES_LOCALHTTP_URL + es_index;	
		System.out.println("Connecting URL " +   httpsURL);
        URL myurl = new URL(httpsURL);
        HttpsURLConnection con = (HttpsURLConnection)myurl.openConnection();
        con.setRequestMethod("POST");
        Gson gson = new Gson();
		String jsonString = gson.toJson(json);
		
        //String jsonString =  "rubbish encoded "+URLEncoder.encode("abcd","UTF-8") ;
        con.setRequestProperty("Content-length", String.valueOf(jsonString.length())); 
        con.setRequestProperty("Content-Type","text/xml;charset=utf-8"); 
        con.setRequestProperty("Accept-Charset", "UTF-8");
        con.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0;Windows98;DigExt)"); 
        con.setDoOutput(true); 
        con.setDoInput(true); 

        DataOutputStream output = new DataOutputStream(con.getOutputStream());  

        output.writeBytes(jsonString);

        output.close();

        DataInputStream input = new DataInputStream( con.getInputStream() ); 



        for( int c = input.read(); c != -1; c = input.read() ) 
        System.out.print( (char)c ); 
        input.close(); 

        System.out.println("Resp Code:"+con .getResponseCode()); 
        System.out.println("Resp Message:"+ con .getResponseMessage()); 
        
		} catch (Exception e) {
			status = 1;
			e.printStackTrace();
        }
        return status;
     }
	

  
	public void fixUntrustCertificate() throws KeyManagementException, NoSuchAlgorithmException{

        TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }

            }
        };

        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };

        // set the  allTrusting verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
    }
	
	
	
	
	public int sendPost(Object json, String esindex) throws Exception {

		String es_index = null;
		int status      = 1;
		
        String httpsURL = null;	
        if(esindex == null){
              es_index = Config.ES_MALINGINDEX;
        }else{
     	   es_index = esindex;
        }

        httpsURL = Config.ES_LOCALHTTP_URL + es_index;	
		URL obj = new URL(httpsURL);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        Gson gson = new Gson();
		String jsonString = gson.toJson(json);

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(jsonString);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + httpsURL);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());
		
		return status;

	}


}
