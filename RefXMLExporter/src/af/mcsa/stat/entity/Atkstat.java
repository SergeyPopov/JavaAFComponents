//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.05.09 at 10:51:03 AM CEST 
//


package af.mcsa.stat.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for atkstat complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="atkstat">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="versjon" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="navn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="starttid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="stopptid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element ref="{http://www.datk.no}kontrollstrekning" maxOccurs="2" minOccurs="0"/>
 *         &lt;element ref="{http://www.datk.no}kontrollsted" maxOccurs="4" minOccurs="0"/>
 *         &lt;element ref="{http://www.datk.no}datalogger" minOccurs="0"/>
 *         &lt;element ref="{http://www.datk.no}hastighetsmåler"/>
 *         &lt;element ref="{http://www.datk.no}måling" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "atkstat", propOrder = {
    "versjon",
    "navn",
    "starttid",
    "stopptid",
    "kontrollstrekning",
    "kontrollsted",
    "datalogger",
    "hastighetsm\u00e5ler",
    "m\u00e5ling"
})
public class Atkstat {

    @XmlElement(required = true)
    protected String versjon;
    protected String navn;
    protected Date starttid;
    protected Date stopptid;
    protected List<Kontrollstrekning> kontrollstrekning;
    protected List<Kontrollsted> kontrollsted;
    protected Datalogger datalogger;
    @XmlElement(required = true)
    protected Hastighetsmaler hastighetsmaler;
	protected Maling sMaling;
	protected List<Maling> maling;

    /**
     * Gets the value of the versjon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersjon() {
        return versjon;
    }

    /**
     * Sets the value of the versjon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersjon(String value) {
        this.versjon = value;
    }

    /**
     * Gets the value of the navn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNavn() {
        return navn;
    }

    /**
     * Sets the value of the navn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNavn(String value) {
        this.navn = value;
    }

    /**
     * Gets the value of the starttid property.
     * 
     * @return
     *     possible object is
     *     {@link Date }
     *     
     */
    public Date getStarttid() {
        return starttid;
    }

    /**
     * Sets the value of the starttid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Date }
     *     
     */
    public void setStarttid(Date value) {
        this.starttid = value;
    }

    /**
     * Gets the value of the stopptid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getStopptid() {
        return stopptid;
    }

    /**
     * Sets the value of the stopptid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopptid(Date value) {
        this.stopptid = value;
    }

    /**
     * Gets the value of the kontrollstrekning property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the kontrollstrekning property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKontrollstrekning().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Kontrollstrekning }
     * 
     * 
     */
    public List<Kontrollstrekning> getKontrollstrekning() {
        if (kontrollstrekning == null) {
            kontrollstrekning = new ArrayList<Kontrollstrekning>();
        }
        return this.kontrollstrekning;
    }

    /**
     * Gets the value of the kontrollsted property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the kontrollsted property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKontrollsted().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Kontrollsted }
     * 
     * 
     */
    public List<Kontrollsted> getKontrollsted() {
        if (kontrollsted == null) {
            kontrollsted = new ArrayList<Kontrollsted>();
        }
        return this.kontrollsted;
    }

    /**
     * Gets the value of the datalogger property.
     * 
     * @return
     *     possible object is
     *     {@link Datalogger }
     *     
     */
    public Datalogger getDatalogger() {
        return datalogger;
    }

    /**
     * Sets the value of the datalogger property.
     * 
     * @param value
     *     allowed object is
     *     {@link Datalogger }
     *     
     */
    public void setDatalogger(Datalogger value) {
        this.datalogger = value;
    }

    /**
     * Gets the value of the hastighetsmåler property.
     * 
     * @return
     *     possible object is
     *     {@link Hastighetsmaler }
     *     
     */
    public Hastighetsmaler getHastighetsmaler() {
        return hastighetsmaler;
    }

    /**
     * Sets the value of the hastighetsmåler property.
     * 
     * @param value
     *     allowed object is
     *     {@link Hastighetsmaler }
     *     
     */
    public void setHastighetsmaler(Hastighetsmaler value) {
        this.hastighetsmaler = value;
    }

    public Maling getsMaling() {
    	return sMaling;
	}    
    
    
    public void setsMaling(Maling sMaling) {
    	this.sMaling = sMaling;
	}
    
    /**
     * Gets the value of the måling property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the måling property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMåling().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Maling }
     * 
     * 
     */
    public List<Maling> getMaling() {
        if (maling == null) {
            maling = new ArrayList<Maling>();
        }
        return this.maling;
    }

}
