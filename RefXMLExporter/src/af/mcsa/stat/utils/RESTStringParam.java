package af.mcsa.stat.utils;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;

public class RESTStringParam {
	
	
	public  RESTStringParam() {
	}
	
	public static BigDecimal string2BD(String str){

		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
	    symbols.setGroupingSeparator(',');
	    symbols.setDecimalSeparator('.');
	    String pattern = "#,##0.0#";	
	    BigDecimal bigDecimal = null;
	    try{  

	        DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
       	    decimalFormat.setParseBigDecimal(true);

	        // parse the string
	        bigDecimal = (BigDecimal) decimalFormat.parse(str);
	        System.out.println(bigDecimal);
	    }   catch (ParseException e) {

            e.printStackTrace();

        }
	   
	   return bigDecimal;
	}
	
		public static void main(String[] args) throws IOException, ParseException {
	        // TODO Auto-generated method stub
	        /*
			String value = "1,000,000,000.999999999999999";
	        BigDecimal money = new BigDecimal(value.replaceAll(",", ""));
	        System.out.println(money);
	        */
			DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		    symbols.setGroupingSeparator(',');
		    symbols.setDecimalSeparator('.');
		    String pattern = "#,##0.0#";	
		    BigDecimal bigDecimal;
		    DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
			decimalFormat.setParseBigDecimal(true);

			// parse the string
			bigDecimal = (BigDecimal) decimalFormat.parse("79.1");
			System.out.println(bigDecimal);
			
	    }
		
		public static String getEncodedText(String text) {
	        StringBuilder sb = new StringBuilder();

	        for (int i = 0; i < text.length(); i++) {
	            char ch = text.charAt(i);
	            int codePoint = text.codePointAt(i);

	            if (codePoint >= 0 && codePoint <= 126 
	                    && (codePoint != 34 && codePoint != 39
	                            && codePoint != 60 && codePoint != 62
	                            && codePoint != 94 && codePoint != 96 && codePoint != '_')) {
	                sb.append(ch);
	                continue;
	            }


	            sb.append(convertToEntity(String.valueOf(codePoint)));
	        }

	        return sb.toString();
	    }


	    public static String convertToEntity(String decimalValue) {
	        return "&#" + decimalValue+ ";";
	    }
		
		
	

}
