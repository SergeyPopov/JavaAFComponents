package af.mcsa.stat.utils;

import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.logging.Handler;

import af.mcsa.stat.utils.Config;

public class ATKLogger {
  static private FileHandler fileTxt;
  static private SimpleFormatter formatterTxt;
  static private String LogLevel = null;

  static public void setup(String dirtoparse) throws IOException {

    // get the global logger to configure it
    Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    // suppress the logging output to the console
    Logger rootLogger = Logger.getLogger(""); // *getLogger*("");
    Handler[] handlers = rootLogger.getHandlers();
    if (handlers[0] instanceof ConsoleHandler) {
      rootLogger.removeHandler(handlers[0]);
    }
    LogLevel = Config.LOGGING_LEVEL;
    if (LogLevel=="1"){ 
       logger.setLevel(Level.SEVERE);
    }else if(LogLevel=="2"){     
        logger.setLevel(Level.WARNING);
    }else if(LogLevel=="3"){     
        logger.setLevel(Level.INFO);
    }else if(LogLevel=="4"){     
        logger.setLevel(Level.CONFIG);
    }else if(LogLevel=="5"){     
        logger.setLevel(Level.FINE);
    }else if(LogLevel=="6"){     
         logger.setLevel(Level.FINER);
    }else if(LogLevel=="7"){     
        logger.setLevel(Level.FINEST);
    }
    fileTxt = new FileHandler(dirtoparse.trim()+Config.XMLLOG);
    // create a TXT formatter
    formatterTxt = new SimpleFormatter();
    fileTxt.setFormatter(formatterTxt);
    logger.addHandler(fileTxt);
  }
  
  public static void contextDestroyed(Logger logger) {

	   //get all handlers
	   Handler[] handlers = logger.getHandlers();
	   
	   //remove all handlers
	   for(int i = 0; i < handlers.length; i++)
	   { 
	       logger.removeHandler(handlers[i]);
	       handlers[i].close(); 
	   } 
	 
	   /*
	   //delele all the files related to the logging process
	   File logFiles = new File(root+"WEB-INF//logs//");      
	   String[] fileNames = logFiles.list(); 
	   
	   for(int i = 0; i < fileNames.length; i++)
	   {
	       File currentFile=new File(root+"WEB-INF//logs//"+fileNames[i]);
	       currentFile.delete();
	   }
	   */
	   //reset the logger

	   logger = null;
	 }
  
}
 
