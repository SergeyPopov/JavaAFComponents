package af.mcsa.stat.adapters;

import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import af.mcsa.stat.utils.Config;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;

public class JSONBuilder {

	
	public JSONBuilder() {
		
	}
	Gson gson = new Gson();
	
	public String getString(Object obj ) throws UnsupportedEncodingException{

	    	String jsonInString  =null;
	    	String jsonOutString =null;
	    	
	    	try {
	    		jsonInString = new Gson().toJson(obj);
	    		//jsonInString = gson.toJson(obj);
	    		byte[] encJsonbytes = jsonInString.getBytes(Config.XMLENCODING);
	    		jsonOutString       = new String(encJsonbytes);
	    	} catch (JsonIOException e) {
	    		
				e.printStackTrace();
			}
	    	return jsonOutString;
	    }
	
    public void record(Object obj ){
    	
    	String jsonInString;
		try {
			jsonInString = getString(obj);
	    	System.out.println(jsonInString);
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}

    	try {
			gson.toJson(obj, new FileWriter(Config.XMLDIR));
		} catch (JsonIOException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}	
    }
}
