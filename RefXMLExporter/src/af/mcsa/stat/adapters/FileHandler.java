package af.mcsa.stat.adapters;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Stream;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


import af.mcsa.stat.utils.ATKLogger;
import af.mcsa.stat.utils.Config;
import net.jodah.failsafe.CircuitBreaker;

public class FileHandler implements Runnable{
	
	CircuitBreaker breaker = new CircuitBreaker()
	  .withFailureThreshold(3, 10)
	  .withSuccessThreshold(5)
	  .withDelay(1, TimeUnit.MINUTES);

    private static Logger logger = Logger.getLogger(
            "no.svv.atkstat.adapters.FileHandler");
    
    //private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    
	public FileHandler(){}
	
	
	public List<String> getFileList(String dirstr){
		List<String> filelist = new ArrayList<String>();
		File[] files = new File(dirstr).listFiles();
		//If this pathname does not denote a directory, then listFiles() returns null. 
    	try{
    		for (File file : files) {
    			if (file.isFile()) {
    				filelist.add(file.getName());
    			}
    		}
    	}  catch (Exception e) {
	        e.printStackTrace();
    	}
    	return filelist;
    }
	
	
	public static void getFileAttr(String file){
       
    	try{
            Path path = Paths.get(file);
            BasicFileAttributes bfa = Files.readAttributes(path, BasicFileAttributes.class);       
            
            System.out.println("Creation Time      : " + bfa.creationTime());
            System.out.println("Last Access Time   : " + bfa.lastAccessTime());
            System.out.println("Last Modified Time : " + bfa.lastModifiedTime());
            System.out.println("Is Directory       : " + bfa.isDirectory());
            System.out.println("Is Other           : " + bfa.isOther());
            System.out.println("Is Regular File    : " + bfa.isRegularFile());
            System.out.println("Is Symbolic Link   : " + bfa.isSymbolicLink());
            System.out.println("Size               : " + bfa.size());
    	}  catch (Exception e) {
	        e.printStackTrace();
    	}
    }

	public static String getExtension(File file){
		   String extension = "";
		   try{
		      String fileName = file.toString();
		      int i = fileName.lastIndexOf('.');
		      if (i > 0) {
		          extension = fileName.substring(i+1);
		      }
		   } catch (Exception e) {
	 	      e.printStackTrace();
	 	 }  
		 return extension;
	} 
	
	public boolean FileExists (String inbound_dir) throws  IOException {	

		   boolean locked = false;
		   String lockfile = inbound_dir+ Config.XMLLOG + ".lck";
		   System.out.println("Checking lock at   : " + lockfile);
	    	try{
	    		
	    		File file = new File(lockfile);
	    		locked = file.exists();
	    	    if (file.exists() && file.isFile())
	    	    {
	    	    	locked = true;
	    			System.out.println("Directory locked   : " + inbound_dir);
	    	    }
	    	}  catch (Exception e) {
		        e.printStackTrace();
	    	}
	    	return locked;
	}
	
	public boolean DirExists (String inbound_dir) throws  IOException {	
		   boolean exists = false;
	    	try{
	    		File varTmpDir = new File(inbound_dir);
	    		exists = varTmpDir.exists();
	    	}  catch (Exception e) {
		        e.printStackTrace();
	    	}
	    	return exists;
	}	
	 
    public static List<String> getDir (String xmlindir) throws  IOException {	

    	String[] directories = null;
    	try {
      	    File file = new File(xmlindir);
    	      directories = file.list(new FilenameFilter() {
    	      public boolean accept(File current, String name) {
    	        return new File(current, name).isDirectory();
    	      }
    	   });
    	  System.out.println(Arrays.toString(directories));
     	 } catch (Exception e) {
	      e.printStackTrace();
	     }   	
    	 return Arrays.asList(directories);
    }
    
	public int MoveFile(String dirtoparse, String filename){
		   String extension        = "";
		   String archivedirectory = Config.XMLARCDIR;
	       InputStream inStream    = null;
	       OutputStream outStream  = null;
	       int status              = 1;
		   logger.info("Moving file to archive folder: " + archivedirectory);
		   try{
	    	    File parsedfile = new File(dirtoparse+filename);
	    	    File archfile   = new File(archivedirectory+filename);
	    	    
	    	    inStream  = new FileInputStream(parsedfile);
	    	    outStream = new FileOutputStream(archfile);
	    	    
	    	    byte[] buffer = new byte[1024];
	    		
	    	    int length;
	    	    //copy the file content in bytes 
	    	    while ((length = inStream.read(buffer)) > 0){
	    	    	outStream.write(buffer, 0, length);
	    	    }
	    	    inStream.close();
	    	    outStream.close();
	    	    
	    	    status = 0;
			   
		   } catch (Exception e) {
			  logger.severe("Unable to move file to archive folder: " + e.getMessage());
	 	      e.printStackTrace();
	 	 }  
		 return status;
	} 
    
	public int DeleteFile(String fullpathtoparse){
	       int status = 1;
		   logger.info("Deleting file: " + fullpathtoparse);
		   try{
	    	    File parsedfile = new File(fullpathtoparse);
	    	    if(parsedfile.delete()){
		    	    status = 0;
					System.out.println("Deleted ATK file  : " + fullpathtoparse);	    	    	
	    	    }else{
	    	       status = 1;
	  			   System.out.println("problem with deleting ATK file");
	    	    }
		   } catch (Exception e) {
			  System.out.println("problem with deleting ATK file  : " + e.getMessage());
			  logger.severe("Unable delete file: " + e.getMessage());
	 	      e.printStackTrace();
	 	 }  
		 return status;
	} 
	
	
   public List<String> getATKFilesfromDir (String inbound_dir) throws  IOException {	

    	String xml_ext    = Config.XML_EXT;
   	    String extension = null;
   	    
    	List<String> filelist = new ArrayList<String>();
		//If this pathname does not denote a directory, then listFiles() returns null. 
    	try{
    		File[] files = new File(inbound_dir).listFiles();
    		for (File file : files) {
    			if (file.isFile()) {
    				
    				extension = getExtension(file);
    				if (extension.equalsIgnoreCase(xml_ext)){
    				   filelist.add(file.getName());
    				  System.out.println("ATK file  : " + file);
    				  getFileAttr(file.toString());
    				}
    			}
    		}
    	}  catch (Exception e) {
	        e.printStackTrace();
    	}
    	return filelist;
    }

   public int processFilesInDir(String inbound_dir) throws IOException{
	   	logger.info("TEST: import new Inbound XML files ("+inbound_dir+") to ES");
	   	String dirtoparse   = null; 
	   	int status          = 0;
	   	System.out.println("Building filelist for "+"."+Config.XML_EXT);
	   	
	   	if (inbound_dir == null){
	   		dirtoparse = Config.XMLDIR;
	   	}else{
	   		dirtoparse = inbound_dir;
	   	}
  
		File currentDir = new File(dirtoparse);
		File[] files = currentDir.listFiles(new FilenameFilter() {
			    public boolean accept(File currentDir, String name) {
			       // return name.toLowerCase().endsWith("."+Config.XML_EXT);
			    	return name.endsWith("."+Config.XML_EXT);
			    }
		 });
		System.out.println("Got filelist:"+files.length);
		
	   	 //File[] files = currentDir.listFiles();
	     //try{
	    	//System.out.println("Parsing filelist...");
	   	    Stream.of(files)
    		        //.parallel()
    		        //.forEach(XMLFileParser::XMLParser);   
	   	            .forEach( file ->{
	   	            	try{
	   	                     XMLFileParser.XMLParser(file);     
    	 }
	   	 catch (org.xmlpull.v1.XmlPullParserException ep) {
	   	    logger.severe("Error during ATK XML Data Import: " + ep.getMessage());
	   	             }
         catch (IOException ex) {
        	 logger.severe("Error during ATK XML Data Import: " + ex.getMessage());
         }
	   });          	
	   	 /*           	
         catch (Exception e) {
	                 logger.severe("Error during ATK XML Data Import: " + e.getMessage());
	                 status    = 1;
	                 e.printStackTrace();
	     }
	     */
	      return status;
   }       

   
   public int parseFilesInDir(String inbound_dir) {

	   	logger.info("TEST: import new Inbound XML files ("+inbound_dir+") to ES");
	   	String fullpathtoparse    = null; 
	   	String dirtoparse         = null; 
	   	FileHandler  filehandler  = new FileHandler();
	   	int status =1;
	   	System.out.println("Building filelist...");
	   	
	   	if (inbound_dir == null){
	   		dirtoparse = Config.XMLDIR;
	   	}else{
	   		dirtoparse = inbound_dir;
	   	}
	   	
	   	try {
	   		//check for lock - currently running parser for this directory  
	   	     if (!FileExists(dirtoparse)){
	             // get filelist from this directory  	   		   
	   		    List<String> atkdatafiles = filehandler.getATKFilesfromDir(inbound_dir);
	            if(atkdatafiles.size() > 0) {
	            	logger.info("Identified " + atkdatafiles.size() + " new ATK XML files. Parsing individually");
	            
	         		for (String filename : atkdatafiles) {

	       				System.out.println("ATK file  : " + filename);
	       				
	       				fullpathtoparse = dirtoparse+filename;
	       				
	       				status = XMLFileParser.XMLExporter(fullpathtoparse, Config.OBJPARSINGPATTERN);
	       				
	       				//move file into archive folder
	       				status = MoveFile(dirtoparse, filename);

	       				if (status ==0){
		       				System.out.println("Deleting ATK file  : " + filename);
	       					status = DeleteFile(fullpathtoparse);
	       				}else{
	       					System.out.println("problem with deleting ATK file  : " + filename);
	       				}

	       		}
	            
	            } else {
	            	logger.info("No new ATK XML files to import");
	            }
	   	      }else {
	          	logger.info("Parser instance is already running for this directory");
	          }	            
	            
	        } catch (Exception e) {
	            logger.severe("Error during ATK XML Data Import: " + e.getMessage());
	            e.printStackTrace();
	        }
	        logger.info("Finished ATK XML Data Import");
	        System.out.println("");
			return status;
		}
   
   public int parseFilesInSubDir() {
   	
   	logger.info("TEST: import new ATK " + Config.XML_EXT + " files to ES");
   	int status      = 1;
    String fullpathtoparse     = null; 
   	FileHandler  filehandler   = new FileHandler();
    
   	try {
      		 List<String> atkdatafolders = filehandler.getDir(Config.XMLDIR);

            if(atkdatafolders.size() > 0) {
            	logger.info("Identified " + atkdatafolders.size() + " new ATK XML files. Parsing individually");
            
         		for (String filename : atkdatafolders) {

         			logger.info("ATK file  : " + filename + "in "+ Config.XMLDIR);
       				
       				fullpathtoparse = Config.XMLDIR+filename;        				
       				//status = filehandler.parseFilesInDir(fullpathtoparse);
                	status = filehandler.processFilesInDir(fullpathtoparse);
 
   					if (status != 0) {
   						throw new RuntimeException("Failed to process ATK XML: " + filename);
   					}else{
   						System.out.println("ATK file processed OK : " + filename);
   						//Config.setParams(filename);
   					}
       		}
            
            } else {
            	logger.info("No new ATK XML files to import");
            }
            
            
        } catch (Exception e) {
            logger.severe("Error during ATK XML Data Import: " + e.getMessage());
            e.printStackTrace();
        }
        logger.info("Finished ATK XML Data Import");
        System.out.println("");
		return status;
	}
   
   public void parseData(String inbound_dir) {
	
	Config.getParams();  
	logger.info("Import new ATK XML files ("+Config.XML_EXT+") to ES");
	 
	String dirtoparse = null;   
	if (inbound_dir == null){
	   	dirtoparse = Config.XMLDIR;
	}else{
	  	dirtoparse = inbound_dir;
	}  

   	int status      = 1;
   	FileHandler  filehandler   = new FileHandler();
    	try {

            if(Boolean.valueOf(Config.XML_PARSESUBDIR)) {
            	logger.info("Parsing with subdirectories ");
            	status = filehandler.parseFilesInSubDir();
            
            } else {
            	logger.info("parsing sindle directory: " + dirtoparse);
            	status = filehandler.processFilesInDir(dirtoparse);
            	//status = filehandler.parseFilesInDir(dirtoparse); 
            }
            
        } catch (Exception e) {
            logger.severe("Error during ATK XML Data Import: " + e.getMessage());
            e.printStackTrace();
        }
        logger.info("Finished ATK XML Data Import");
        System.out.println("");
		
	}
   
    public void run() {
    	breaker.open();
    	breaker.halfOpen();
    	breaker.close();
    	FileHandler  filehandler = new FileHandler();
    	if (breaker.allowsExecution()) {
     	   try {
              filehandler.parseData(null);
              breaker.recordSuccess();
           } catch (Exception e) {
             logger.severe("Critical error, cannot procede with XML Data Import");  
             e.printStackTrace();
             breaker.recordFailure(e);
           }
    	}  
	}
   
    public static void main(String[] args) {
		Config.getParams();
		System.out.println("Scaning with frequency " + Config.XMLPARSER_TIMEOUT);
        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(new FileHandler(), 0, Config.XMLPARSER_TIMEOUT, TimeUnit.SECONDS);
    }
           
	   
}
