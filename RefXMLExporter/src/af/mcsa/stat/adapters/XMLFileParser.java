package af.mcsa.stat.adapters;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.fest.reflect.core.Reflection;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import af.mcsa.stat.db.ConnectionManager;
import af.mcsa.stat.db.ESPostHttpClient;
import af.mcsa.stat.db.ESPostHttpsClient;
import af.mcsa.stat.db.PersistencyHelper;
import af.mcsa.stat.db.PersistencyManager;
import af.mcsa.stat.entity.Akseldata;
import af.mcsa.stat.entity.Atkstat;
import af.mcsa.stat.entity.Case;
import af.mcsa.stat.entity.Datalogger;
import af.mcsa.stat.entity.Felt;
import af.mcsa.stat.entity.Hastighetsmaler;
import af.mcsa.stat.entity.Kontrollsted;
import af.mcsa.stat.entity.Kontrollstrekning;
import af.mcsa.stat.entity.Maling;
import af.mcsa.stat.entity.ObjectFactory;
import af.mcsa.stat.entity.Vegident;
import af.mcsa.stat.utils.*;

import com.google.gson.annotations.SerializedName;
import com.stanfy.gsonxml.GsonXml;
import com.stanfy.gsonxml.GsonXmlBuilder;
import com.stanfy.gsonxml.XmlParserCreator;
import com.stanfy.gsonxml.XmlReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlElement;

public class XMLFileParser {

	public XMLFileParser(){}
	
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public static boolean initiated = false;
    
    public static boolean isInitiated() {
		return initiated;
	}

	public static void setInitiated(boolean initiated) {
		XMLFileParser.initiated = initiated;
	}
      static XmlPullParser xpp; // = factory.newPullParser();
	
	  /** Default parser creator. */
	
	  public static final XmlParserCreator PARSER_CREATOR = new XmlParserCreator() {
	    //@Override
	    public XmlPullParser createParser() {
	      try {
	        return Reflection.staticMethod("newPullParser")
	            .withReturnType(XmlPullParser.class)
	            .in(Class.forName("no.svv.atkstat"))
	            .invoke();
	      } catch (final Exception ignored) {
	        // it's not Android
	      }

	      try {
	        final XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
	        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, true);
	        return parser;
	      } catch (final Exception e) {
	        throw new RuntimeException(e);
	      }
	    }
	  };

	  static GsonXml createGson() {
	    return createGson(false);
	  }
	  static GsonXml createGson(final boolean namespaces) {
	    return new GsonXmlBuilder().setXmlParserCreator(PARSER_CREATOR).setTreatNamespaces(namespaces).create();
	  }
	
	// Individual Objects parsers 
	
	public static Akseldata parseAkseldata(boolean insideAkseldata) throws XmlPullParserException, IOException{
		
		Akseldata akseldata = new Akseldata();
		int eventType = xpp.getEventType();
		String aksexmlldata = null;
		
		while (insideAkseldata) { 
			if (eventType == XmlPullParser.START_TAG) {

	             if (xpp.getName().equalsIgnoreCase("kontrollsted")) {
	            	 	if (insideAkseldata){
	            	 		aksexmlldata =xpp.nextText();
	            	 		akseldata.setKontrollsted(new BigInteger(aksexmlldata));
	            	 		System.out.println("Akseldata kontrollsted: "+aksexmlldata);
	            	 	}
	             }			
	             if (xpp.getName().equalsIgnoreCase("nummer")) {
	            	 	if (insideAkseldata){
	            	 		aksexmlldata =xpp.nextText();
	            	 		akseldata.setNummer(new BigInteger(aksexmlldata));
	            	 		System.out.println("Akseldata nummer: "+aksexmlldata);
	            	 	}
	             }						
	             if (xpp.getName().equalsIgnoreCase("hastighet")) {
	            	 	if (insideAkseldata){
	            	 		aksexmlldata =xpp.nextText();
	            	 		akseldata.setHastighet(RESTStringParam.string2BD(aksexmlldata));
	            	 		System.out.println("Akseldata hastighet: "+aksexmlldata);
	            	 	}
	             }	
	        
	             if (xpp.getName().equalsIgnoreCase("vekt")) {
	            	 	if (insideAkseldata){
	            	 		aksexmlldata =xpp.nextText();
	            	 		akseldata.setVekt(RESTStringParam.string2BD(aksexmlldata));
	            	 		System.out.println("Akseldata vekt: "+aksexmlldata);
	            	 	}
	             }	
	             if (xpp.getName().equalsIgnoreCase("avstand")) {
	            	 	if (insideAkseldata){
	            	 		aksexmlldata =xpp.nextText();
	            	 		akseldata.setAvstand(RESTStringParam.string2BD(aksexmlldata));
	            	 		System.out.println("Akseldata avstand: "+aksexmlldata);
	            	 	}
	             }	             
			} 
		    else if (eventType == XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase("akseldata")) {
		    	System.out.println("Akseldata breaking ---------------- ");
		    	  insideAkseldata = false;
	 	    	  break;
		    }
		    //move to next element
		    eventType = xpp.next();
			
		}
		return akseldata;
	}
	/* ------------------------------------
	 * parseFelt parsing
	 * ------------------------------------
     protected BigInteger feltnummer;
     protected String navn;
     protected String beskrivelse;

	 */
	public static Felt parseFelt( boolean insideFelt) throws XmlPullParserException, IOException{
		
		Felt felt = new Felt();
		int eventType = xpp.getEventType();
		String feltdata = null;
		
		while (insideFelt) { 
			if (eventType == XmlPullParser.START_TAG) {

	             if (xpp.getName().equalsIgnoreCase("feltnummer")) {
	            	 	if (insideFelt){
	            	 		feltdata =xpp.nextText();
	            	 		felt.setFeltnummer(new BigInteger(feltdata));
	            	 		System.out.println("Felt feltnummer: "+feltdata);
	            	 	}
	             }			
	             if (xpp.getName().equalsIgnoreCase("navn")) {
	            	 	if (insideFelt){
	            	 		feltdata =xpp.nextText();
	            	 		felt.setNavn(feltdata);
	            	 		System.out.println("Felt navn: "+feltdata);
	            	 	}
	             }						
	             if (xpp.getName().equalsIgnoreCase("beskrivelse")) {
	            	 	if (insideFelt){
	            	 		feltdata =xpp.nextText();
	            	 		felt.setBeskrivelse(feltdata);
	            	 		System.out.println("Felt nummer: "+feltdata);
	            	 	}
	             }	
            
			} 
		    else if (eventType == XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase("felt")) {
		    	System.out.println("Felt breaking ---------------- ");
		    	insideFelt = false;
	 	    	break;
		    }
		    //move to next element
		    eventType = xpp.next();
			
		}
		return felt;
	}
	
	/* ------------------------------------
	 * parseVegident parsing
	 * ------------------------------------
    "fylke",
    "kategori",
    "nummer",
    "hp",
    "metertall"
	 */
	public static Vegident parseVegident( boolean insideVegident  ) throws XmlPullParserException, IOException{
		
		Vegident vegident = new Vegident();
		int eventType = xpp.getEventType();
		String vegidentdata = null;
		
		while (insideVegident) { 
			if (eventType == XmlPullParser.START_TAG) {

	             if (xpp.getName().equalsIgnoreCase("fylke")) {
	            	 	if (insideVegident){
	            	 		vegidentdata =xpp.nextText();
	            	 		vegident.setFylke(vegidentdata);
	            	 		System.out.println("Vegident fylke: "+vegidentdata);
	            	 	}
	             }			
	             if (xpp.getName().equalsIgnoreCase("kategori")) {
	            	 	if (insideVegident){
	            	 		vegidentdata =xpp.nextText();
	            	 		vegident.setKategori(vegidentdata);
	            	 		System.out.println("Vegident kategori: "+vegidentdata);
	            	 	}
	             }						
	             if (xpp.getName().equalsIgnoreCase("nummer")) {
	            	 	if (insideVegident){
	            	 		vegidentdata =xpp.nextText();
	            	 		vegident.setNummer(new BigInteger(vegidentdata));
	            	 		System.out.println("Vegident nummer: "+vegidentdata);
	            	 	}
	             }	
	        
	             if (xpp.getName().equalsIgnoreCase("hp")) {
	            	 	if (insideVegident){
	            	 		vegidentdata =xpp.nextText();
	            	 		vegident.setHp(new BigInteger(vegidentdata));
	            	 		System.out.println("Vegident vekt: "+vegidentdata);
	            	 	}
	             }	
	             if (xpp.getName().equalsIgnoreCase("metertall")) {
	            	 	if (insideVegident){
	            	 		vegidentdata =xpp.nextText();
	            	 		vegident.setMetertall(new BigInteger(vegidentdata));
	            	 		System.out.println("Vegident metertall: "+vegidentdata);
	            	 	}
	             }	             
			} 
		    else if (eventType == XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase("vegident")) {
		    	System.out.println("Vegident breaking ---------------- ");
		    	insideVegident = false;
	 	    	  break;
		    }
		    //move to next element
		    eventType = xpp.next();
			
		}
		return vegident;
	}
	
	/* ------------------------------------
	 * parseKontrollsted parsing
	 * ------------------------------------
	"nummer",
    "navn",
    "fartsgrense",
    "vegident",
    "felt"
	 */
	
	public static Kontrollsted parseKontrollsted( boolean insideKSted  ) throws XmlPullParserException, IOException{
		
		Kontrollsted kontrollsted = new Kontrollsted();
		Vegident vegident         = new Vegident();
		Felt     felt             = new Felt();	

		boolean insideVegident    = false;
		boolean insideFelt        = false;
		int eventType = xpp.getEventType();
		String kstedxmldata = null;
		
		while (insideKSted) { 
			if (eventType == XmlPullParser.START_TAG) {

	             if (xpp.getName().equalsIgnoreCase("nummer")) {
	            	 	if (insideKSted){
	            	 		kstedxmldata =xpp.nextText();
	            	 		kontrollsted.setNummer(new BigInteger(kstedxmldata));
	            	 		System.out.println("Kontrollsted kontrollsted: "+kstedxmldata);
	            	 	}
	             }			
	             if (xpp.getName().equalsIgnoreCase("navn")) {
	            	 	if (insideKSted){
	            	 		kstedxmldata =xpp.nextText();
	            	 		kontrollsted.setNavn(kstedxmldata);
	            	 		System.out.println("Kontrollsted navn: "+kstedxmldata);
	            	 	}
	             }						
	             if (xpp.getName().equalsIgnoreCase("fartsgrense")) {
	            	 	if (insideKSted){
	            	 		kstedxmldata =xpp.nextText();
	            	 		kontrollsted.setFartsgrense(new BigInteger(kstedxmldata));
	            	 		System.out.println("Kontrollsted fartsgrense: "+kstedxmldata);
	            	 	}
	             }	
	             
	             //---------------parsing vegident
	             if (xpp.getName().equalsIgnoreCase("vegident")) {
	            	 	if (insideKSted){
	            	 		insideVegident = true;      	
	            	 		vegident = parseVegident( insideVegident);
	            	 		kontrollsted.setVegident(vegident);
	            	 	}
	             }
	             
	           //--------------- parsing felt array
	             
	             if (xpp.getName().equalsIgnoreCase("felt")) {
	            	 	if (insideKSted){
	            	 		insideFelt = true;      	
	            	 		felt = parseFelt(insideFelt);
	            	 		kontrollsted.getFelt().add(felt);
	            	 		
	            	 	}
	             }	
	                          
			} 
		    else if (eventType == XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase("kontrollsted")) {
		    	System.out.println("Kontrollsted breaking ---------------- ");
		    	insideKSted = false;
	 	    	break;
		    }
		    //move to next element
		    eventType = xpp.next();
			
		}
		System.out.println("Printing  kontrollsted JSON ");

		return kontrollsted;
	}

	/* ------------------------------------
	 * parseKontrollsted parsing
	 * ------------------------------------
	"nummer",
    "navn",
    "fartsgrense",
    "vegident",
    "felt"
	 */
	
	public static Kontrollstrekning parseKontrollstrekning(boolean insideKStrek) throws XmlPullParserException, IOException{
		
		Kontrollstrekning kontrollstrekning = new Kontrollstrekning();
		Vegident vegident         = new Vegident();

		boolean insideVegident    = false;
		int eventType = xpp.getEventType();
		String kstrekxmldata = null;
		
		while (insideKStrek) { 
			if (eventType == XmlPullParser.START_TAG) {

	             if (xpp.getName().equalsIgnoreCase("nummer")) {
	            	 	if (insideKStrek){
	            	 		kstrekxmldata =xpp.nextText();
	            	 		kontrollstrekning.setNummer(new BigInteger(kstrekxmldata));
	            	 		System.out.println("Kontrollstrekning nummer: "+kstrekxmldata);
	            	 	}
	             }			
	             if (xpp.getName().equalsIgnoreCase("navn")) {
	            	 	if (insideKStrek){
	            	 		kstrekxmldata =xpp.nextText();
	            	 		kontrollstrekning.setNavn(kstrekxmldata);
	            	 		System.out.println("Kontrollstrekning navn: "+kstrekxmldata);
	            	 	}
	             }	
	             if (xpp.getName().equalsIgnoreCase("lengde")) {
	            	 	if (insideKStrek){
	            	 		kstrekxmldata =xpp.nextText();
	            	 		kontrollstrekning.setLengde(new BigInteger(kstrekxmldata));
	            	 		System.out.println("Kontrollstrekning lengde: "+kstrekxmldata);
	            	 	}
	             }	
	             if (xpp.getName().equalsIgnoreCase("måletype")) {
	            	 	if (insideKStrek){
	            	 		kstrekxmldata =xpp.nextText();
	            	 		kontrollstrekning.setMaletype(kstrekxmldata);
	            	 		System.out.println("Kontrollstrekning maletype: "+kstrekxmldata);
	            	 	}
	             }	
	             if (xpp.getName().equalsIgnoreCase("fartsgrense")) {
	            	 	if (insideKStrek){
	            	 		kstrekxmldata =xpp.nextText();
	            	 		kontrollstrekning.setFartsgrense(new BigInteger(kstrekxmldata));
	            	 		System.out.println("Kontrollstrekning fartsgrense: "+kstrekxmldata);
	            	 	}
	             }	
	             if (xpp.getName().equalsIgnoreCase("ankomstpunkt")) {
	            	 	if (insideKStrek){
	            	 		kstrekxmldata =xpp.nextText();
	            	 		kontrollstrekning.setAnkomstpunkt(new BigInteger(kstrekxmldata));
	            	 		System.out.println("Kontrollstrekning ankomstpunkt: "+kstrekxmldata);
	            	 	}
	             }
	             if (xpp.getName().equalsIgnoreCase("beregningspunkt")) {
	            	 	if (insideKStrek){
	            	 		kstrekxmldata =xpp.nextText();
	            	 		kontrollstrekning.setBeregningspunkt(new BigInteger(kstrekxmldata));
	            	 		System.out.println("Kontrollstrekning beregningspunkt: "+kstrekxmldata);
	            	 	}
	             }	
	             if (xpp.getName().equalsIgnoreCase("deteksjonsgrense")) {
	            	 	if (insideKStrek){
	            	 		kstrekxmldata =xpp.nextText();
	            	 		kontrollstrekning.setDeteksjonsgrense(new BigInteger(kstrekxmldata));
	            	 		System.out.println("Kontrollstrekning deteksjonsgrense: "+kstrekxmldata);
	            	 	}
	             }
	             //---------------parsing vegident
	             if (xpp.getName().equalsIgnoreCase("vegident")) {
	            	 	if (insideKStrek){
	            	 		insideVegident = true;      	
	            	 		vegident = parseVegident( insideVegident);
	            	 		kontrollstrekning.setVegident(vegident);
	            	 	}
	             }
			} 
		    else if (eventType == XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase("kontrollstrekning")) {
		    	System.out.println("Kontrollstrekning breaking ---------------- ");
		    	insideKStrek = false;
	 	    	break;
		    }
		    //move to next element
		    eventType = xpp.next();
			
		}
		System.out.println("Printing  kontrollsted JSON ");

		return kontrollstrekning;
	}

	
	/* ------------------------------------
	 * parseKontrollsted parsing
	 * ------------------------------------
     "serienummer",
     "type",
     "versjon"
	 */
	
	public static Datalogger parseDatalogger( boolean insideDlogger  ) throws XmlPullParserException, IOException{
		
		Datalogger datalogger = new Datalogger();
		
		int eventType = xpp.getEventType();
		String dloggxmldata = null;
		
		while (insideDlogger) { 
			if (eventType == XmlPullParser.START_TAG) {

	             if (xpp.getName().equalsIgnoreCase("serienummer")) {
	            	 	if (insideDlogger){
	            	 		dloggxmldata =xpp.nextText();
	            	 		datalogger.setSerienummer(new BigInteger(dloggxmldata));
	            	 		System.out.println("Datalogger serienummer: "+dloggxmldata);
	            	 	}
	             }			
	             if (xpp.getName().equalsIgnoreCase("type")) {
	            	 	if (insideDlogger){
	            	 		dloggxmldata =xpp.nextText();
	            	 		datalogger.setType(dloggxmldata);
	            	 		System.out.println("Datalogger type: "+dloggxmldata);
	            	 	}
	             }						
	             if (xpp.getName().equalsIgnoreCase("versjon")) {
	            	 	if (insideDlogger){
	            	 		dloggxmldata =xpp.nextText();
	            	 		datalogger.setVersjon(dloggxmldata);
	            	 		System.out.println("Datalogger versjon: "+dloggxmldata);
	            	 	}
	             }	
	      
			} 
		    else if (eventType == XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase("datalogger")) {
		    	System.out.println("Datalogger breaking ---------------- ");
		    	insideDlogger = false;
	 	    	break;
		    }
		    //move to next element
		    eventType = xpp.next();
			
		}
		System.out.println("Printing  Datalogger JSON ");

		return datalogger;
	}

	/* ------------------------------------
	 * parseHastighetsmaler parsing
	 * ------------------------------------
     "serienummer",
     "type",
     "versjon",
     "programvare"
	 */
	
	public static Hastighetsmaler parseHastighetsmaler(boolean insideHastmaler) throws XmlPullParserException, IOException{
		
		Hastighetsmaler hastighetsmaler = new Hastighetsmaler();
		
		int eventType = xpp.getEventType();
		String hmalerxmldata = null;
		
		while (insideHastmaler) { 
			if (eventType == XmlPullParser.START_TAG) {

	             if (xpp.getName().equalsIgnoreCase("serienummer")) {
	            	 	if (insideHastmaler){
	            	 		hmalerxmldata =xpp.nextText();
	            	 		hastighetsmaler.setSerienummer(new BigInteger(hmalerxmldata));
	            	 		System.out.println("Kontrollsted serienummer: "+hmalerxmldata);
	            	 	}
	             }			
	             if (xpp.getName().equalsIgnoreCase("type")) {
	            	 	if (insideHastmaler){
	            	 		hmalerxmldata =xpp.nextText();
	            	 		hastighetsmaler.setType(hmalerxmldata);
	            	 		System.out.println("Kontrollsted type: "+hmalerxmldata);
	            	 	}
	             }						
	             if (xpp.getName().equalsIgnoreCase("versjon")) {
	            	 	if (insideHastmaler){
	            	 		hmalerxmldata =xpp.nextText();
	            	 		hastighetsmaler.setVersjon(hmalerxmldata);
	            	 		System.out.println("Kontrollsted versjon: "+hmalerxmldata);
	            	 	}
	             }	
	             if (xpp.getName().equalsIgnoreCase("programvare")) {
	            	 	if (insideHastmaler){
	            	 		hmalerxmldata =xpp.nextText();
	            	 		hastighetsmaler.setProgramvare(hmalerxmldata);
	            	 		System.out.println("Kontrollsted versjon: "+hmalerxmldata);
	            	 	}
	             }	
	      
			} 
		    else if (eventType == XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase("hastighetsmåler")) {
		    	System.out.println("Hastighetsmaler breaking ---------------- ");
		    	insideHastmaler = false;
	 	    	break;
		    }
		    //move to next element
		    eventType = xpp.next();
			
		}
		System.out.println("Hastighetsmaler  datalogger JSON ");

		return hastighetsmaler;
	}
	
    /*	
	  Parsing Core Atkstat XML object, from root
	  
	      "versjon",
          "navn",
    	  "starttid",
    	  "stopptid",
    	  ---------------------
    	  "kontrollstrekning",
    	  "kontrollsted",
    	  "datalogger",
    	  "hastighetsm\u00e5ler",
    	  "m\u00e5ling"
	*/
	
	public static Atkstat parseAtkstat( boolean insideAtkstat) throws XmlPullParserException, IOException{
	     boolean insideAkseldata = false;
	     boolean insideKSted     = false;
		 

         boolean insideDlogger   = false;
         boolean insideHastmaler = false;
	     
	     String atkstatdata      = null;
		 
		 //private static Logger logger = Logger.getLogger(
		 //           "com.acando.atk.adapter.XMLFileReader");
	     
	     Atkstat atkstat = new Atkstat();
	     //Kontrollsted kontrollsted = new Kontrollsted();
		 
		 RESTDateParam rdp = new RESTDateParam();

		 
		 int eventType = xpp.getEventType();

		 System.out.println("Start Atkstat outside------------------------------------------- "+eventType);
		 
		 while (insideAtkstat) { 
			 
			 if (eventType == XmlPullParser.START_TAG) {
			 
	             if (xpp.getName().equalsIgnoreCase("versjon")) {
	            	 	if (insideAtkstat){
	            	 		atkstatdata =xpp.nextText();
	            	 		atkstat.setVersjon(atkstatdata);
	            	 		System.out.println("Atkstat versjon: "+atkstatdata);
	            	 	}
	             }	 	
	   	         else if (xpp.getName().equalsIgnoreCase("navn")) {
		            	 	if (insideAtkstat){
		            	 		atkstatdata =xpp.nextText();
		            	 		atkstat.setNavn(atkstatdata);
		            	 		System.out.println("Atkstat navn: "+atkstatdata);
		            	 	}
			     }
	             else if (xpp.getName().equalsIgnoreCase("starttid")) {
	            	 	if (insideAtkstat){
	            	 		atkstatdata =xpp.nextText();
	            	 		atkstat.setStarttid(rdp.RESTTimestampWithTZDParam(atkstatdata, Config.DATE_FORMAT));
	            	 		System.out.println("Atkstat Dato: "+atkstatdata);
	            	 	}
	             }
	             else if (xpp.getName().equalsIgnoreCase("stopptid")) {
	            	 	if (insideAtkstat){
	            	 		atkstatdata =xpp.nextText();
	            	 		atkstat.setStopptid(rdp.RESTTimestampWithTZDParam(atkstatdata, Config.DATE_FORMAT));
	            	 		System.out.println("Atkstat Dato: "+atkstatdata);
	            	 	}
	             }
	             //------------------------------------------------------------------
	             //------------------ kontrollsted
	             else if (xpp.getName().equalsIgnoreCase("kontrollsted") && eventType == XmlPullParser.START_TAG) {
	            	 	if (insideAtkstat){
	            	 		insideKSted = true; 
	            	 		Kontrollsted kontrollsted = new Kontrollsted();
	            	 		kontrollsted = parseKontrollsted( insideKSted);
	            	 		atkstat.getKontrollsted().add(kontrollsted);
	            	 	}
		    	}
	            //------------------ datalogger
	             else if (xpp.getName().equalsIgnoreCase("datalogger") && eventType == XmlPullParser.START_TAG) {
	         	 	if (insideAtkstat){
	         	 		insideDlogger = true; 
	         	 		Datalogger datalogger = new Datalogger();
	         	 		datalogger = parseDatalogger(insideDlogger);
	         	 		atkstat.setDatalogger(datalogger);
	        	 		
	         	 	}
		    	}               
	                
	            //------------------ hastighetsmåler
	             else if (xpp.getName().equalsIgnoreCase("hastighetsmåler") && eventType == XmlPullParser.START_TAG) {
	         	 	if (insideAtkstat){
	         	 		insideHastmaler = true; 
	         	 		Hastighetsmaler hastighetsmaler = new Hastighetsmaler();
	         	 		hastighetsmaler = parseHastighetsmaler(insideHastmaler);
	         	 		atkstat.setHastighetsmaler(hastighetsmaler);
	         	 		insideAtkstat = false;
	         	 	}
		    	}     
		    	//-----------------------------------------------------------------------------------------------

			} 
		    else if (eventType == XmlPullParser.START_TAG && xpp.getName().equalsIgnoreCase("måling")) {
		     	  System.out.println("Start Atkstat breaking------------------------------------------- "+eventType);
		    	  insideAtkstat = false;
	 	    	  //break;
	 	    	
		    }
		    //move to next element
		    eventType = xpp.next();
		 } 
		 System.out.println("atkstat JSON");
	     //JSONBuilder jb = new JSONBuilder();
	     //jb.record(atkstat); 

		return atkstat;
	}
	
    /*	
	  Parsing Maling XML object, sibling
	*/
	public static Maling parseMaling( boolean insideMaling  ) throws Exception{
     boolean insideAkseldata = false;
	 
	 //private static Logger logger = Logger.getLogger(
	 //           "com.acando.atk.adapter.XMLFileReader");
     
	 Maling maling = new Maling();
	 List<Akseldata> aksellist = new ArrayList<Akseldata>();
	 
	 RESTDateParam rdp = new RESTDateParam();
	 ESPostHttpsClient eshc = new ESPostHttpsClient();	 
	
	 int eventType = xpp.getEventType();
	 String malingdata = null;
	 System.out.println("Start Maling outside------------------------------------------- "+eventType);
	 
	 while (insideMaling) { 
		 
		 
		 if (eventType == XmlPullParser.START_TAG) {
             //Log.v("ENTER", String.valueOf(xpp.getEventType()));
             //System.out.println("Start Maling Inside --------------"+eventType+"----------------------------- "+xpp.getName());
		 
             if (xpp.getName().equalsIgnoreCase("dato")) {
            	 	if (insideMaling){
            	 		malingdata =xpp.nextText();
            	 		//maling.setDato(rdp.RESTTimestampWithTZDParam(malingdata, Config.DATE_FORMAT));
            	 		//maling.setDato(rdp.RESTXMLGregorianCalendar(malingdata));
            	 		maling.setDato(rdp.RESTMillsinseEpoch(malingdata));
            	 		
            	 		System.out.println("Maling Dato: "+malingdata);
            	 	}
             }
             else if (xpp.getName().equalsIgnoreCase("datatype")) {
            	 	if (insideMaling){
            	 		malingdata =xpp.nextText();
            	 		maling.setDatatype(malingdata);
            	 		System.out.println("Maling datatype: "+ malingdata);
            	 	}
             }
             else if (xpp.getName().equalsIgnoreCase("hastighet")) {
            	 	if (insideMaling){
            	 		malingdata =xpp.nextText();
            	 		maling.setHastighet(RESTStringParam.string2BD(malingdata));
            	 		System.out.println("Maling hastighet: "+malingdata);
            	 	}
	    	}
             else if (xpp.getName().equalsIgnoreCase("status")) {
            	 	if (insideMaling){
            	 		malingdata =xpp.nextText();
            	 		maling.setStatus(new BigInteger(malingdata));
            	 		System.out.println("Maling status: "+malingdata);
            	 	}
             }
 
             else if (xpp.getName().equalsIgnoreCase("akseldata")) {
            	 	if (insideMaling){
            	 		insideAkseldata = true;      	
            	 		Akseldata  akseldata = parseAkseldata( insideAkseldata);
            	 		maling.getAkseldata().add(akseldata);
            	 		//aksellist.add(akseldata);

            	 	}
             }
	 
             else if (xpp.getName().equalsIgnoreCase("aktiv")) {
            	 	if (insideMaling){
            	 		malingdata =xpp.nextText();
            	 		maling.setAktiv(Boolean.parseBoolean(malingdata));
               	 		System.out.println("Maling Aktiv: "+malingdata);
            	 	}
             }
		} 
	    else if (eventType == XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase("måling")) {
	    
 	    	  insideMaling = false;
 	    	  break;
 	    	
	    }
	    //move to next element
	    eventType = xpp.next();
	 } 
	 System.out.println("aksellist size: "+maling.getAkseldata().size());
	 /*
     JSONBuilder jb = new JSONBuilder();
     String jsonstr = jb.getString(maling);
     
     //recording into ES
     eshc.postJsonDocument(jsonstr);	 
	 */
     return maling;
	}
	  
  //  public static void main (String args[])
  //          throws XmlPullParserException, IOException {
    	
  
    public static int XMLExporter (String filename, String objparsingpattern)
            throws XmlPullParserException, IOException {
       
    	   int status          = 1;
    	   int poststatus      = 1;
    	   XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
           factory.setNamespaceAware(true);
           xpp = factory.newPullParser();
            
           File file = new File(filename);
            
           FileInputStream fis = new FileInputStream(file);
           xpp.setInput(fis, Config.XMLENCODING);


            //Keep track of which tag inside of XML
            boolean insideMaling    = false;
            boolean insideAtkstat   = true;
            boolean insideDlogger   = false;
            boolean insideHastmaler = false;
            
   	        boolean insideAkseldata = false;
   	        boolean insideKSted     = false;
   	        boolean insideKStrek    = false;
   		    String atkstatdata      = null;
            String jsonstr          = null;

   	        Atkstat atkstat = new Atkstat();
   	        Maling maling   = new Maling();
   	        
   		    RESTDateParam rdp = new RESTDateParam();
       	    ESPostHttpsClient eshc = new ESPostHttpsClient();	 
            JSONBuilder jb = new JSONBuilder();
     		PersistencyManager pm = new PersistencyManager();
   		    ConnectionManager  cm = new ConnectionManager();
            Connection connection = cm.getConnection();
            //XMLFileParser xfp = new XMLFileParser();
            
            try {   
            	System.out.println("Initiating Logger for: " + filename);
    		    int s = filename.lastIndexOf('/');
    	    	String dirtoparse = filename.substring(0, s+1);
    			
    	    	if (!XMLFileParser.isInitiated()){
    	    	   ATKLogger.setup(dirtoparse);
    	    	   XMLFileParser.setInitiated(true);
    			}            	
    			
    	    	System.out.println("Root");
            	int eventType = xpp.getEventType();
            	while (eventType != XmlPullParser.END_DOCUMENT) {
            		if(eventType == XmlPullParser.START_DOCUMENT) {
            			System.out.println("Start atkstatistikk document");
            		} else if(eventType == XmlPullParser.START_TAG) {
            			System.out.println("Start tag "+xpp.getName()+" "+eventType);
                 
            			// START Atkstat            
                        //--------------------------------------------------------------------
            			System.out.println("Parsing Atkstat Object");
                
            			if (xpp.getName().equalsIgnoreCase("versjon")) {
            				if (insideAtkstat){
            					atkstatdata =xpp.nextText();
            					atkstat.setVersjon(atkstatdata);
            					System.out.println("Atkstat versjon: "+atkstatdata);
            				}
            			}	 	
            			else if (xpp.getName().equalsIgnoreCase("navn")) {
            				if (insideAtkstat){
            					atkstatdata =xpp.nextText();
            					atkstat.setNavn(atkstatdata);
            					System.out.println("Atkstat navn: "+atkstatdata);
            				}
            			}
            			else if (xpp.getName().equalsIgnoreCase("starttid")) {
            				if (insideAtkstat){
            					atkstatdata =xpp.nextText();
            					atkstat.setStarttid(rdp.RESTTimestampWithTZDParam(atkstatdata, Config.DATE_FORMAT));
            					System.out.println("Atkstat Dato: "+atkstatdata);
            				}
            			}
            			else if (xpp.getName().equalsIgnoreCase("stopptid")) {
            				if (insideAtkstat){
            					atkstatdata =xpp.nextText();
            					atkstat.setStopptid(rdp.RESTTimestampWithTZDParam(atkstatdata, Config.DATE_FORMAT));
            					System.out.println("Atkstat Dato: "+atkstatdata);
            				}
            			}
             
            			//------------------ kontrollsted
            			else if (xpp.getName().equalsIgnoreCase("kontrollsted") && eventType == XmlPullParser.START_TAG) {
            				if (insideAtkstat){
            					insideKSted = true; 
            					Kontrollsted kontrollsted = new Kontrollsted();
            					kontrollsted = parseKontrollsted( insideKSted);
            					atkstat.getKontrollsted().add(kontrollsted);
            				}
            			}
            			//------------------ kontrollstrekning
            			else if (xpp.getName().equalsIgnoreCase("kontrollstrekning") && eventType == XmlPullParser.START_TAG) {
            				if (insideAtkstat){
            					insideKStrek = true; 
            					Kontrollstrekning kontrollstrekning = new Kontrollstrekning();
            					kontrollstrekning = parseKontrollstrekning( insideKStrek);
            					atkstat.getKontrollstrekning().add(kontrollstrekning);;
            				}
            			}
            			//------------------ datalogger
            			else if (xpp.getName().equalsIgnoreCase("datalogger") && eventType == XmlPullParser.START_TAG) {
            				if (insideAtkstat){
            					insideDlogger = true; 
            					Datalogger datalogger = new Datalogger();
            					datalogger = parseDatalogger(insideDlogger);
            					atkstat.setDatalogger(datalogger);
        	 		
            				}
            			}               
                
            			//------------------ hastighetsmåler
            			else if (xpp.getName().equalsIgnoreCase("hastighetsmåler") && eventType == XmlPullParser.START_TAG) {
            				if (insideAtkstat){
            					insideHastmaler = true; 
            					Hastighetsmaler hastighetsmaler = new Hastighetsmaler();
            					hastighetsmaler = parseHastighetsmaler(insideHastmaler);
            					atkstat.setHastighetsmaler(hastighetsmaler);
        	 		
            				}
            			}                  
                
              
            			// END Atkstat
            			//insideAtkstat = false;
            			/* ------------ Parsing Maling --------------------------*/
            			System.out.println("Parsing Maling Object");
            			if (xpp.getName().equalsIgnoreCase("måling") ) {
            				insideMaling = true;

            				//Create new maling object
            				maling = parseMaling(insideMaling);
            				System.out.println("måling detected " + maling.getDato()+" Hastighet: "+ maling.getHastighet());
                     
            				//->get historic data from DB and setting in into Maling object
            				//Using PersistencyHelper 
            				
            				 String sdatofrom = rdp.convertLongTime(maling.getDato()-1000);
            				 String sdatoto   = rdp.convertLongTime(maling.getDato()+1000);;
            				 String serie_no  = atkstat.getDatalogger().getSerienummer().toString();
            				 String maalested_kode = atkstat.getKontrollsted().get(0).getNummer().toString();
            				 
            				 Case mcase = pm.getMaleSakFromDb(connection, serie_no, maalested_kode, sdatofrom, sdatoto);
            				 maling.setMcase(mcase);
            				//<-db parsing completed
            				
            				if(objparsingpattern.equalsIgnoreCase("COMBINED")){
            					//for entire JSON ATKSTAT object
            					atkstat.getMaling().add(maling);
            				}   
            				if(objparsingpattern.equalsIgnoreCase("MALING")|| objparsingpattern.equalsIgnoreCase("SEPARATE")){
            					atkstat.setsMaling(maling);
            					jsonstr = jb.getString(atkstat);
            					
            					//System.out.println(jsonstr); for unit etst only

            					//recording into ES

            					poststatus = eshc.postsslJsonObject(atkstat, Config.ES_MALINGINDEX);
            					
            					if (poststatus != 0) {
            						throw new RuntimeException("Failed to post to ES, check the ES availability!" );
            					}else{
            						status = 0;
            						atkstat.setsMaling(null);
            						ATKLogger.contextDestroyed(LOGGER);
            					}

            				}
            			}
            			} else if(eventType == XmlPullParser.END_TAG) {
            				System.out.println("End tag "+xpp.getName());
            			} else if(eventType == XmlPullParser.TEXT) {
            				System.out.println("Text "+xpp.getText());
            			}
            			eventType = xpp.next();
            		} //end while
            		jsonstr = jb.getString(atkstat);
            		System.out.println("End document, writing last JSON atkstat " +jsonstr);
            		// +atkstat.getStarttid()+" Stopptid: "+ atkstat.getStopptid());
                    fis.close();
            		if(objparsingpattern.equalsIgnoreCase("SEPARATE")){

            			//recording into ES
            			// eshc.postJsonString(jsonstr, Config.ES_ATKSTATINDEX);
            			poststatus = eshc.postsslJsonObject(atkstat, Config.ES_STATINDEX);
            			if (poststatus != 0) {
            				throw new RuntimeException("Failed to post to ES, check the ES availability!" );
            			}else{
            				status = 0;
            				ATKLogger.contextDestroyed(LOGGER);
            			}
            		}
            		if(objparsingpattern.equalsIgnoreCase("COMBINED")){

            				//recording into ES
            				//eshc.postJsonString(jsonstr, Config.ES_ATKOBJINDEX);
            				poststatus = eshc.postsslJsonObject(atkstat, Config.ES_OBJINDEX);
            				if (poststatus != 0) {
            					throw new RuntimeException("Failed to post to ES, check the ES availability!" );
            				}else{
            					status = 0;
            					ATKLogger.contextDestroyed(LOGGER);
            				}
            		}
           
            	} catch (Exception e) {
            		status = 1;
            		e.printStackTrace();
            	} finally{
            		try {
						connection.close();
						ATKLogger.contextDestroyed(LOGGER);
						LOGGER.severe("Parsing XML document completed");
					} catch (SQLException e) {
						//e.printStackTrace();
						LOGGER.severe("Failed to close the connection. Check DB connection parameters: " +e.getMessage());
					}
            	}  
            return status;
        }
    

    
    public static void XMLParser(File filename)
            throws XmlPullParserException, IOException {
    
		    RESTDateParam rdp = new RESTDateParam();
       	    ESPostHttpsClient eshc = new ESPostHttpsClient();	 
            JSONBuilder jb = new JSONBuilder();
     		PersistencyManager pm = new PersistencyManager();
   		    ConnectionManager  cm = new ConnectionManager();
            Connection connection = cm.getConnection();

    	try {  
    	   int status          = 1;
    	   int poststatus      = 1;
    	   XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
           factory.setNamespaceAware(true);
           xpp = factory.newPullParser();
           String objparsingpattern = Config.OBJPARSINGPATTERN;
           
           //File file = new File(filename);
            
           FileInputStream fis = new FileInputStream(filename);
           xpp.setInput(fis, Config.XMLENCODING);


            //Keep track of which tag inside of XML
            boolean insideMaling    = false;
            boolean insideAtkstat   = true;
            boolean insideDlogger   = false;
            boolean insideHastmaler = false;
            
   	        boolean insideAkseldata = false;
   	        boolean insideKSted     = false;
   	        boolean insideKStrek    = false;
   		    String atkstatdata      = null;
            String jsonstr          = null;

   	        Atkstat atkstat = new Atkstat();
   	        Maling maling   = new Maling();
   	        
            //XMLFileParser xfp = new XMLFileParser();
            
           // try {   
            	System.out.println("Initiating Logger for: " + filename);
    		    int s = filename.getName().lastIndexOf('/');
    	    	String dirtoparse = filename.getName().substring(0, s+1);
    			
    	    	if (!XMLFileParser.isInitiated()){
    	    	   ATKLogger.setup(dirtoparse);
    	    	   XMLFileParser.setInitiated(true);
    			}            	
    			
    	    	System.out.println("Root");
            	int eventType = xpp.getEventType();
            	while (eventType != XmlPullParser.END_DOCUMENT) {
            		if(eventType == XmlPullParser.START_DOCUMENT) {
            			System.out.println("Start atkstatistikk document");
            		} else if(eventType == XmlPullParser.START_TAG) {
            			System.out.println("Start tag "+xpp.getName()+" "+eventType);
                 
            			// START Atkstat            
                        //--------------------------------------------------------------------
            			System.out.println("Parsing Atkstat Object");
                
            			if (xpp.getName().equalsIgnoreCase("versjon")) {
            				if (insideAtkstat){
            					atkstatdata =xpp.nextText();
            					atkstat.setVersjon(atkstatdata);
            					System.out.println("Atkstat versjon: "+atkstatdata);
            				}
            			}	 	
            			else if (xpp.getName().equalsIgnoreCase("navn")) {
            				if (insideAtkstat){
            					atkstatdata =xpp.nextText();
            					atkstat.setNavn(atkstatdata);
            					System.out.println("Atkstat navn: "+atkstatdata);
            				}
            			}
            			else if (xpp.getName().equalsIgnoreCase("starttid")) {
            				if (insideAtkstat){
            					atkstatdata =xpp.nextText();
            					atkstat.setStarttid(rdp.RESTTimestampWithTZDParam(atkstatdata, Config.DATE_FORMAT));
            					System.out.println("Atkstat Dato: "+atkstatdata);
            				}
            			}
            			else if (xpp.getName().equalsIgnoreCase("stopptid")) {
            				if (insideAtkstat){
            					atkstatdata =xpp.nextText();
            					atkstat.setStopptid(rdp.RESTTimestampWithTZDParam(atkstatdata, Config.DATE_FORMAT));
            					System.out.println("Atkstat Dato: "+atkstatdata);
            				}
            			}
             
            			//------------------ kontrollsted
            			else if (xpp.getName().equalsIgnoreCase("kontrollsted") && eventType == XmlPullParser.START_TAG) {
            				if (insideAtkstat){
            					insideKSted = true; 
            					Kontrollsted kontrollsted = new Kontrollsted();
            					kontrollsted = parseKontrollsted( insideKSted);
            					atkstat.getKontrollsted().add(kontrollsted);
            				}
            			}
            			//------------------ kontrollstrekning
            			else if (xpp.getName().equalsIgnoreCase("kontrollstrekning") && eventType == XmlPullParser.START_TAG) {
            				if (insideAtkstat){
            					insideKStrek = true; 
            					Kontrollstrekning kontrollstrekning = new Kontrollstrekning();
            					kontrollstrekning = parseKontrollstrekning( insideKStrek);
            					atkstat.getKontrollstrekning().add(kontrollstrekning);;
            				}
            			}
            			//------------------ datalogger
            			else if (xpp.getName().equalsIgnoreCase("datalogger") && eventType == XmlPullParser.START_TAG) {
            				if (insideAtkstat){
            					insideDlogger = true; 
            					Datalogger datalogger = new Datalogger();
            					datalogger = parseDatalogger(insideDlogger);
            					atkstat.setDatalogger(datalogger);
        	 		
            				}
            			}               
                
            			//------------------ hastighetsmåler
            			else if (xpp.getName().equalsIgnoreCase("hastighetsmåler") && eventType == XmlPullParser.START_TAG) {
            				if (insideAtkstat){
            					insideHastmaler = true; 
            					Hastighetsmaler hastighetsmaler = new Hastighetsmaler();
            					hastighetsmaler = parseHastighetsmaler(insideHastmaler);
            					atkstat.setHastighetsmaler(hastighetsmaler);
        	 		
            				}
            			}                  
                
              
            			// END Atkstat
            			//insideAtkstat = false;
            			/* ------------ Parsing Maling --------------------------*/
            			System.out.println("Parsing Maling Object");
            			if (xpp.getName().equalsIgnoreCase("måling") ) {
            				insideMaling = true;

            				//Create new maling object
            				maling = parseMaling(insideMaling);
            				System.out.println("måling detected " + maling.getDato()+" Hastighet: "+ maling.getHastighet());
                     
            				//->get historic data from DB and setting in into Maling object
            				//Using PersistencyHelper 
            				
            				 String sdatofrom = rdp.convertLongTime(maling.getDato()-1000);
            				 String sdatoto   = rdp.convertLongTime(maling.getDato()+1000);;
            				 String serie_no  = atkstat.getDatalogger().getSerienummer().toString();
            				 String maalested_kode = atkstat.getKontrollsted().get(0).getNummer().toString();
            				 
            				 Case mcase = pm.getMaleSakFromDb(connection, serie_no, maalested_kode, sdatofrom, sdatoto);
            				 maling.setMcase(mcase);
            				 //<-db parsing completed
            				
            				if(objparsingpattern.equalsIgnoreCase("COMBINED")){
            					//for entire JSON ATKSTAT object
            					atkstat.getMaling().add(maling);
            				}   
            				if(objparsingpattern.equalsIgnoreCase("MALING")|| objparsingpattern.equalsIgnoreCase("SEPARATE")){
            					atkstat.setsMaling(maling);
            					jsonstr = jb.getString(atkstat);
            					
            					//System.out.println(jsonstr); for unit etst only

            					//recording into ES

            					poststatus = eshc.postsslJsonObject(atkstat, Config.ES_MALINGINDEX);
            					//poststatus = eshc.sendPost(atkstat, Config.ES_MALINGINDEX);
            					if (poststatus != 0) {
            						throw new RuntimeException("Failed to post to ES, check the ES availability!" );
            					}else{
            						status = 0;
            						atkstat.setsMaling(null);
            						ATKLogger.contextDestroyed(LOGGER);
            					}

            				}
            			}
            			} else if(eventType == XmlPullParser.END_TAG) {
            				System.out.println("End tag "+xpp.getName());
            			} else if(eventType == XmlPullParser.TEXT) {
            				System.out.println("Text "+xpp.getText());
            			}
            			eventType = xpp.next();
            		} //end while
            		jsonstr = jb.getString(atkstat);
            		System.out.println("End document, writing last JSON atkstat " +jsonstr);
            		// +atkstat.getStarttid()+" Stopptid: "+ atkstat.getStopptid());
                    fis.close();
            		if(objparsingpattern.equalsIgnoreCase("SEPARATE")){

            			//recording into ES
            			// eshc.postJsonString(jsonstr, Config.ES_ATKSTATINDEX);
            			poststatus = eshc.postsslJsonObject(atkstat, Config.ES_STATINDEX);
            			if (poststatus != 0) {
            				throw new RuntimeException("Failed to post to ES, check the ES availability!" );
            			}else{
            				status = 0;
            				ATKLogger.contextDestroyed(LOGGER);
            			}
            		}
            		if(objparsingpattern.equalsIgnoreCase("COMBINED")){

            				//recording into ES
            				//eshc.postJsonString(jsonstr, Config.ES_ATKOBJINDEX);
            				poststatus = eshc.postsslJsonObject(atkstat, Config.ES_OBJINDEX);
            				if (poststatus != 0) {
            					throw new RuntimeException("Failed to post to ES, check the ES availability!" );
            				}else{
            					status = 0;
            					ATKLogger.contextDestroyed(LOGGER);
            				}
            		}
           
            	} catch (Exception e) {
            		//status = 1;
            		e.printStackTrace();
              	} finally{
            		try {
						connection.close();
						ATKLogger.contextDestroyed(LOGGER);
						LOGGER.severe("Parsing XML document completed");
					} catch (SQLException e) {
						//e.printStackTrace();
						LOGGER.severe("Failed to close the connection. Check DB connection parameters: " +e.getMessage());
					}
            	}  
              //return status;
        }    
    
    
    
    public static void main (String args[]) throws  XmlPullParserException, IOException{
    	
    	//Config.getParams();
        String controlfolder     = Config.XMLFILE;
        String objparsingpattern = "MALING"; 
        boolean parsefolder      = false;
        boolean scheduled        = false;
       	FileHandler  filehandler = new FileHandler();
        
        int status          = 1;
        
        if (args.length == 0) {
           	scheduled  = true; 
        }else if (args.length == 1) {
            controlfolder       = args[0];
            parsefolder         = true;
        }else if (args.length == 2) {
            controlfolder       = args[0];      
        }else {
             System.out.println("Wrong number of arguments; ");
             System.out.println("Usage:  <XMLExporter>.bat controlfolder objparsingpattern"); 
             System.out.println("Where:  <XMLExporter>.bat - command file for scanner;"); 
             System.out.println("        controlfolder   - URL/FSO for Inbound Stat XML file ; "); 
             System.out.println("        objparsingpattern  - Object parsing pattern; "); 
             System.out.println("Object parsing pattern is the way how Atkstat and Maling objects will be recorded into ES"); 
             System.out.println("Where :"); 
             System.out.println("        MALING   - Only single Maling object with Objstat header on top (maling wrapped); "); 
     	 	 System.out.println("        SEPARATE - Objstat(as a single object) and Maling object separately; ");
             System.out.println("        COMPLETE - Complete Master STAT Object with Malings array inside; "); 
             throw new IllegalArgumentException("Programm terminated with handled exception:.");
        }
       
  	   try{
  		   if(scheduled){
  			  FileHandler.main(args); 
  		   }else{
    		      if (parsefolder){
       		          filehandler.parseData(controlfolder);
    		          status = 0;
    		      }else{  			   
    			      //ATKLogger.setup("");
    		    	  status =  XMLExporter(controlfolder, objparsingpattern);
    		      }	  
  		   }
  		         
  	   }catch(Exception e){
  		  e.printStackTrace();
  	   }

	}
}
