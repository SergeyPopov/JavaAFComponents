package af.mcsa.stat.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Properties;

public class Config {

	public static  boolean IS_LOCAL = true;
	public static  String  LOGGING_LEVEL = "3";
    public static  String ES_STATLOGINDEX  = "kontroll/ifilm";
    public static  String FF_PARSESUBDIR   = "false";
    public static  String FF_EXT           = "STA";
    public static  String FFLOG        = "ff-parser.log";
    public static  int    FFPARSER_TIMEOUT = 10;
	//"D:/Sergey/Projects/ATK/Statistikk_for_ATK/KDServer/MSGBox/ATK/In/";
    //"D:/Sergey/Projects/ATK/Statistikk_for_ATK/KDServer/MSGBox/ATK/In/---15---.STA"
    public static  String FFDIR  = "/msgbox/In/";
    public static  String FFFILE = null;
    public static  String FFMAPPER = "atkffadapter-config.xml";
    public static  String FFTRACE  = null;
    public static  String XMLENCODING = "ISO-8859-1";
    public static  String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    public static  String POST_PROJECT_URL = "https://acandostatlab.uio.no/rest/projects";
    public static  String ES_LOCALHTTP_URL = "http://localhost:9200/";
    public static  String LAST_PROCESSED_FILE = null;

    
    public static void getParams(){
    
    	Properties prop = new Properties();
    	String propfile ="./config/stat.properties";
    	File fp         = new File(propfile);
    	//BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(propfile)));
    	
    	try {

    		prop.load(new FileInputStream(fp));

    		// set the properties value
    		LOGGING_LEVEL       = prop.getProperty("logging_level");
    		ES_STATLOGINDEX     = prop.getProperty("es_statlogindex");
    		FF_EXT              = prop.getProperty("ff_ext");
    		FFLOG               = prop.getProperty("fflog");
    		FF_PARSESUBDIR      = prop.getProperty("ff_parsesubdir");
    		FFTRACE             = prop.getProperty("fftrace");
    		FFMAPPER            = prop.getProperty("ffmapper");
    		FFPARSER_TIMEOUT    = Integer.parseInt(prop.getProperty("ffparser_timeout"));
    		FFDIR               = prop.getProperty("ffdir");
    		FFFILE              = prop.getProperty("fffile");    		
    		XMLENCODING         = prop.getProperty("xmlencoding");    	
    		DATE_FORMAT         = prop.getProperty("date_format");        		     		
    		POST_PROJECT_URL    = prop.getProperty("post_project_url");      		
    		ES_LOCALHTTP_URL    = prop.getProperty("es_localhttp_url");        		
    		LAST_PROCESSED_FILE = prop.getProperty("last_processed_file");
    		
    		
    	} catch (IOException io) {
    		io.printStackTrace();
    	} 
   }
    
    public static String getLastParsedFile(String atk_dir){
        
    	Properties prop = new Properties();
    	String propfile = FFTRACE;
    	File fp         = new File(atk_dir+propfile);
    	String fname    = null;
    	
    	try {
    		prop.load(new FileInputStream(fp));
    		// set the properties value
    		fname  = prop.getProperty("last_processed_file");
    	}catch (FileNotFoundException e){
    	    return fname;
    	}catch (Exception io) {
    		io.printStackTrace();
    	} 
    	return fname;
   }
  
    public static void setParams(String filename, String atk_dir) throws FileNotFoundException{
        
    	Properties prop = new Properties();
    	//OutputStream output = null;
    	String propfile = FFTRACE;
    	File fp         = new File(atk_dir+propfile);
    	FileOutputStream fileOut = new FileOutputStream(fp);
    	
    	try {

    		//output = new FileOutputStream(fp);

    		// set the properties value
    		prop.setProperty("last_processed_file", filename);
    		prop.setProperty("last_processed_directory", atk_dir);

    		// save properties to project root folder
    		prop.store(fileOut, null);

    	} catch (IOException io) {
    		io.printStackTrace();
    	} finally {
    		if (fileOut != null) {
    			try {
    				fileOut.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    		}

    	}
   }    
    
 
    
    
    public static void main (String args[]) throws IOException{
     setParams("testparam","testvalue");
    /*	
  	   try{
  		   Config.getParams();
  		   System.out.println("All parameters are set");
  		   System.out.println("FFDIR: "+FFDIR);
  	   }catch(Exception e){
  		  e.printStackTrace();
  	   }
      */ 
	}
}
    

