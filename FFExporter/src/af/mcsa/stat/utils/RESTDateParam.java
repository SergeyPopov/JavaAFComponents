package af.mcsa.stat.utils;


import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.ws.rs.WebApplicationException;


public class RESTDateParam {
	
    private static SimpleDateFormat df = new SimpleDateFormat( Config.DATE_FORMAT , Locale.ENGLISH);
    private static java.sql.Date date;
    private static java.sql.Timestamp timestamp;
    
	public  RESTDateParam() {
	}

	
    public static Timestamp RESTTimestampWithTZDParam( String dateTimeStr, String dateFormatStr ) throws WebApplicationException {
    	SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd", Locale.ENGLISH );
    	java.util.Date date;
    	java.sql.Timestamp tmsdate;
        try {
        	// date = new java.util.Date( df.parse( dateTimeStr ).getTime() );
        	
        	 tmsdate = new java.sql.Timestamp( df.parse( dateTimeStr ).getTime() );
        } catch ( final ParseException ex ) {
            throw new WebApplicationException( ex );
        }
        return tmsdate;
    }
	
	public static int getWeekNumber(String dateStr){
		int week = 0;
		SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd", Locale.ENGLISH );
		//Date date = null;
		try {
			//date = df.parse(datestr);
			java.sql.Timestamp date = new java.sql.Timestamp( df.parse( dateStr ).getTime() );
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			week = cal.get(Calendar.WEEK_OF_YEAR);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
	   return week;
	}
	
	public static int getWeekNumberTest53(int year, int month, int date ){
		int week = 0;
		SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd", Locale.ENGLISH );
		//Date date = null;
		try {
			 Calendar cal = Calendar.getInstance();
			 //cal.set(year, month, date);
			 cal.set(Calendar.YEAR, 2015);
			 cal.set(Calendar.MONTH, Calendar.DECEMBER);
			 cal.set(Calendar.DAY_OF_MONTH, 31);
			 week = cal.get(Calendar.WEEK_OF_YEAR);
			 //System.out.println(week);
		} catch (Exception e) {
			e.printStackTrace();
		}
	   return week;
	}
	
	public static int getWeekNumberBase53(int year, int month, int date ){
		int week = 0;
		SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd", Locale.ENGLISH );
		//Date date = null;
		try {
			 Calendar cal = Calendar.getInstance();
			 cal.set(year, month, date);
			 //cal.set(Calendar.YEAR, 2015);
			 //cal.set(Calendar.MONTH, Calendar.DECEMBER);
			 //cal.set(Calendar.DAY_OF_MONTH, 31);
			 week = cal.get(Calendar.WEEK_OF_YEAR);
			 //System.out.println(week);
		} catch (Exception e) {
			e.printStackTrace();
		}
	   return week;
	}

	
	
	public static void main(String[] args) {
		int year = 15;
		int month = 12;
		int day = 29;
		int week = 0;
		
		int millenia = 0;
		if (year > 50){
			millenia = 1900 + year;
			
		}else{
			millenia = 2000 + year;
		}
		
		String input = Integer.toString(millenia) +"-"
                + Integer.toString(month)+"-"
                + Integer.toString(day);
		//String input = "2009-08-01";

		System.out.println("STARTDATO input : " + input);
		java.sql.Timestamp tmsdate = RESTTimestampWithTZDParam(input, Config.DATE_FORMAT);
		System.out.println(tmsdate);
		input = tmsdate.toString();
		
		week = getWeekNumber(input);
		System.out.println(week);
		
		week = getWeekNumberBase53(millenia, month, day);
		System.out.println(week);

		week = getWeekNumberTest53(millenia, month, day);
		System.out.println(week);
	}

}
