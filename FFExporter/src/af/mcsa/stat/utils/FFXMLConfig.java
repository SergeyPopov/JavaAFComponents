package af.mcsa.stat.utils; 

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import java.io.File;
import java.util.logging.Logger;

public class FFXMLConfig {
	
	 public  FFXMLConfig() {}
	  private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	  public  NodeList getConfigArray() {
		  
		    NodeList nList = null;

		    try {

			File fXmlFile = new File(Config.FFMAPPER);
		    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
					
			nList = doc.getElementsByTagName("property");

			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					LOGGER.fine("Field's start : "   + eElement.getAttribute("start"));
					LOGGER.fine("Field's end : "     + eElement.getAttribute("end"));
					LOGGER.fine("ObjectFieldName : " + eElement.getTextContent());
				}
			}
		    } catch (Exception e) {
		  	   e.printStackTrace();
		    }
		    return nList;
  }	
	

  public static void main(String argv[]) {

    try {

	File fXmlFile = new File("D:/Sergey/Projects/ATK/Statistikk_for_ATK/KDServer/MSGBox/ATK/Config/atkffadapter-config.xml");
	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	Document doc = dBuilder.parse(fXmlFile);
	doc.getDocumentElement().normalize();

	System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			
	NodeList nList = doc.getElementsByTagName("property");
			
	System.out.println("----------------------------");

	for (int temp = 0; temp < nList.getLength(); temp++) {

		Node nNode = nList.item(temp);
				
		System.out.println("\nCurrent Element :" + nNode.getNodeName());
				
		if (nNode.getNodeType() == Node.ELEMENT_NODE) {

			Element eElement = (Element) nNode;

			System.out.println("Field's start : "   + eElement.getAttribute("start"));
			System.out.println("Field's end : "     + eElement.getAttribute("end"));
			System.out.println("ObjectFieldName : " + eElement.getTextContent());

		}
	}
    } catch (Exception e) {
  	   e.printStackTrace();
    }
  }

}