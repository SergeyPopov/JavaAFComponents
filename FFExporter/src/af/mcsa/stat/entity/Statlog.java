package af.mcsa.stat.entity;

import java.math.BigInteger;
import java.sql.Timestamp;

public class Statlog {

	public  Statlog() {}

	public int FIELD1;
  	public int FIELD2;
  	public int FIELD3;
  	public int FIELD4;
  	public int FIELD5; 
	public int FIELD6;
  	public int FIELD7;
  	public int FIELD8;
  	public int FIELD9;
  	public int FIELD10;
  	public int FIELD11;
  	public int FIELD12;
  	public int FIELD13;
	public int FIELD14;
	public int FIELD15;	
  	public int FIELD16;
  	public int FIELD17;
  	public int FIELD18;
  	public int FIELD19;
    public int FIELD20;
  	public int FIELD21;
  	public int FIELD22;
  	public int FIELD23;
  	public int FIELD24;
  	public int FIELD25;
  	public int FIELD26;
  	public int FIELD27;
  	public int FIELD28;
  	public int FIELD29;
  	public int FIELD30;
  	public int FIELD31;
  	public int FIELD32;
  	public int FIELD33;      
  	public int FIELD34;  
  	public int FIELD35; 
  	public int FIELD36;       
  	public int FIELD37;    
  	public int FIELD38;    
  	public int FIELD39;    
  	public int FIELD40;    
  	public int FIELD41;    
  	public int FIELD42;    
  	public int FIELD43;    
  	public int FIELD44;    
  	public int FIELD45;    
  	public int FIELD46;    
  	public int FIELD47;    
  	public int FIELD48;    
  	public int FIELD49;    
  	public int FIELD50;    
  	public int FIELD51;    
  	public int FIELD52;    
  	public int FIELD53;    
  	public int FIELD54;    
  	public int FIELD55;    
  	public int FIELD56;    
  	public int FIELD57;    
  	public int FIELD58;    
  	public int FIELD59;    
  	public int FIELD60;    
  	public int FIELD61;    
  	public int FIELD62;    
  	public int FIELD63;    
  	public int FIELD64;    
  	public int FIELD65;    
  	public int FIELD66;    
  	public int FIELD67;    
  	public int FIELD68;    
  	public int FIELD69;    
  	public int FIELD70;    
  	public int FIELD71;    
  	public int FIELD72;    
  	public int FIELD73;    
  	public int FIELD74;    
  	public int FIELD75;    
  	public int FIELD76;    
  	public int FIELD77;    
  	public String FIELD78;    
  	public int FIELD79;    
  	public float FIELD80;    
  	public String FIELD81;    
  	public int FIELD82;    
  	public int FIELD83;    
  	public String FIELD84;    
  	public String FIELD85;    
  	public String FIELD86;    
  	

  	public String MAALESTED_KODE;
	public Timestamp STARTDATO;
	public int STARTWEEK;	
	
  	public int getFIELD1() {
		return FIELD1;
	}
	public void setFIELD1(int iFIELD1) {
		FIELD1 = iFIELD1;
	}
	public int getFIELD2() {
		return FIELD2;
	}
	public void setFIELD2(int iFIELD2) {
		FIELD2 = iFIELD2;
	}
	public int getFIELD3() {
		return FIELD3;
	}
	public void setFIELD3(int iFIELD3) {
		FIELD3 = iFIELD3;
	}
	public int getFIELD4() {
		return FIELD4;
	}
	public void setFIELD4(int iFIELD4) {
		FIELD4 = iFIELD4;
	}
	
  	public int getFIELD5() {
		return FIELD5;
	}
	public void setFIELD5(int iFIELD5) {
		FIELD5 = iFIELD5;
	}

	public int getFIELD6() {
		return FIELD6;
	}
	public void setFIELD6(int iFIELD6) {
		FIELD6 = iFIELD6;
	}
	public int getFIELD7() {
		return FIELD7;
	}
	public void setFIELD7(int iFIELD7) {
		FIELD7 = iFIELD7;
	}
	public int getFIELD8() {
		return FIELD8;
	}
	public void setFIELD8(int iFIELD8) {
		FIELD8 = iFIELD8;
	}
	public int getFIELD9() {
		return FIELD9;
	}
	public void setFIELD9(int iFIELD9) {
		FIELD9 = iFIELD9;
	}
	public int getFIELD10() {
		return FIELD10;
	}
	public void setFIELD10(int iFIELD10) {
		FIELD10 = iFIELD10;
	}
	public int getFIELD11() {
		return FIELD11;
	}
	public void setFIELD11(int iFIELD11) {
		FIELD11 = iFIELD11;
	}
	public int getFIELD12() {
		return FIELD12;
	}
	public void setFIELD12(int iFIELD12) {
		FIELD12 = iFIELD12;
	}
	public int getFIELD13() {
		return FIELD13;
	}
	public void setFIELD13(int iFIELD13) {
		FIELD13 = iFIELD13;
	}
  	public int getFIELD14() {
		return FIELD14;
	}
	public void setFIELD14(int iFIELD14) {
		FIELD14 = iFIELD14;
	}
	
	public int getFIELD15() {
		return FIELD15;
	}
	public void setFIELD15(int iFIELD15) {
		FIELD15 = iFIELD15;
	}

	public int getFIELD16() {
		return FIELD16;
	}
	public void setFIELD16(int iFIELD16) {
		FIELD16 = iFIELD16;
	}
	public int getFIELD17() {
		return FIELD17;
	}
	public void setFIELD17(int iFIELD17) {
		FIELD17 = iFIELD17;
	}
	public int getFIELD18() {
		return FIELD18;
	}
	public void setFIELD18(int iFIELD18) {
		FIELD18 = iFIELD18;
	}
	public int getFIELD19() {
		return FIELD19;
	}
	public void setFIELD19(int iFIELD19) {
		FIELD19 = iFIELD19;
	}
	public int getFIELD20() {
		return FIELD20;
	}
	public void setFIELD20(int iFIELD20) {
		FIELD20 = iFIELD20;
	}
	public int getFIELD21() {
		return FIELD21;
	}
	public void setFIELD21(int iFIELD21) {
		FIELD21 = iFIELD21;
	}
	public int getFIELD22() {
		return FIELD22;
	}
	public void setFIELD22(int iFIELD22) {
		FIELD22 = iFIELD22;
	}
	public int getFIELD23() {
		return FIELD23;
	}
	public void setFIELD23(int iFIELD23) {
		FIELD23 = iFIELD23;
	}
	public int getFIELD24() {
		return FIELD24;
	}
	public void setFIELD24(int iFIELD24) {
		FIELD24 = iFIELD24;
	}
	public int getFIELD25() {
		return FIELD25;
	}
	public void setFIELD25(int iFIELD25) {
		FIELD25 = iFIELD25;
	}
	public int getFIELD26() {
		return FIELD26;
	}
	public void setFIELD26(int iFIELD26) {
		FIELD26 = iFIELD26;
	}
	public int getFIELD27() {
		return FIELD27;
	}
	public void setFIELD27(int iFIELD27) {
		FIELD27 = iFIELD27;
	}
	public int getFIELD28() {
		return FIELD28;
	}
	public void setFIELD28(int iFIELD28) {
		FIELD28 = iFIELD28;
	}
	public int getFIELD29() {
		return FIELD29;
	}
	public void setFIELD29(int iFIELD29) {
		FIELD29 = iFIELD29;
	}
	public int getFIELD30() {
		return FIELD30;
	}
	public void setFIELD30(int iFIELD30) {
		FIELD30 = iFIELD30;
	}
	public int getFIELD31() {
		return FIELD31;
	}
	public void setFIELD31(int iFIELD31) {
		FIELD31 = iFIELD31;
	}
	public int getFIELD32() {
		return FIELD32;
	}
	public void setFIELD32(int iFIELD32) {
		FIELD32 = iFIELD32;
	}
	public int getFIELD33() {
		return FIELD33;
	}
	public void setFIELD33(int iFIELD33) {
		FIELD33 = iFIELD33;
	}
	public int getFIELD34() {
		return FIELD34;
	}
	public void setFIELD34(int iFIELD34) {
		FIELD34 = iFIELD34;
	}
	public int getFIELD35() {
		return FIELD35;
	}
	public void setFIELD35(int iFIELD35) {
		FIELD35 = iFIELD35;
	}
	public int getFIELD36() {
		return FIELD36;
	}
	public void setFIELD36(int iFIELD36) {
		FIELD36 = iFIELD36;
	}
	public int getFIELD37() {
		return FIELD37;
	}
	public void setFIELD37(int iFIELD37) {
		FIELD37 = iFIELD37;
	}
	public int getFIELD38() {
		return FIELD38;
	}
	public void setFIELD38(int iFIELD38) {
		FIELD38 = iFIELD38;
	}
	public int getFIELD39() {
		return FIELD39;
	}
	public void setFIELD39(int iFIELD39) {
		FIELD39 = iFIELD39;
	}
	public int getFIELD40() {
		return FIELD40;
	}
	public void setFIELD40(int iFIELD40) {
		FIELD40 = iFIELD40;
	}
	public int getFIELD41() {
		return FIELD41;
	}
	public void setFIELD41(int iFIELD41) {
		FIELD41 = iFIELD41;
	}
	public int getFIELD42() {
		return FIELD42;
	}
	public void setFIELD42(int iFIELD42) {
		FIELD42 = iFIELD42;
	}
	public int getFIELD43() {
		return FIELD43;
	}
	public void setFIELD43(int iFIELD43) {
		FIELD43 = iFIELD43;
	}
	public int getFIELD44() {
		return FIELD44;
	}
	public void setFIELD44(int iFIELD44) {
		FIELD44 = iFIELD44;
	}
	public int getFIELD45() {
		return FIELD45;
	}
	public void setFIELD45(int iFIELD45) {
		FIELD45 = iFIELD45;
	}
	public int getFIELD46() {
		return FIELD46;
	}
	public void setFIELD46(int iFIELD46) {
		FIELD46 = iFIELD46;
	}
	public int getFIELD47() {
		return FIELD47;
	}
	public void setFIELD47(int iFIELD47) {
		FIELD47 = iFIELD47;
	}
	public int getFIELD48() {
		return FIELD48;
	}
	public void setFIELD48(int iFIELD48) {
		FIELD48 = iFIELD48;
	}
	public int getFIELD49() {
		return FIELD49;
	}
	public void setFIELD49(int iFIELD49) {
		FIELD49 = iFIELD49;
	}
	public int getFIELD50() {
		return FIELD50;
	}
	public void setFIELD50(int iFIELD50) {
		FIELD50 = iFIELD50;
	}
	public int getFIELD51() {
		return FIELD51;
	}
	public void setFIELD51(int iFIELD51) {
		FIELD51 = iFIELD51;
	}
	public int getFIELD52() {
		return FIELD52;
	}
	public void setFIELD52(int iFIELD52) {
		FIELD52 = iFIELD52;
	}
	public int getFIELD53() {
		return FIELD53;
	}
	public void setFIELD53(int iFIELD53) {
		FIELD53 = iFIELD53;
	}
	public int getFIELD54() {
		return FIELD54;
	}
	public void setFIELD54(int iFIELD54) {
		FIELD54 = iFIELD54;
	}
	public int getFIELD55() {
		return FIELD55;
	}
	public void setFIELD55(int iFIELD55) {
		FIELD55 = iFIELD55;
	}
	public int getFIELD56() {
		return FIELD56;
	}
	public void setFIELD56(int iFIELD56) {
		FIELD56 = iFIELD56;
	}
	public int getFIELD57() {
		return FIELD57;
	}
	public void setFIELD57(int iFIELD57) {
		FIELD57 = iFIELD57;
	}
	public int getFIELD58() {
		return FIELD58;
	}
	public void setFIELD58(int iFIELD58) {
		FIELD58 = iFIELD58;
	}
	public int getFIELD59() {
		return FIELD59;
	}
	public void setFIELD59(int iFIELD59) {
		FIELD59 = iFIELD59;
	}
	public int getFIELD60() {
		return FIELD60;
	}
	public void setFIELD60(int iFIELD60) {
		FIELD60 = iFIELD60;
	}
	public int getFIELD61() {
		return FIELD61;
	}
	public void setFIELD61(int iFIELD61) {
		FIELD61 = iFIELD61;
	}
	public int getFIELD62() {
		return FIELD62;
	}
	public void setFIELD62(int iFIELD62) {
		FIELD62 = iFIELD62;
	}
	public int getFIELD63() {
		return FIELD63;
	}
	public void setFIELD63(int iFIELD63) {
		FIELD63 = iFIELD63;
	}
	public int getFIELD64() {
		return FIELD64;
	}
	public void setFIELD64(int iFIELD64) {
		FIELD64 = iFIELD64;
	}
	public int getFIELD65() {
		return FIELD65;
	}
	public void setFIELD65(int iFIELD65) {
		FIELD65 = iFIELD65;
	}
	public int getFIELD66() {
		return FIELD66;
	}
	public void setFIELD66(int iFIELD66) {
		FIELD66 = iFIELD66;
	}
	public int getFIELD67() {
		return FIELD67;
	}
	public void setFIELD67(int iFIELD67) {
		FIELD67 = iFIELD67;
	}
	public int getFIELD68() {
		return FIELD68;
	}
	public void setFIELD68(int iFIELD68) {
		FIELD68 = iFIELD68;
	}
	public int getFIELD69() {
		return FIELD69;
	}
	public void setFIELD69(int iFIELD69) {
		FIELD69 = iFIELD69;
	}
	public int getFIELD70() {
		return FIELD70;
	}
	public void setFIELD70(int iFIELD70) {
		FIELD70 = iFIELD70;
	}
	public int getFIELD71() {
		return FIELD71;
	}
	public void setFIELD71(int iFIELD71) {
		FIELD71 = iFIELD71;
	}
	public int getFIELD72() {
		return FIELD72;
	}
	public void setFIELD72(int iFIELD72) {
		FIELD72 = iFIELD72;
	}
	public int getFIELD73() {
		return FIELD73;
	}
	public void setFIELD73(int iFIELD73) {
		FIELD73 = iFIELD73;
	}
	public int getFIELD74() {
		return FIELD74;
	}
	public void setFIELD74(int iFIELD74) {
		FIELD74 = iFIELD74;
	}
	public int getFIELD75() {
		return FIELD75;
	}
	public void setFIELD75(int iFIELD75) {
		FIELD75 = iFIELD75;
	}
	public int getFIELD76() {
		return FIELD76;
	}
	public void setFIELD76(int iFIELD76) {
		FIELD76 = iFIELD76;
	}
	public int getFIELD77() {
		return FIELD77;
	}
	public void setFIELD77(int iFIELD77) {
		FIELD77 = iFIELD77;
	}
	public String getFIELD78() {
		return FIELD78;
	}
	public void setFIELD78(String sFIELD78) {
		FIELD78 = sFIELD78;
	}
	public int getFIELD79() {
		return FIELD79;
	}
	public void setFIELD79(int iFIELD79) {
		FIELD79 = iFIELD79;
	}
	public float getFIELD80() {
		return FIELD80;
	}
	public void setFIELD80(float fFIELD80) {
		FIELD80 = fFIELD80;
	}
	public String getFIELD81() {
		return FIELD81;
	}
	public void setFIELD81(String sFIELD81) {
		FIELD81 = sFIELD81;
	}
	public int getFIELD82() {
		return FIELD82;
	}
	public void setFIELD82(int iFIELD82) {
		FIELD82 = iFIELD82;
	}
	public float getFIELD83() {
		return FIELD83;
	}
	public void setFIELD83(int iFIELD83) {
		FIELD83 = iFIELD83;
	}
	public String getFIELD84() {
		return FIELD84;
	}
	public void setFIELD84(String sFIELD84) {
		FIELD84 = sFIELD84;
	}
	public String getFIELD85() {
		return FIELD85;
	}
	public void setFIELD85(String sFIELD85) {
		FIELD85 = sFIELD85;
	}
	public String getFIELD86() {
		return FIELD86;
	}
	public void setFIELD86(String sFIELD86) {
		FIELD86 = sFIELD86;
	}

 //--------------------------------------------------------------------------------- 	
 //                Additional fields, not in XML config file
 //--------------------------------------------------------------------------------- 	

	public String getMAALESTED_KODE() {
		return MAALESTED_KODE;
	}
	public void setMAALESTED_KODE(String mAALESTED_KODE) {
		MAALESTED_KODE = mAALESTED_KODE;
	}
	
  	public Timestamp getSTARTDATO() {
		return STARTDATO;
	}
	public void setSTARTDATO(Timestamp sTARTDATO) {
		STARTDATO = sTARTDATO;
	}
	
	public int getSTARTWEEK() {
		return STARTWEEK;
	}
	public void setSTARTWEEK(int sTARTWEEK) {
		STARTWEEK = sTARTWEEK;
	}

}
