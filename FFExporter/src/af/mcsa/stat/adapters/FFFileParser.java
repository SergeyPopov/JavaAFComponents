package af.mcsa.stat.adapters;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.math.BigInteger;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import af.mcsa.stat.adapters.JSONBuilder;
import af.mcsa.stat.db.ESPostHttpClient;
import af.mcsa.stat.entity.Statlog;
import af.mcsa.stat.utils.RtmLogger;
import af.mcsa.stat.utils.Config;
import af.mcsa.stat.utils.FFXMLConfig;
import af.mcsa.stat.utils.RESTDateParam;
import af.mcsa.stat.utils.RESTStringParam;

public class FFFileParser  extends Throwable{
	
	public FFFileParser() throws IOException  {}
  
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public static boolean initiated = false;
    
    public static boolean isInitiated() {
		return initiated;
	}

	public static void setInitiated(boolean initiated) {
		FFFileParser.initiated = initiated;
	}
	
	public static String getSubstring(String str,int start, int end) {
        String element = null;
		try {
			element = str.substring(start, end);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return element;
   }

	public static int parse(String str) {
        
 	    int status         = 1;
 	    int poststatus     = 1;
		String element     = null;
		String fname       = null;
		FFXMLConfig ffcong = new FFXMLConfig();
   	    ESPostHttpClient eshc = new ESPostHttpClient();	 
        NodeList nList = ffcong.getConfigArray();
        
        Statlog slog = new Statlog();
        Class lClass = slog.getClass();
        JSONBuilder jb = new JSONBuilder();
        try {

			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;
                    int start = Integer.parseInt(eElement.getAttribute("start"));
                    int end   = Integer.parseInt(eElement.getAttribute("end"));
                    String datatype = getSubstring(eElement.getAttribute("type"),0,1); 
                    fname    = eElement.getTextContent();
                    
                    LOGGER.fine("Field's start : "   + eElement.getAttribute("start"));
                    LOGGER.fine("Field's end : "     + eElement.getAttribute("end"));
                    LOGGER.fine("ObjectFieldName : " + eElement.getTextContent() +" datatype: "+datatype);
					
					element = getSubstring(str,start-1,end);
					
					LOGGER.fine("Extracted field : " + element);
					
					Field field = lClass.getField(fname);
					LOGGER.fine("Reflected field : " + field.toString());
					
					if(datatype.equalsIgnoreCase("I")){
						if (element.trim().length()==0){
							element ="0";
						}
				   	    field.set(slog, Integer.parseInt(element.trim()));
					}else if (datatype.equalsIgnoreCase("B")){
						if (element.trim().length()==0){
							element ="0";
						}
						field.set(slog, new BigInteger(element.trim().getBytes()));    
					}else if (datatype.equalsIgnoreCase("F")){
						if (element.trim().length()==0){
							element ="0.0";
						}
						//replacing commas with dot
						element = element.replace(",", ".");
						field.set(slog, Float.parseFloat(element.trim()));
					} else{
						if (element.trim().length()==0){
						    field.set(slog, " ");
						}else{
							element = RESTStringParam.getEncodedText(element);
						    field.set(slog, element.trim());
						}
					}
					
					String getFIELD2 = Integer.toString(slog.getFIELD2());
					if (getFIELD2.length()<2){
						getFIELD2 = "0" + getFIELD2;
					}
					
					slog.setMAALESTED_KODE(getFIELD2+ Integer.toString(slog.getFIELD4()));
					
	          
					int millenia = 0;
					if (slog.getFIELD11() > 50){
						millenia = 1900 + slog.getFIELD11();
						
					}else{
						millenia = 2000 + slog.getFIELD11();
					}
					
					
					String startdato = Integer.toString(millenia) +"-"
					                 + Integer.toString(slog.getFIELD12())+"-"
					                 + Integer.toString(slog.getFIELD13());
					
					slog.setSTARTDATO(RESTDateParam.RESTTimestampWithTZDParam(startdato, Config.DATE_FORMAT));
					slog.setSTARTWEEK(RESTDateParam.getWeekNumber(startdato));
					//System.out.println("STARTDATO for YY : " + Integer.toString(slog.getFIELD11()) +  " and week "+slog.getSTARTWEEK() + " is "+ slog.getSTARTDATO().toString());
					
					//System.out.println("--------------------------------------------");

				}
			}
			// posting to ES withing try
			//System.out.println(jb.getString(slog));
			poststatus = eshc.postJsonObject(slog, Config.ES_STATLOGINDEX);
			if (poststatus != 0) {
				LOGGER.severe("Failed to post to ES, check the ES availability! MAALESTED_KODE: " + slog.getFIELD5());
				throw new RuntimeException("Failed to post to ES, check the ES availability!" );
			}else{
				status = 0;
				slog =null;
			}
		} catch (Exception e) {
			LOGGER.severe("Error in data formatting! MAALESTED_KODE: " + slog.getFIELD5() + " Parsing field: " + fname + " Error message: " + e.getMessage());
			e.printStackTrace();
		} 
        return status;
		
   }

	
	public static int FFExporter(String controlfolder) throws IOException {
		
		int status = 1;
		FFFileParser ffc = new FFFileParser();
		try {

		    int s = controlfolder.lastIndexOf('/');
	    	String dirtoparse = controlfolder.substring(0, s+1);
	    	
			if (!FFFileParser.isInitiated()){
	    	     RtmLogger.setup(dirtoparse);
	    	     FFFileParser.setInitiated(true);
			}
			
			File file      = new File(controlfolder);
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			//StringBuffer stringBuffer = new StringBuffer();
			//String last,line;
			Long i = (long) 0; 
			Long j = (long) 0; 
			
	        String next, line = bufferedReader.readLine();
	        for (boolean first = true, last = (line == null); !last; first = false, line = next) {
	                last = ((next = bufferedReader.readLine()) == null);

	                //process the line
	                status = parse(line);
	                i=i+1;
	                if (status !=0){
	                	j=j+1;
	                }
	                
	        }
	        LOGGER.severe("Completed. Lines processed:"+i +"; Failed = " + j);
			RtmLogger.contextDestroyed(LOGGER);
			
			bufferedReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return status;
	}	    
	
	public static void readwriresreams(String file2read,String file2write) {
		InputStream inputStream = null;
		OutputStream outputStream = null;
		try {
			// read this file into InputStream
			inputStream = new FileInputStream(file2read);
			// write the inputStream to a FileOutputStream
			outputStream = new FileOutputStream(new File(file2write));
			int read = 0;
			byte[] bytes = new byte[1024];
			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
			LOGGER.severe("Done!");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (outputStream != null) {
				try {
					// outputStream.flush();
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		}
   }
	
	
	public static void main(String[] args) throws IOException {
	  	//Config.getParams();
		System.out.println("Checking inbound directory at   : " + Config.FFDIR);
        String controlfolder = Config.FFDIR;
        boolean parsefolder  = false;
        boolean scheduled  = false;
       	FileHandler  filehandler   = new FileHandler();
        

        int status           = 1;
        
        if ((args.length == 0)) {
        	scheduled  = true;      
        }else if (args.length == 1) {
             controlfolder       = args[0];
             parsefolder         = true;
        }else if (args.length == 2) {
            controlfolder       = args[0];            
       } else {
           System.out.println("Wrong number of arguments; ");
           System.out.println("Usage:  <FFExporter>.bat Infolder "); 
           System.out.println("Where:  <FFExporter>.bat - command file for scanner;"); 
           System.out.println("        Infolder   - URL/FSO for Inbound Stat Flat-Fixed test file, like MSGBox/TP/In/testfile.STA ; "); 
           throw new IllegalArgumentException("Program terminated with handled exception:.");
       }
       
        
       
  	   try{
  		   if(scheduled){
  			  FileHandler.main(args); 
  		   }else{
   		      if (parsefolder){
    		       filehandler.parseData(controlfolder);
 		           status = 0;
 		      }else{
 			       RtmLogger.setup("");
 			       status =  FFExporter(controlfolder);
 		      }  
  		   }
  	   }catch(Exception e){
  		  e.printStackTrace();
  	   }

	}

}
