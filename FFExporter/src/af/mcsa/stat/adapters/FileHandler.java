package af.mcsa.stat.adapters;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import af.mcsa.stat.utils.RtmLogger;
import af.mcsa.stat.utils.Config;
import net.jodah.failsafe.CircuitBreaker;
import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.RetryPolicy;

public class FileHandler implements Runnable{

	
	RetryPolicy retryPolicy = new RetryPolicy()
	  .retryOn(IOException.class)
	  .withDelay(1, TimeUnit.SECONDS)
	  .withMaxRetries(3);
	
    private static Logger logger = Logger.getLogger(
            "af.mcsa.stat.adapters.FileHandler");
    
	public FileHandler(){}
	
	
	public List<String> getFileList(String dirstr){
		List<String> filelist = new ArrayList<String>();
		File[] files = new File(dirstr).listFiles();
		//If this pathname does not denote a directory, then listFiles() returns null. 
    	try{
    		for (File file : files) {
    			if (file.isFile()) {
    				filelist.add(file.getName());
    			}
    		}
    	}  catch (Exception e) {
	        e.printStackTrace();
    	}
    	return filelist;
    }
	
	
	public static void getFileAttr(String file){
       
    	try{
            Path path = Paths.get(file);
            BasicFileAttributes bfa = Files.readAttributes(path, BasicFileAttributes.class);       
            
            System.out.println("Creation Time      : " + bfa.creationTime());
            System.out.println("Last Access Time   : " + bfa.lastAccessTime());
            System.out.println("Last Modified Time : " + bfa.lastModifiedTime());
            System.out.println("Is Directory       : " + bfa.isDirectory());
            System.out.println("Is Other           : " + bfa.isOther());
            System.out.println("Is Regular File    : " + bfa.isRegularFile());
            System.out.println("Is Symbolic Link   : " + bfa.isSymbolicLink());
            System.out.println("Size               : " + bfa.size());
    	}  catch (Exception e) {
	        e.printStackTrace();
    	}
    }

	public static String getExtension(File file){
	   String extension = "";
	   try{
	      String fileName = file.toString();
	      int i = fileName.lastIndexOf('.');
	      if (i > 0) {
	          extension = fileName.substring(i+1);
	      }
	   } catch (Exception e) {
 	      e.printStackTrace();
 	 }  
	 return extension;
	} 
	
	
	public boolean FileExists (String atk_dir) throws  IOException {	

		   boolean locked = false;
		   String lockfile = atk_dir+ Config.FFLOG + ".lck";
		   System.out.println("Checking lock at   : " + lockfile);
	    	try{
	    		
	    		File file = new File(lockfile);
	    		locked = file.exists();
	    	    if (file.exists() && file.isFile())
	    	    {
	    	    	locked = true;
	    			System.out.println("Directory locked   : " + atk_dir);
	    	    }else{
	    	    	locked = false;
	    	    	System.out.println("Directory unlocked   : " + atk_dir);
	    	    }
	    	}  catch (Exception e) {
		        e.printStackTrace();
	    	}
	    	return locked;
	}
	
	public boolean DirExists (String atk_dir) throws  IOException {	
		   boolean exists = false;
	    	try{
	    		File varTmpDir = new File(atk_dir);
	    		exists = varTmpDir.exists();
	    	}  catch (Exception e) {
		        e.printStackTrace();
	    	}
	    	return exists;
	}
	
	public static int DeleteFile(String fullpathtoparse){
	       int status = 1;
	       File parsedfile;
		   logger.info("Deleting file: " + fullpathtoparse);
		   try{
	    	    parsedfile = new File(fullpathtoparse + Config.FFLOG + ".lck");
	    	    if(parsedfile.delete()){
		    	    status = 0;
					System.out.println("Deleted ATK file  : " + fullpathtoparse);	    	    	
	    	    }else{
	    	       status = 1;
	  			   System.out.println("problem with deleting ATK file");
	    	    }
	    	    
		   } catch (Exception e) {
			  System.out.println("problem with deleting ATK file  : " + e.getMessage());
			  logger.severe("Unable delete file: " + e.getMessage());
	 	      e.printStackTrace();
	 	 }  
		 return status;
	} 
	
	
    public static List<String> getDir (String xmlindir) throws  IOException {	
        //Config.ATK_XMLDIR
    	String[] directories = null;
    	try {
      	    File file = new File(xmlindir);
    	      directories = file.list(new FilenameFilter() {
    	      public boolean accept(File current, String name) {
    	        return new File(current, name).isDirectory();
    	      }
    	   });
    	  System.out.println(Arrays.toString(directories));
     	 } catch (Exception e) {
	      e.printStackTrace();
	     }   	
    	 return Arrays.asList(directories);
    }
    
	
    
   public List<String> getATKFilesfromDir (String inbound_dir) throws  IOException {	
     	

     	String ff_ext    = Config.FF_EXT;
   	    String extension = null;
		
		List<String> filelist = new ArrayList<String>();
		//File[] files = new File(atk_dir).listFiles();
		//If this pathname does not denote a directory, then listFiles() returns null. 
    	try{
    		File[] files = new File(inbound_dir).listFiles();
    		for (File file : files) {
    			System.out.println("INBOUND file  : " + file);
    			if (file.isFile()) {
    				extension = getExtension(file);
    				if (extension.equalsIgnoreCase(ff_ext)){
    	    			System.out.println("INBOUND file has extension : " + extension);
    					String lastfile = Config.getLastParsedFile(inbound_dir);
    					System.out.println("Last processed file detected as : " + lastfile);
    					if (lastfile == null || (file.getName().compareTo(lastfile) > 0 )){  
    					   filelist.add(file.getName());
    					   logger.info("INBOUND file  : " + file);
    				       getFileAttr(file.toString());

    				       //unit test only 
    				       //status = XMLFileParser.ATKXMLExporter(file.toString(), objparsingpattern);
    					}else{
    						logger.info("No new files in directory after processed:" + inbound_dir );	
    					}
    				}
    			}
    		}
    	}  catch (Exception e) {
	        e.printStackTrace();
    	}
    	System.out.println("List completed : " + filelist.size());
    	return filelist;
    }
   
 public int parseFilesInDir(String inbound_dir) {

   	logger.info("TEST: import new ATK Flat-Fixed files ("+inbound_dir+") to ES");
   	String fullpathtoparse   = null; 
   	String dirtoparse        = null; 
   	FileHandler  filehandler = new FileHandler();
   	int status =1;
   	System.out.println("Building filelist...");

   	if (inbound_dir == null){
   		dirtoparse = Config.FFDIR;
   	}else{
   		dirtoparse = inbound_dir;
   	}
  	
   	  try {
   		//check for lock - currently running parser for this directory  
   	     if (!FileExists(dirtoparse)){
   	    	
   	    	 System.out.println("Getting into  : " + dirtoparse);
             // get filelist from this directory  
   	    	 List<String> atkdatafiles = filehandler.getATKFilesfromDir(dirtoparse);
   	    	 //ATKLogger.setup(dirtoparse);
   		     if(atkdatafiles.size() > 0) {
              	   logger.info("Identified " + atkdatafiles.size() + " new inbound FF files. Parsing individually");
            
         		   for (String filename : atkdatafiles) {

       				    System.out.println("Parsing new inbound file  : " + filename);
       				      				
       				    fullpathtoparse = dirtoparse+filename;
       				    
       				    //ATKLogger.setup(dirtoparse);
     				    System.out.println("File logger initiated");
     				    
     				    //status = Failsafe.with(retryPolicy).get(() -> FF2JSONConverter.ATKFFExporter(fullpathtoparse));
       				   
     				    status = FFFileParser.FFExporter(fullpathtoparse);
   					    if (status != 0) {
   						    throw new RuntimeException("Failed to process inbound FF-file: " + fullpathtoparse);
   					    }else{
   						    System.out.println("inbound file processed OK : " + fullpathtoparse);
   						    Config.setParams(filename, inbound_dir );
   						    
   				        	//delete lock
   				        	//System.out.println("Removing directory locks...");
   				        	//status = FileHandler.DeleteFile(dirtoparse);
   					    }
       		        }
             
              } else {
            	 logger.info("No new inbound XML files to import");
              }
   	   	  }else {
        	logger.info("Parser instance is already running for this directory");
          }
            
        } catch (Exception e) {
            logger.severe("Error during inbound XML Data Import: " + e.getMessage());
            e.printStackTrace();
        }
   	
        logger.info("Finished inbound XML Data Import");
        return status;
	}
   
   public int parseFilesInSubDir() {

		//List<String> atkdatafolders = filehandler.getDir(Config.ATK_FFDIR);
		
   	logger.info("TEST: import new Stat Flat-Fixed files ("+Config.FF_EXT+") to ES");
   	String fullpathtoparse     = null; 
   	FileHandler  filehandler   = new FileHandler();
   	int status =1;
   	System.out.println("Building filelis...");
   	
   	try {
   		 List<String> atkdatafolders = filehandler.getDir(Config.FFDIR);
   		 //filehandler.parseFilesInDir(Config.ATK_FFDIR);
          
   		 if(atkdatafolders.size() > 0) {
            	logger.info("Identified " + atkdatafolders + " new Inbound directory. Parsing content");
           
         		for (String folder : atkdatafolders) {

      				System.out.println("Inbound file  : " + folder);
      				
      				fullpathtoparse = Config.FFDIR+folder;

      				filehandler.parseFilesInDir(fullpathtoparse);
      		}
           
           } else {
           	logger.info("No new IN folders to parse");
           }
        } catch (Exception e) {
            logger.severe("Error during Stat XML Data Import: " + e.getMessage());
            e.printStackTrace();
        }
        logger.info("Finished Stat XML Data Import from directory");
        System.out.println("");
        return status;
	}
   
   public void parseData(String atk_dir) {

	Config.getParams();   
	String dirtoparse = null;   
	if (atk_dir == null){
	   	dirtoparse = Config.FFDIR;
	}else{
	  	dirtoparse = atk_dir;
	}  
	   
  	logger.info("TEST: import new Stat Flat-Fixed files ("+Config.FF_EXT+") to ES");
   	int status      = 1;
   	FileHandler  filehandler   = new FileHandler();
    	try {

            if(Boolean.valueOf(Config.FF_PARSESUBDIR)) {
            	logger.info("Parsing with subdirectories ");
            	status = filehandler.parseFilesInSubDir();
            
            } else {
            	logger.info("parsing sindle directory: " + dirtoparse);
            	status = filehandler.parseFilesInDir(dirtoparse); 
            }
            
        } catch (Exception e) {
            logger.severe("Error during Stat FF Data Import: " + e.getMessage());
            e.printStackTrace();
        }
        logger.info("Finished Stat FF Data Import");
        System.out.println("");
		
	}
   
    public void run() {

    	FileHandler  filehandler = new FileHandler();
     	try {
             filehandler.parseData(null);
         } catch (Exception e) {
             e.printStackTrace();
         }
	}
    
    /*
     * schedule (Callable task, long delay, TimeUnit timeunit)
     * schedule (Runnable task, long delay, TimeUnit timeunit)
     * scheduleAtFixedRate (Runnable, long initialDelay, long period, TimeUnit timeunit)
     * scheduleWithFixedDelay (Runnable, long initialDelay, long period, TimeUnit timeunit)
     * 
     */
   
    public static void main(String[] args) {
		//Config.getParams();
		System.out.println("Scaning with frequency " + Config.FFPARSER_TIMEOUT);
        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(new FileHandler(), 0, Config.FFPARSER_TIMEOUT, TimeUnit.SECONDS);
    }
        
	   
	   
}
