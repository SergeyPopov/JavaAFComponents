/**
 * 
 */
package test.af.mcsa.stat.db;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import af.mcsa.stat.db.ConnectionManager;
import af.mcsa.stat.utils.Config;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

/**
 * @author noserpop
 *
 */
public class ConnectionManagerTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		 Config.getParams();
		 Connection connection = null;
		 PreparedStatement preparedStatement = null;
		 Statement statement = null;
		 String sqlstatement = "select true from DUAL";
		 ResultSet resultSet = null;
		 String id = "qwe";
		 ConnectionManager cm = new ConnectionManager();
	}

	/**
	 * @throws java.lang.Exception
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
 */
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		Config.getParams();
		 Connection connection = null;
		 PreparedStatement preparedStatement = null;
		 Statement statement = null;
		 String sqlstatement = "select true from DUAL";
		 ResultSet resultSet = null;
		 String id = "qwe";
		 ConnectionManager cm = new ConnectionManager();
	}

	/**
	 * Test method for {@link af.mcsa.stat.db.ConnectionManager#ConnectionManager()}.
	 */
	@Test
	public void testConnectionManager() {
		 Config.getParams();
		 Connection connection = null;
		 PreparedStatement preparedStatement = null;
		 Statement statement = null;
		 String sqlstatement = "select true from DUAL";
		 ResultSet resultSet = null;
		 String id = "qwe";
		 ConnectionManager cm = new ConnectionManager();
	       try {
	            // The newInstance() call is a work around for some
	            // broken Java implementations
	        	cm.LoadDriver();
	        	connection = DriverManager
	   	          .getConnection(Config.PROJECT_DB_URL +Config.DB_NAME + 
	   	                   "?user="+Config.UserName+ "&password="+Config.Password);
	        	
	        	preparedStatement = (PreparedStatement) connection.prepareStatement(sqlstatement);  
	         	resultSet = preparedStatement.executeQuery();
	         	
	            //Class.forName("com.mysql.jdbc.Driver").newInstance();
	         	if (resultSet.next()) {
	                System.out.println("Connection tested " +resultSet.getString("true"));
	         	}
	         	else{
	         		fail("Select from DUAL has failed");
	         	}
	         		
	        } catch (Exception ex) {
	        	fail(" Select from DUAL has failed, no DB connection");
	        	System.out.println("Failed to obtained MySQL Driver; ");
	            // handle the error
	        	ex.printStackTrace();
	        }

	}

	/**
	 * Test method for {@link af.mcsa.stat.db.ConnectionManager#LoadDriver()}.
	 */
	@Test
	public void testLoadDriver() {
        try {
            // The newInstance() call is a work around for some
            // broken Java implementations

        	Class.forName(Config.JDBC_DRIVER).newInstance();
            System.out.println("MySQL Driver obtained; ");
        } catch (Exception ex) {
        	fail("Failed to obtained MySQL Driver");
        	System.out.println("Failed to obtained MySQL Driver; ");
            // handle the error
        	ex.printStackTrace();
        }
	}

	/**
	 * Test method for {@link no.svv.atkstat.db.ConnectionManager#getConnection()}.
	
	@Test
	public void testGetConnection() {
		fail("Not yet implemented");
	}
	 */
	/**
	 * Test method for {@link no.svv.atkstat.db.ConnectionManager#main(java.lang.String[])}.
	
	@Test
	public void testMain() {
		fail("Not yet implemented");
	}
	*/
}
