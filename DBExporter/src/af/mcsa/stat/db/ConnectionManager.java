package af.mcsa.stat.db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import af.mcsa.stat.utils.Config;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import com.mysql.jdbc.Statement;




import net.jodah.failsafe.RetryPolicy;
import net.jodah.failsafe.CircuitBreaker;
import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.RetryPolicy;

// Notice, do not import com.mysql.jdbc.*
// or you will have problems!

public class ConnectionManager {
	
	
	RetryPolicy retryPolicy = new RetryPolicy()
	  .retryOn(Exception.class)
	  .withDelay(1, TimeUnit.SECONDS)
	  .withMaxRetries(3);
	
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    
	public  ConnectionManager() {}
	

	public void LoadDriver () {
        try {
            // The newInstance() call is a work around for some
            // broken Java implementations

        	Class.forName(Config.JDBC_DRIVER).newInstance();
            System.out.println("MySQL Driver loaded; ");
        } catch (Exception ex) {
        	System.out.println("Failed to load MySQL Driver; ");
            // handle the error
        	ex.printStackTrace();
			LOGGER.severe("Failed to close the connection. Check DB connection parameters: " +ex.getMessage());

        }
       // return drivermanager;
	} 
	
	public  Connection getConnection() {
		Connection connection =null;
		try {
        	 connection = Failsafe.with(retryPolicy).get(() -> connect());
        } catch (Exception ex) {
        	LOGGER.severe("Failed to establish DB Connection; ");
            // handle the error
        	ex.printStackTrace();
			LOGGER.severe("Failed to obtain MySQL Driver. Failed to obtain MySQL Driver: " +ex.getMessage());
        }
        return connection;
	} 
	
	public  Connection connect() {
		Connection connection =null;
		try {
            // The newInstance() call is a work around for some
            // broken Java implementations
        	LoadDriver();
            System.out.println("Getting MySQL Connection for user="+Config.UserName+ "&password="+Config.Password);
        	System.out.println("Obtainig MySQL URL :" + Config.PROJECT_DB_URL);
        	connection = DriverManager
         	          .getConnection(Config.PROJECT_DB_URL +Config.DB_NAME + 
         	                   "?user="+Config.UserName+ "&password="+Config.Password);

        	//Class.forName("com.mysql.jdbc.Driver").newInstance();
            System.out.println("MySQL Driver obtained for user="+Config.UserName+ "&password="+Config.Password);
        } catch (Exception ex) {
        	System.out.println("Failed to obtain MySQL Driver; ");
            // handle the error
        	ex.printStackTrace();
			LOGGER.severe("Failed to obtain MySQL Driver. Failed to obtain MySQL Driver: " +ex.getMessage());
        }
        return connection;
	}  	
	
	public static void main(String[] args) {
		Config.getParams();
		 Connection connection = null;
		 PreparedStatement preparedStatement = null;
		 Statement statement = null;
		 String sqlstatement = "select true from DUAL";
		 ResultSet resultSet = null;
		 String id = "qwe";
		 ConnectionManager cm = new ConnectionManager();
        try {
            // The newInstance() call is a work around for some
            // broken Java implementations
        	cm.LoadDriver();
        	connection = DriverManager
   	          .getConnection(Config.PROJECT_DB_URL +Config.DB_NAME + 
   	                   "?user="+Config.UserName+ "&password="+Config.Password);
        	
        	preparedStatement = (PreparedStatement) connection.prepareStatement(sqlstatement);  
         	resultSet = preparedStatement.executeQuery();
         	
            //Class.forName("com.mysql.jdbc.Driver").newInstance();
         	if (resultSet.next()) {
                System.out.println("Connection tested " +resultSet.getString("true"));
         	}
         	else{
         		System.out.println("Select from DUAL has failed");
         	}
         		
        } catch (Exception ex) {
        	System.out.println("Error in MAIN: Failed to obtain MySQL Driver; ");
            // handle the error
        	ex.printStackTrace();
        }
	}  

 
	
}