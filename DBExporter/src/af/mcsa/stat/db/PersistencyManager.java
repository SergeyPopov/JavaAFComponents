package af.mcsa.stat.db;

import java.math.BigInteger;
import java.sql.Date;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

import javax.ws.rs.WebApplicationException;

import af.mcsa.stat.entity.Case;
import af.mcsa.stat.utils.Config;


public class PersistencyManager {
	
	public  PersistencyManager() {}

    private static Logger logger = Logger.getLogger(
            "no.acando.cristin.db.PersistenceHelper");
    
    /*
     * extraction SAK data using
     * 
        String sql = "SELECT "
    			      + "  datk.maalested.MAALESTED_ID,"
    			      + "  datk.maalested.MAALESTED_KODE,"
    			      + "  datk.maalested.MAALESTED_NAVN,"
    			      + "  datk.maalested.FARTSGRENSE,"
    			      + "  datk.maalested.M_FylkeNr,"
    			      + "  datk.maalested.POLITIDISTRIKT_ID,"
    			      + "  datk.fylke.FYLKE_NAVN,"
    			      + "  datk.politidistrikt.POLITIDISTRIKT_NAVN," 
    			      + "  datk.serie.SERIE_ID,"
    			      + "  datk.serie.SERIENR,"
    			      + "  datk.serie.STARTDATOTID,"
    			      + "  datk.serie.SLUTTDATOTID,"
    			      + "  datk.serie.FARTSGRENSE as SRFARTSGRENSE,"
    			      + "  datk.serie.FARTSGRENSE1 as SRFARTSGRENSE1,"
    			      + "  datk.serie.ANTALLBILDERISERIE,"
    			      + "  datk.sak.SAK_ID,"
    			      + "  sak.DATOTID"
    			      + " FROM "
    			      + "  datk.maalested,"
    			      + "  datk.fylke,"
    			      + "  datk.politidistrikt,"
    			      + "  datk.serie,"
    			      + "  datk.sak"
    			      + " WHERE maalested.MAALESTED_ID   = serie.MAALESTED_ID"
    			      + "   AND fylke.FYLKENR               = maalested.M_FylkeNr" 
    			      + "   AND maalested.POLITIDISTRIKT_ID = politidistrikt.POLITIDISTRIKT_ID" 
    			      + "   AND serie.SERIE_ID           = sak.SERIE_ID"
    			      + "   AND serie.SERIENR            = " + serienr
                      + "   AND maalested.MAALESTED_KODE = " + malerstedcode  
                      + "   AND sak.DATOTID BETWEEN '"+datoidfrom+"' AND '"+datoidto +"' "; 

     */
    public Case getMaleSakFromDb(Connection connection,
    	                             	String serienr,
    		                            String malerstedcode,
    		                            String datoidfrom,
    		                            String datoidto
    		                            )throws Exception {


    	
        logger.info("Selecting sak(Case) from histdata, malerstedcode: " + malerstedcode);

        String sql = "SELECT "
			      + "  dlake.sak.MAALESTED_ID,"
			      + "  dlake.sak.MAALESTED_KODE,"
			      + "  dlake.sak.MAALESTED_NAVN,"
			      + "  dlake.sak.FARTSGRENSE,"
			      + "  dlake.sak.M_FylkeNr,"
			      + "  dlake.sak.POLITIDISTRIKT_ID,"
			      + "  dlake.sak.FYLKE_NAVN,"
			      + "  dlake.sak.POLITIDISTRIKT_NAVN," 
			      + "  dlake.sak.SERIE_ID,"
			      + "  dlake.sak.SERIENR,"
			      + "  dlake.sak.STARTDATOTID,"
			      + "  dlake.sak.SLUTTDATOTID,"
			      + "  dlake.sak.SRFARTSGRENSE  as SRFARTSGRENSE,"
			      + "  dlake.sak.SRFARTSGRENSE1 as SRFARTSGRENSE1,"
			      + "  dlake.sak.ANTALLBILDERISERIE,"
			      + "  dlake.sak.SAK_ID,"
			      + "  dlake.sak.DATOTID"
			      + " FROM "
			      + "  dlake.sak"
			      + " WHERE sak.SERIENR      = " + serienr
                + "   AND sak.MAALESTED_KODE = " + malerstedcode  
                + "   AND sak.DATOTID BETWEEN '" +datoidfrom +"' AND '"+datoidto +"' "; 
    	
         System.out.println("Preparing statement.. ");
         PreparedStatement preparedStatement = connection.prepareStatement(sql);
       
       
        System.out.println("Executing statement.. ");
        ResultSet rs = preparedStatement.executeQuery(sql);
        
        Case mcase = new Case();
        System.out.println("Fetching recorset.. ");
        if(rs.next()) {
        	mcase.setSak_id(new BigInteger(Integer.toString(rs.getInt("SAK_ID"))));
        	mcase.setSerie_id(new BigInteger(Integer.toString(rs.getInt("SERIE_ID"))));
        	mcase.setMaalested_id(new BigInteger(Integer.toString(rs.getInt("MAALESTED_ID"))));        	
        	mcase.setM_fylkenr(new BigInteger(Integer.toString(rs.getInt("M_FylkeNr"))));         	
        	mcase.setFartsgrense(new BigInteger(Integer.toString(rs.getInt("FARTSGRENSE"))));     
        	mcase.setPolitidistrikt_id(new BigInteger(Integer.toString(rs.getInt("POLITIDISTRIKT_ID"))));             	
        	mcase.setSerienr(new BigInteger(Integer.toString(rs.getInt("SERIENR"))));                 	
        	mcase.setAntallbilderiserie(new BigInteger(Integer.toString(rs.getInt("ANTALLBILDERISERIE"))));         	
        	
        	mcase.setMaalested_kode(rs.getString("MAALESTED_KODE"));      	
        	mcase.setMaalested_navn(rs.getString("MAALESTED_NAVN"));      
        	mcase.setFylke_navn(rs.getString("FYLKE_NAVN"));              	
        	mcase.setPolitidistrikt_navn(rs.getString("POLITIDISTRIKT_NAVN"));          	

        	mcase.setSrfartsgrense(rs.getBigDecimal("SRFARTSGRENSE"));
        	mcase.setSrfartsgrense1(rs.getBigDecimal("SRFARTSGRENSE1"));       	
        	
        	mcase.setStartdatotid(rs.getTimestamp("STARTDATOTID"));    
        	mcase.setSluttdatotid(rs.getTimestamp("SLUTTDATOTID"));    
        	
        	if (mcase.getAntallbilderiserie() !=null && mcase.getAntallbilderiserie().longValue()>0){
        		mcase.setFoto_tatt(true);
        	}else{
        		mcase.setFoto_tatt(false);
        	}
        	
    
            logger.info("Detected  SAK_ID in  DATK: " + mcase.getSak_id() + mcase.getStartdatotid().toString());
        } else {
            logger.warning("No SAK found in DATK DB");
        }
        rs.close();
        preparedStatement.close();
        return mcase;
    }  
 
    
    /* --------------------------------------------------------------------------------------------------------------------
     * FOR UNIT TEST ONLY !!!
     * --------------------------------------------------------------------------------------------------------------------
     */
    
    
    public static Timestamp RESTTimestampWithTZDParam( String dateTimeStr, String dateFormatStr ) throws WebApplicationException {
    	SimpleDateFormat df = new SimpleDateFormat( dateFormatStr, Locale.ENGLISH );
    	java.util.Date date;
    	java.sql.Timestamp tmsdate;
        try {
        	//java.util.Date date = new java.util.Date( df.parse( dateTimeStr ).getTime() );
        	
        	 tmsdate = new java.sql.Timestamp( df.parse( dateTimeStr ).getTime() );
        } catch ( final ParseException ex ) {
            throw new WebApplicationException( ex );
        }
        return tmsdate;
    }
    
	public static void main(String[] args) {
		Config.getParams();
		 Connection connection = null;
		 PreparedStatement preparedStatement = null;
		 Statement statement = null;
		 String sqlstatement = "select true from DUAL";
		 ResultSet resultSet = null;
		 String id = "qwe";
		 ConnectionManager cm = new ConnectionManager();
		 PersistencyManager pm = new PersistencyManager();
		
	        int SAK_ID = 0;
		 
	        int MAALESTED_ID = 2;
	        String SERIE_NR = "225";

	        String MAALESTED_KODE = "1640326";
	        String sDATOTIDFROM = "2009-07-02 23:24:42";
	        String sDATOTIDTO   = "2009-07-02 23:24:42";
	        
	        Timestamp DATOTIDFROM = RESTTimestampWithTZDParam(sDATOTIDFROM, "yyy-MM-dd HH:mm:ss");
	        Timestamp DATOTIDTO   = RESTTimestampWithTZDParam(sDATOTIDTO, "yyy-MM-dd HH:mm:ss");
	        
	        
        try {
            // The newInstance() call is a work around for some
            // broken Java implementations
        	cm.LoadDriver();
        	connection = DriverManager
   	          .getConnection(Config.PROJECT_DB_URL +Config.DB_NAME + 
   	                   "?user="+Config.UserName+ "&password="+Config.Password);
        	
        	preparedStatement = (PreparedStatement) connection.prepareStatement(sqlstatement);  
         	resultSet = preparedStatement.executeQuery();
         	
            //Class.forName("com.mysql.jdbc.Driver").newInstance();
         	if (resultSet.next()) {
                System.out.println("Connection tested " +resultSet.getString("true"));
         	}
         	else{
         		System.out.println("Select from DUAL has failed");
         	}
         	
         	pm.getMaleSakFromDb(connection,  SERIE_NR, MAALESTED_KODE, sDATOTIDFROM, sDATOTIDTO);
         	
        } catch (Exception ex) {
        	System.out.println("Failed to obtained MySQL Driver; ");
            // handle the error
        	ex.printStackTrace();
        }
	}  
    
}
