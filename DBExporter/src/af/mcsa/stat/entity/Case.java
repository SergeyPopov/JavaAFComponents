package af.mcsa.stat.entity;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
/*
 * case - sak
 */
public class Case {
	
	protected BigInteger maalested_id;
	protected String     maalested_kode;
	protected String     maalested_navn;
	protected BigInteger fartsgrense;
	protected BigInteger m_fylkenr;
	protected BigInteger politidistrikt_id;
	protected String     fylke_navn;
	protected String     politidistrikt_navn;   
	protected BigInteger serie_id;
	protected BigInteger serienr;
	protected Timestamp  startdatotid;
	protected Timestamp  sluttdatotid;
	protected BigDecimal srfartsgrense;
	protected BigDecimal srfartsgrense1;
	protected boolean    foto_tatt;
	protected BigInteger antallbilderiserie;
	protected BigInteger sak_id;

	public BigInteger getMaalested_id() {
		return maalested_id;
	}
	public void setMaalested_id(BigInteger maalested_id) {
		this.maalested_id = maalested_id;
	}
	public String getMaalested_kode() {
		return maalested_kode;
	}
	public void setMaalested_kode(String maalested_kode) {
		this.maalested_kode = maalested_kode;
	}
	public String getMaalested_navn() {
		return maalested_navn;
	}
	public void setMaalested_navn(String maalested_navn) {
		this.maalested_navn = maalested_navn;
	}
	public BigInteger getFartsgrense() {
		return fartsgrense;
	}
	public void setFartsgrense(BigInteger fartsgrense) {
		this.fartsgrense = fartsgrense;
	}
	public BigInteger getM_fylkenr() {
		return m_fylkenr;
	}
	public void setM_fylkenr(BigInteger m_fylkenr) {
		this.m_fylkenr = m_fylkenr;
	}
	public BigInteger getPolitidistrikt_id() {
		return politidistrikt_id;
	}
	public void setPolitidistrikt_id(BigInteger politidistrikt_id) {
		this.politidistrikt_id = politidistrikt_id;
	}
	public String getFylke_navn() {
		return fylke_navn;
	}
	public void setFylke_navn(String fylke_navn) {
		this.fylke_navn = fylke_navn;
	}
	public String getPolitidistrikt_navn() {
		return politidistrikt_navn;
	}
	public void setPolitidistrikt_navn(String politidistrikt_navn) {
		this.politidistrikt_navn = politidistrikt_navn;
	}
	public BigInteger getSerie_id() {
		return serie_id;
	}
	public void setSerie_id(BigInteger serie_id) {
		this.serie_id = serie_id;
	}
	public BigInteger getSerienr() {
		return serienr;
	}
	public void setSerienr(BigInteger serienr) {
		this.serienr = serienr;
	}
	public Timestamp getStartdatotid() {
		return startdatotid;
	}
	public void setStartdatotid(Timestamp startdatotid) {
		this.startdatotid = startdatotid;
	}
	public Timestamp getSluttdatotid() {
		return sluttdatotid;
	}
	public void setSluttdatotid(Timestamp sluttdatotid) {
		this.sluttdatotid = sluttdatotid;
	}
	public BigDecimal getSrfartsgrense() {
		return srfartsgrense;
	}
	public void setSrfartsgrense(BigDecimal srfartsgrense) {
		this.srfartsgrense = srfartsgrense;
	}
	public BigDecimal getSrfartsgrense1() {
		return srfartsgrense1;
	}
	public void setSrfartsgrense1(BigDecimal srfartsgrense1) {
		this.srfartsgrense1 = srfartsgrense1;
	}
	public boolean isFoto_tatt() {
		return foto_tatt;
	}
	public void setFoto_tatt(boolean foto_tatt) {
		this.foto_tatt = foto_tatt;
	}
	public BigInteger getAntallbilderiserie() {
		return antallbilderiserie;
	}
	public void setAntallbilderiserie(BigInteger antallbilderiserie) {
		this.antallbilderiserie = antallbilderiserie;
	}
	public BigInteger getSak_id() {
		return sak_id;
	}
	public void setSak_id(BigInteger sak_id) {
		this.sak_id = sak_id;
	}

}
