package af.mcsa.stat.utils;


import java.sql.Timestamp;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.ws.rs.WebApplicationException;

public class RESTDateParam {

    // Declare the date format for the parsing to be correct
    private SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd" );
    private java.sql.Date date;


    
    /**
     * This is the default constructor which must take in one string parameter.
     * The parameter is no other than the one passed in through the REST
     * end-point. We'll see it later...
     */
    public RESTDateParam( ) {        
    }
    public void RESTDateFormatParam( String dateStr ) throws WebApplicationException {
        try {
            date = new java.sql.Date( df.parse( dateStr ).getTime() );
        } catch ( final ParseException ex ) {
            // Wrap up any expection as javax.ws.rs.WebApplicationException
            throw new WebApplicationException( ex );
        }
    }
    
    public void RESTTimeParam( String timeStr ) throws WebApplicationException {
        try {
        	java.sql.Time time = new java.sql.Time( df.parse( timeStr ).getTime() );
        } catch ( final ParseException ex ) {
            throw new WebApplicationException( ex );
        }
    }
    
    public void RESTTimestampParam( String timestampStr ) throws WebApplicationException {
        try {
        	java.sql.Timestamp timestamp = new java.sql.Timestamp( df.parse( timestampStr ).getTime() );
        } catch ( final ParseException ex ) {
            throw new WebApplicationException( ex );
        }
    }
    
    public Timestamp RESTTimestampWithTZDParam( String dateTimeStr, String dateFormatStr ) throws WebApplicationException {
    	SimpleDateFormat df = new SimpleDateFormat( dateFormatStr, Locale.ENGLISH );
    	java.util.Date date;
    	java.sql.Timestamp tmsdate;
        try {
        	//java.util.Date date = new java.util.Date( df.parse( dateTimeStr ).getTime() );
        	
        	 tmsdate = new java.sql.Timestamp( df.parse( dateTimeStr ).getTime() );
        } catch ( final ParseException ex ) {
            throw new WebApplicationException( ex );
        }
        return tmsdate;
    }

    /**
     * This is a getter method which returns the parsed date string value as
     * java.sql.Date
     *
     */
    public java.sql.Date getDate() {
        return date;
    }

    /**
     * For convenience of result checking
     */
    @Override
    public String toString() {
        if ( date != null ) {
            return date.toString();
        } else {
            return "";
        }
    }
    
    public static XMLGregorianCalendar  RESTXMLGregorianCalendar (String timestampStr) throws Exception {
        XMLGregorianCalendar xmlCalender=null;
        GregorianCalendar calender = new GregorianCalendar();
        
        String dformat = "yyyy-MM-dd'T'HH:mm:ss";
        //String dformat = "yyyy-MM-dd HH:mm:ss";
        Date  date = new SimpleDateFormat(dformat, Locale.ENGLISH).parse(timestampStr);
        calender.setTime(date);
        xmlCalender = DatatypeFactory.newInstance().newXMLGregorianCalendar(calender);
        return xmlCalender;
     }
  
    public static long RESTMillsinseEpoch( String dateStr ) throws WebApplicationException {
    	long timeInMillis;
    	try {
            String dformat = "yyyy-MM-dd'T'HH:mm:ss";
            //String dformat = "yyyy-MM-dd HH:mm:ss";
            
    	    SimpleDateFormat df = new SimpleDateFormat( dformat, Locale.ENGLISH);
    		java.sql.Date date = new java.sql.Date( df.parse( dateStr ).getTime() );

            timeInMillis = date.getTime();
        } catch ( final ParseException ex ) {
            // Wrap up any expection as javax.ws.rs.WebApplicationException
            throw new WebApplicationException( ex );
        }
    	return timeInMillis;
    }
    
    
    public static String convertTimeWithTimeZome(long time){
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone("CET"));
        cal.setTimeInMillis(time);
        return (cal.get(Calendar.YEAR) + " " + (cal.get(Calendar.MONTH) + 1) + " " 
                + cal.get(Calendar.DAY_OF_MONTH) + " " + cal.get(Calendar.HOUR_OF_DAY) + ":"
                + cal.get(Calendar.MINUTE));

    }
    
    public String convertLongTime(long timeInMillis){
        return new Timestamp(timeInMillis).toString();
    }
    
    public static void main(String[] args) throws Exception {
    	
		 RESTDateParam rdp = new RESTDateParam();
    	//"2016-02-01T06:12:55+0800" "01/Feb/2016:06:12:55 +0000"  "01/Oct/2015:00:01:57 +0000"
    	//"yyy-MM-dd'T'HH:mm:ssZ"  "dd/MM/yyyy:HH:mm:ssz"
    	//"2009-07-02 23:24:42", "yyyy-MM-dd HH:mm:ss"
	    String dateStr = "2009-07-02 23:24:42";
	    
	    //XMLGregorianCalendar d = RESTXMLGregorianCalendar(dateStr);
	    //System.out.println("Parsed date: "+ d.toString());
	    
	    long timeInMillis = RESTMillsinseEpoch(dateStr);
	    System.out.println("Parsed date: "+ Long.toString(timeInMillis));
	    System.out.println("Parsed long date: "+ rdp.convertLongTime(timeInMillis -1000));

	    System.out.println("Parsed long date with timezone: "+ convertTimeWithTimeZome(timeInMillis));
	    
	    Date date = new Date(timeInMillis);
	    Format format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    System.out.println("Parsed long date:" + format.format(date));
	    
	    /*
	    SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        try {
        	java.sql.Timestamp date = new java.sql.Timestamp( df.parse( dateStr ).getTime() );
        	//java.util.Date date = new java.sql.Timestamp( df.parse( dateStr ).getTime() );
        	System.out.println("Parsed date: "+ date.toString());
        } catch ( final ParseException ex ) {
            // Wrap up any expection as javax.ws.rs.WebApplicationException
            throw new WebApplicationException( ex );
        };
        */
    }
    
    
}