package af.mcsa.stat.db;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import af.mcsa.stat.utils.Config;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;

public class ESPostHttpClient {
	
	
	public  ESPostHttpClient() {}
	
	public int postJsonObject(Object json, String esindex) {
		 //  .UTF_8
		String es_index = null;
		int status      = 1;
		
        String es_url = null;	
        if(esindex == null){
              es_index = Config.ES_MALINGINDEX;
        }else{
     	   es_index = esindex;
        }
        //(CloseableHttpClient httpClient = HttpClientBuilder.create().build())
        try {
	           es_url = Config.ES_LOCALHTTP_URL + es_index;
 			   System.out.println("Preparing connection to URL: " + es_url);
	           HttpParams httpParams = new BasicHttpParams();
	           HttpProtocolParams.setContentCharset(httpParams, HTTP.UTF_8);
	           HttpProtocolParams.setHttpElementCharset(httpParams, HTTP.UTF_8);
	           HttpClient client = new DefaultHttpClient(httpParams);
	           HttpPost request = new HttpPost(es_url);
	           StringEntity str = null;
	           Gson gson = new Gson();
	           String jsonString = gson.toJson(json);
	          
	           str = new StringEntity(jsonString, HTTP.UTF_8);
	           System.out.println("Posting :" + str);
	           request.setEntity(str);
	           request.setHeader("Accept", "application/json");
	           request.setHeader("Content-type", "application/json");
	           HttpResponse response = client.execute(request);   

	           if (response.getStatusLine().getStatusCode() != 201) {
		            throw new RuntimeException("Failed : HTTP error code : "
		                + response.getStatusLine().getStatusCode());
		       }else{
		    	   status = 0;
		       }
	
	           BufferedReader br = new BufferedReader(
                        new InputStreamReader((response.getEntity().getContent())));	           
	           String output;
		        System.out.println("Output from Server .... \n");
		        while ((output = br.readLine()) != null) {
		            System.out.println(output);
		        }
		        
		        
	      } catch (Exception e) {
	            status = 1;
		        e.printStackTrace();
          }
        return status;
	  }

	public void postJsonString(String json, String esindex) {

		String es_index = null;
		try{
	        	
	           String es_url = null;	
	           if(esindex == null){
	                 es_index = Config.ES_MALINGINDEX;
	           }else{
	        	   es_index = esindex;
	           }
	        
  
	        es_url = Config.ES_LOCALHTTP_URL + es_index;
	        DefaultHttpClient httpClient = new DefaultHttpClient();
	        HttpPost postRequest = new HttpPost(es_url);

	        StringEntity input = new StringEntity(json);
	        input.setContentType("application/json");
	        //input.setContentEncoding("UTF-8");
	        postRequest.setEntity(input);

	        HttpResponse response = httpClient.execute(postRequest);

	        if (response.getStatusLine().getStatusCode() != 201) {
	            throw new RuntimeException("Failed : HTTP error code : "
	                + response.getStatusLine().getStatusCode());
	
	        }

	        BufferedReader br = new BufferedReader(
	                        new InputStreamReader((response.getEntity().getContent())));

	        String output;
	        System.out.println("Output from Server .... \n");
	        while ((output = br.readLine()) != null) {
	            System.out.println(output);
	        }
	        httpClient.close();
	        httpClient.getConnectionManager().shutdown();

	      } catch (Exception e) {
	    	
	        e.printStackTrace();

	      } 

	  }

	
}
