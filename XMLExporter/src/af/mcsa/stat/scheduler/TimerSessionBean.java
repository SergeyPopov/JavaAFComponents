package af.mcsa.stat.scheduler;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;

import af.mcsa.stat.adapters.FileHandler;

@Singleton
public class TimerSessionBean {
    @Resource
    TimerService timerService;
    FileHandler  filehandler = new FileHandler();

    private Date lastProgrammaticTimeout;
    private Date lastAutomaticTimeout;
    
    private Logger logger = Logger.getLogger(
            "no.svv.atkstat.scheduler.TimerSessionBean");
    
    public void setTimer(long intervalDuration) {
        logger.info("Setting a programmatic timeout for "
                + intervalDuration + " milliseconds from now.");
        Timer timer = timerService.createTimer(intervalDuration, 
                "Created new programmatic timer");
    }
    
    @Timeout
    public void programmaticTimeout(Timer timer) {
        this.setLastProgrammaticTimeout(new Date());
        logger.info("Programmatic timeout occurred.");
    }

    //
    @Schedule(minute="*/1", hour="*")
    public void automaticTimeout() {
        this.setLastAutomaticTimeout(new Date());
        logger.info("Automatic timeout occured");
        System.out.println("");
        logger.info("Starting ATK XML Data Import");
        
        try {
           
        	logger.info("TEST: import new ATK XML files to import");
           /*
           List<String> atkxmlfiles = filehandler.exprortATKDatafromDir();

            if(atkxmlfiles.size() > 0) {
                logger.info("Imported " + atkxmlfiles.size() + " new ATK XML file(s) ");
            } else {
                logger.info("No new ATK XML files to import");
            }
            */
        } catch (Exception e) {
            logger.severe("Error during ATK XML Data Import: " + e.getMessage());
            e.printStackTrace();
        }
        logger.info("Finished ATK XML Data Import");
        System.out.println("");
        
    }

    public String getLastProgrammaticTimeout() {
        if (lastProgrammaticTimeout != null) {
            return lastProgrammaticTimeout.toString();
        } else {
            return "never";
        }
        
    }

    public void setLastProgrammaticTimeout(Date lastTimeout) {
        this.lastProgrammaticTimeout = lastTimeout;
    }

    public String getLastAutomaticTimeout() {
        if (lastAutomaticTimeout != null) {
            return lastAutomaticTimeout.toString();
        } else {
            return "never";
        }
    }

    public void setLastAutomaticTimeout(Date lastAutomaticTimeout) {
        this.lastAutomaticTimeout = lastAutomaticTimeout;
    }
    

    public  void main (String args[]) throws  IOException {	
 
    	try {
    		this.automaticTimeout();
        } catch (Exception e) {

            e.printStackTrace();
        }

    }
    
}