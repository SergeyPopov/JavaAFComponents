package af.mcsa.stat.core.circuitbreaker;

import org.springframework.integration.Message;

public class NoOpCircuitBreakerOpenHandler implements CircuitBreakerOpenHandler {
    @Override
    public Object handle(String system, Object target, Message<?> message) {
        return message;
    }
}