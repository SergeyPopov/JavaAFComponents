package af.mcsa.stat.core.circuitbreaker;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.Message;
import org.springframework.jmx.export.DynamicMBeanExporter;

import af.mcsa.stat.core.circuitbreaker.RequestStatus;
import af.mcsa.stat.core.config.SystemConfiguration;
import af.mcsa.stat.core.metrics.MetricsService;
import af.mcsa.stat.core.metrics.MetricType;

public abstract class AbstractSuccessFailCircuitBreakerAdvice extends AbstractCircuitBreakerAdvice {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractSuccessFailCircuitBreakerAdvice.class);

    protected static final int BUCKET_SIZE = 5000; // ms

    private static final String CURRENT_STATE = "current_state";
    private static final String CURRENT_FAILURE_PERCENTAGE = "current_failure_percentage";
    private static final String MESSAGE_SUCCESS_RATE = "message_success_rate";
    private static final String MESSAGE_FAILURE_RATE = "message_failure_rate";
    private static final String REJECTED_MESSAGES_RATE = "rejected_messages_rate";

    @SuppressWarnings("serial")
    private static final Map<String, MetricType> METRICS_TO_REPORT = new HashMap<String, MetricType>() {
        {
            put(CURRENT_FAILURE_PERCENTAGE, MetricType.GAUGE);
            put(CURRENT_STATE, MetricType.GAUGE);
            put(MESSAGE_SUCCESS_RATE, MetricType.COUNTER);
            put(MESSAGE_FAILURE_RATE, MetricType.COUNTER);
            put(REJECTED_MESSAGES_RATE, MetricType.COUNTER);

        }
    };

    @SuppressWarnings("serial")
    private static final List<String> MONITORED_EXCEPTIONS = new ArrayList<String>() {
        {
            add("com.lgi.orion.oesp.exception.ExternalSystemUnavailableException");
            add("com.lgi.orion.oesp.exception.ExternalSystemUnexpectedResponseException");
            add("com.lgi.orion.oesp.exception.ExternalSystemException"); // timeouts
        }
    };

    private static final long OPEN_STATE = 2;
    private static final long HALF_OPEN_STATE = 1;
    private static final long CLOSED_STATE = 0;

    @Autowired
    private DynamicMBeanExporter mbeanExporter;

    @Autowired
    private MetricsService metricsService;

    private final Object lock = new Object();

    private ConcurrentMap<String, CircuitBreaker> circuitBreakersByCountry =
            new ConcurrentHashMap<String, CircuitBreaker>();

    private ConcurrentMap<String, CircuitBreakerJmxStats> statsByCountry =
            new ConcurrentHashMap<String, CircuitBreakerJmxStats>();

    private Map<String, Map<String, String>> metricsRegister = new HashMap<String, Map<String, String>>();

    private int halfOpenDelay;
    private CircuitBreakerPolicy circuitBreakerPolicy;
    private CircuitBreakerOpenHandler circuitBreakerOpenHandler;

    /**
     * Subclasses are required to initialise the physical circuit breaker setting up the required policy and
     * open handler.
     * @param systemConfiguration system config instance for breaker settings.
     */
    public abstract void initialiseCircuitBreakerSettings(SystemConfiguration systemConfiguration);

    protected AbstractSuccessFailCircuitBreakerAdvice(String remoteSystemName) {
        super(remoteSystemName);
    }

    @PostConstruct
    void init() {
        final SystemConfiguration systemConfiguration = getSystemConfiguration();
        if (systemConfiguration.getCircuitBreakersEnabled()) {
            initialiseCircuitBreakerSettings(systemConfiguration);
            setupCircuitBreakers();
            registerJMXStats();

            if (systemConfiguration.getMetricsEnabled()) {
                registerOESPMetrics();
            }
        }
    }

    protected Object invokeWithCircuitBreakerAdvice(ExecutionCallback callback, Object target, Message<?> message)
            throws Exception {

        final Object result;
        final String countryCode = (String) message.getHeaders().get("countryCode");

        CircuitBreaker circuitBreaker = getBreaker(countryCode);
        CircuitBreakerJmxStats stats = getCircuitBreakerJmxStats(countryCode);

        stats.getTotalMessagesHandled().incrementAndGet();
        stats.setFailureLevel(circuitBreakerPolicy.getFailureLevel(countryCode));
        stats.setFlowRatePerMinute(circuitBreakerPolicy.getFlowRate(countryCode));
        stats.setFailureLevelLastReported(new Date());

        reportMetric(countryCode, CURRENT_FAILURE_PERCENTAGE, stats.getFailureLevel().get());

        LOGGER.debug("In advice for {}", getRemoteSystemName());

        if ((circuitBreaker.isClosed() && circuitBreakerPolicy.allowsRequest(countryCode))
                || singleMessageAllowed(circuitBreaker)) {
            try {
                result = callback.execute();
                handleSuccess(countryCode, circuitBreaker);
                reportMetric(countryCode, MESSAGE_SUCCESS_RATE, 1);
                return result;
            } catch (Exception e) {
                Throwable oespException = findOespException(e);
                String oespExceptionName = oespException.getClass().getName();

                reportMetric(countryCode, MESSAGE_FAILURE_RATE, 1);

                LOGGER.debug("Handling {}", oespExceptionName);

                if (MONITORED_EXCEPTIONS.contains(oespExceptionName)) {
                    LOGGER.debug("Exception {} is monitored, updating policy", oespExceptionName);
                    handleFailure(countryCode, circuitBreaker);
                }
                LOGGER.debug("Rethrowing {}", oespExceptionName);
                throw e;
            }
        }

        reportMetric(countryCode, REJECTED_MESSAGES_RATE, 1);

        handleBreakerOpen(countryCode, circuitBreaker);
        return circuitBreakerOpenHandler.handle(getRemoteSystemName(), target, message);
    }

    private boolean singleMessageAllowed(CircuitBreaker circuitBreaker) {
        synchronized (lock) {
            if (circuitBreaker.isOpen()) {
                if (isTimeUntilHalfOpenReached(circuitBreaker)) {
                    reportMetric(circuitBreaker.getCountryCode(), CURRENT_STATE, HALF_OPEN_STATE);
                    if (!circuitBreaker.singleMessageSent()) {
                        circuitBreaker.setSingleMessageSent(true);
                        circuitBreaker.setTimeUntilHalfOpen(System.currentTimeMillis() + halfOpenDelay);
                        return true;
                    }
                }
            }
            return false;
        }
    }

    private void handleBreakerOpen(String countryCode, CircuitBreaker circuitBreaker) {
        synchronized (lock) {
            CircuitBreakerJmxStats stats = getCircuitBreakerJmxStats(countryCode);
            if (!circuitBreaker.isOpen()) {
                reportMetric(circuitBreaker.getCountryCode(), CURRENT_STATE, OPEN_STATE);

                stats.setCurrentState("OPEN");
                stats.setLastOpenTime(new Date(System.currentTimeMillis()));
                stats.getOpenCount().incrementAndGet();

                circuitBreaker.setIsOpen();
                circuitBreaker.setTimeUntilHalfOpen(System.currentTimeMillis() + halfOpenDelay);
            }

            stats.getRequestsRejectedDuringLastOpenPeriod().incrementAndGet();

        }
    }

    private CircuitBreakerJmxStats getCircuitBreakerJmxStats(String countryCode) {
        return statsByCountry.get(countryCode);
    }


    private CircuitBreaker getBreaker(String countryCode) {
        return circuitBreakersByCountry.get(countryCode);
    }

    private void handleFailure(String countryCode, CircuitBreaker stateForSystemByCountry) {
        handleSingleMessage(RequestStatus.FAILURE, stateForSystemByCountry);
        circuitBreakerPolicy.recordStatus(countryCode, RequestStatus.FAILURE);
    }

    private void handleSuccess(String countryCode, CircuitBreaker stateForSystemByCountry) {
        handleSingleMessage(RequestStatus.SUCCESS, stateForSystemByCountry);
        circuitBreakerPolicy.recordStatus(countryCode, RequestStatus.SUCCESS);
    }

    private boolean isTimeUntilHalfOpenReached(CircuitBreaker state) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("{}: Time  to half open {} ms", getRemoteSystemName(),
                    state.getTimeUntilHalfOpen() - System.currentTimeMillis());
        }

        return System.currentTimeMillis() >= state.getTimeUntilHalfOpen();
    }


    private void handleSingleMessage(RequestStatus status, CircuitBreaker breaker) {
        synchronized (lock) {
            if (breaker.singleMessageSent()) {
                breaker.setSingleMessageSent(false);

                if (status == RequestStatus.SUCCESS) {
                    CircuitBreakerJmxStats stats = getCircuitBreakerJmxStats(breaker.getCountryCode());
                    stats.setCurrentState("CLOSED");
                    stats.setLastOpenDurationInMillis(System.currentTimeMillis() - stats.getLastOpenTime().getTime());

                    reportMetric(breaker.getCountryCode(), CURRENT_STATE, CLOSED_STATE);

                    breaker.setIsClosed();
                    circuitBreakerPolicy.clearStatus(breaker.getCountryCode());
                } else {
                    CircuitBreakerJmxStats stats = getCircuitBreakerJmxStats(breaker.getCountryCode());
                    stats.getOpenCount().incrementAndGet();
                    stats.setLastOpenTime(new Date(System.currentTimeMillis()));

                    reportMetric(breaker.getCountryCode(), CURRENT_STATE, OPEN_STATE);

                    breaker.setIsOpen();
                    breaker.setTimeUntilHalfOpen(System.currentTimeMillis() + halfOpenDelay);
                }
            }
        }
    }

    protected void setCircuitBreakerPolicy(CircuitBreakerPolicy circuitBreakerPolicy) {
        this.circuitBreakerPolicy = circuitBreakerPolicy;
    }

    protected void setCircuitBreakerOpenHandler(CircuitBreakerOpenHandler circuitBreakerOpenHandler) {
        this.circuitBreakerOpenHandler = circuitBreakerOpenHandler;
    }

    protected void setHalfOpenDelay(int halfOpenDelay) {
        this.halfOpenDelay = halfOpenDelay;
    }

    protected void registerJMXStats() {
        for (String countryCode : getSystemConfiguration().getCountryCodes()) {
            CircuitBreakerJmxStats stats = new CircuitBreakerJmxStats(countryCode);
            statsByCountry.put(countryCode, stats);
            mbeanExporter.registerBeanNameOrInstance(stats,
                    "com.lgi.orion.oesp.circuitbreaker:name=" + countryCode + " " + getRemoteSystemName());

        }
    }

    protected void registerOESPMetrics() {
        for (String countryCode : getSystemConfiguration().getCountryCodes()) {

            Map<String, String> metricsMap = new HashMap<String, String>();
            for (String circuitBreakerMetricName : METRICS_TO_REPORT.keySet()) {
                String metricsServiceKey =
                        metricsService.nameBuilder().withParts(COMPONENT_NAME).withCountry(countryCode)
                                .withParts(getRemoteSystemName()).withName(circuitBreakerMetricName).build();
                metricsMap.put(circuitBreakerMetricName, metricsServiceKey);
            }
            metricsRegister.put(countryCode, metricsMap);

        }
    }

    protected void setupCircuitBreakers() {
        for (String countryCode : getSystemConfiguration().getCountryCodes()) {
            CircuitBreaker breaker = new CircuitBreaker(countryCode);
            circuitBreakersByCountry.put(countryCode, breaker);
        }
    }

    private void reportMetric(String countryCode, String name, long value) {
        if (getSystemConfiguration().getMetricsEnabled()) {
            MetricType type = METRICS_TO_REPORT.get(name);
            String metricName = metricsRegister.get(countryCode).get(name);

            if (type == MetricType.GAUGE) {
                metricsService.updateGauge(metricName, value);
                return;
            }

            if (type == MetricType.COUNTER) {
                metricsService.incrementCounter(metricName, value);
            }

        }
    }
}
