package af.mcsa.stat.core.circuitbreaker;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

public class CircuitBreaker {

    private final AtomicLong timeUntilHalfOpen;
    private final AtomicBoolean breakerIsOpen;
    private final AtomicBoolean singleMessageSent;
    private final String countryCode;

    /**
     * Creates a new instance.
     * @param countryCode - the countryCode.
     */
    public CircuitBreaker(String countryCode) {
        this.timeUntilHalfOpen = new AtomicLong(0L);
        this.breakerIsOpen = new AtomicBoolean(false);
        this.singleMessageSent = new AtomicBoolean(false);
        this.countryCode = countryCode;
    }

    /**
     * Is the breaker open.
     * @return true if it is otherwise false.
     */
    public boolean isOpen() {
        return breakerIsOpen.get();
    }

    /**
     * Is the breaker closed.
     * @return true if it is otherwise false.
     */
    public boolean isClosed() {
        return !breakerIsOpen.get();
    }

    /**
     * Gets singleMessageSent.
     * @return Value of singleMessageSent.
     */
    public boolean singleMessageSent() {
        return singleMessageSent.get();
    }

    /**
     * Sets new singleMessageSent.
     * @param sent New value of singleMessageSent.
     */
    public void setSingleMessageSent(boolean sent) {
        this.singleMessageSent.set(sent);
    }

    /**
     * Set the breaker 'open' state.
     */
    public void setIsOpen() {
        this.breakerIsOpen.set(true);
    }

    /**
     * Set the breaker 'open' state.
     */
    public void setIsClosed() {
        this.breakerIsOpen.set(false);
    }

    /**
     * Gets countryCode.
     * @return Value of countryCode.
     */
    public String getCountryCode() {
        return countryCode;
    }


    /**
     * Sets new timeUntilHalfOpen.
     * @param timeUntilHalfOpen New value of timeUntilHalfOpen.
     */
    public void setTimeUntilHalfOpen(long timeUntilHalfOpen) {
        this.timeUntilHalfOpen.set(timeUntilHalfOpen);
    }

    /**
     * Gets timeUntilHalfOpen.
     * @return Value of timeUntilHalfOpen.
     */
    public long getTimeUntilHalfOpen() {
        return timeUntilHalfOpen.get();
    }
}

