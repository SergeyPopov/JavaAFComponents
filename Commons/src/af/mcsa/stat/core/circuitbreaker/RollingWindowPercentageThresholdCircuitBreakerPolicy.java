package af.mcsa.stat.core.circuitbreaker;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import af.mcsa.stat.core.circuitbreaker.RequestStatus;
import com.netflix.hystrix.strategy.properties.HystrixProperty;
import com.netflix.hystrix.util.HystrixRollingNumber;
import com.netflix.hystrix.util.HystrixRollingNumberEvent;


public class RollingWindowPercentageThresholdCircuitBreakerPolicy {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(RollingWindowPercentageThresholdCircuitBreakerPolicy.class);
    private static final int SIXTY_SECONDS_MS = 60000;

    private final int percentageThreshold;

    private final int timeWindow;

    private final int bucketSize;

    private final String remoteSystemName;

    private ConcurrentMap<String, CircuitBreakerCounts> rollingCountsByCountry =
            new ConcurrentHashMap<String, CircuitBreakerCounts>();

    private final Object lock = new Object();


    /**
     * Creates new instance and initialise with the instance specific values required. Values will vary across adapters.
     * @param remoteSystemName    the remote system for which this circuit breaker policy is applied.
     * @param percentageThreshold the failure level at which the breaker will open
     * @param timePeriod          the rolling time period across which success/failure stats are monitored.
     * @param bucketsize          the time interval in ms across which success/fail counts are tracked.
     */
    public RollingWindowPercentageThresholdCircuitBreakerPolicy(String remoteSystemName, int percentageThreshold,
                                                                int timePeriod,
                                                                int bucketsize) {
        Assert.state(percentageThreshold > 0, "The threshold value must be greater than 0");
        Assert.state(timePeriod > 0, "The time period  must be greater than 0");
        this.percentageThreshold = percentageThreshold;
        this.timeWindow = timePeriod;
        this.bucketSize = bucketsize; // needed by Hystrix
        this.remoteSystemName = remoteSystemName;
    }

    //@Override
    public boolean allowsRequest(String countryCode) {
        synchronized (lock) {
            boolean permitted;

            permitted = rateIsBelowThreshold(countryCode);

            logStatus(permitted ? "permitted" : "denied");

            return permitted;
        }
    }

    //@Override
    public void recordStatus(String countryCode, RequestStatus requestStatus) {
        synchronized (lock) {
            CircuitBreakerCounts circuitBreakerCounts = rollingCountsByCountry.get(countryCode);

            HystrixRollingNumber rollingNumber;

            if (circuitBreakerCounts == null) {
                rollingNumber = new HystrixRollingNumber(HystrixProperty.Factory.asProperty(timeWindow),
                        HystrixProperty.Factory.asProperty(timeWindow / bucketSize));
                circuitBreakerCounts = new CircuitBreakerCounts(countryCode, rollingNumber);
                rollingCountsByCountry.put(countryCode, circuitBreakerCounts);
            } else {
                rollingNumber = circuitBreakerCounts.getCounts();
            }

            if (isSuccess(requestStatus)) {
                rollingNumber.increment(HystrixRollingNumberEvent.SUCCESS);
            } else {
                rollingNumber.increment(HystrixRollingNumberEvent.FAILURE);
            }
        }
    }

    //@Override
    public void clearStatus(String countryCode) {
        CircuitBreakerCounts circuitBreakerCounts = rollingCountsByCountry.get(countryCode);
        if (circuitBreakerCounts != null) {
            circuitBreakerCounts.getCounts().reset();
        }
    }

    //@Override
    public long getFailureLevel(String countryCode) {
        return calculateCurrentFailureRate(countryCode);
    }

    //@Override
    public long getFlowRate(String countryCode) {
        return calculateFlowRate(countryCode);
    }

    boolean rateIsBelowThreshold(String countryCode) {
        return calculateCurrentFailureRate(countryCode) < percentageThreshold;
    }

    private long calculateCurrentFailureRate(String countryCode) {
        CircuitBreakerCounts circuitBreakerCounts = rollingCountsByCountry.get(countryCode);
        if (circuitBreakerCounts == null) {
            return 0;
        }

        HystrixRollingNumber rollingNumber = circuitBreakerCounts.getCounts();
        long successes = rollingNumber.getRollingSum(HystrixRollingNumberEvent.SUCCESS);
        long failures = rollingNumber.getRollingSum(HystrixRollingNumberEvent.FAILURE);

        float flowRate = calculateFlowRate(countryCode);

        long failurePercentage = failures > 0 ? failures * 100 / (successes + failures) : 0;

        logStats(rollingNumber, failurePercentage, flowRate);

        return failurePercentage;
    }

    private long calculateFlowRate(String countryCode) {
        CircuitBreakerCounts circuitBreakerCounts = rollingCountsByCountry.get(countryCode);
        if (circuitBreakerCounts == null) {
            return 0;
        }

        HystrixRollingNumber counts = circuitBreakerCounts.getCounts();
        long successes = counts.getRollingSum(HystrixRollingNumberEvent.SUCCESS);
        long failures = counts.getRollingSum(HystrixRollingNumberEvent.FAILURE);
        long totalInTimeWindow = successes + failures;
        int timeWindowsPerMinute = SIXTY_SECONDS_MS / timeWindow;
        return totalInTimeWindow * timeWindowsPerMinute;
    }


    private boolean isSuccess(RequestStatus requestStatus) {
        return requestStatus == RequestStatus.SUCCESS;
    }


    private void logStats(HystrixRollingNumber counts, long failurePercentage, float flowRate) {
        if (LOGGER.isDebugEnabled()) {
            long[] s = counts.getValues(HystrixRollingNumberEvent.SUCCESS);
            long[] f = counts.getValues(HystrixRollingNumberEvent.FAILURE);
            LOGGER.debug("{}: f {} ", remoteSystemName, f);
            LOGGER.debug("{}: s {} ", remoteSystemName, s);
            LOGGER.debug("{}: r {} / min ", remoteSystemName, flowRate);
            LOGGER.debug("{}: percentage {}", remoteSystemName, failurePercentage);
        }
    }

    private void logStatus(String msg) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("{}: request is {} by policy", remoteSystemName, msg);
        }
    }

}

