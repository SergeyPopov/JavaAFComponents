package af.mcsa.stat.core.circuitbreaker;


import org.springframework.integration.Message;

import af.mcsa.stat.core.exception.CircuitBreakerOpenException;

public class DefaultCircuitBreakerOpenHandler implements CircuitBreakerOpenHandler {

    private static final String ERROR_MSG = "System Unavailable";

    @Override
    public Object handle(String system, Object target, Message<?> message) {
        throw new CircuitBreakerOpenException(system, ERROR_MSG);
    }
}
