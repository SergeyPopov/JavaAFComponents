package af.mcsa.stat.core.circuitbreaker;

public enum RequestStatus {

    /**
     * A successful request.
     */
    SUCCESS,

    /**
     * A failing request.
     */
    FAILURE

}