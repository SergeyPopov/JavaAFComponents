package af.mcsa.stat.core.circuitbreaker;

import java.io.Serializable;
import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;

public class CircuitBreakerJmxStats implements Serializable {

    private static final long serialVersionUID = 7695974960088937267L;

    private String currentState = "CLOSED";
    private final String countryCode;
    private final AtomicLong flowRatePerMinute = new AtomicLong();
    private final AtomicLong failureLevel = new AtomicLong();
    private Date failureLevelLastReported;
    private final AtomicLong openCount = new AtomicLong();
    private final AtomicLong lastOpenDurationInMillis = new AtomicLong();
    private Date lastOpenTime;
    private final AtomicLong requestsRejectedDuringLastOpenPeriod = new AtomicLong();
    private final AtomicLong totalMessagesHandled = new AtomicLong();

    /**
     * Create a new instance.
     * @param countryCode the country code.
     */
    public CircuitBreakerJmxStats(String countryCode) {
        this.countryCode = countryCode;
    }


    /**
     * Gets flowRate.
     * @return Value of flowRate.
     */
    @ManagedAttribute
    public AtomicLong getFlowRatePerMinute() {
        return flowRatePerMinute;
    }

    /**
     * Gets totalMessagesHandled.
     * @return Value of totalMessagesHandled.
     */
    @ManagedAttribute
    public AtomicLong getTotalMessagesHandled() {
        return totalMessagesHandled;
    }

    /**
     * Gets currentState.
     * @return Value of currentState.
     */
    @ManagedAttribute
    public String getCurrentState() {
        return currentState;
    }

    /**
     * Gets lastOpenTime.
     * @return Value of lastOpenTime.
     */
    @ManagedAttribute
    public Date getLastOpenTime() {
        return lastOpenTime;
    }

    /**
     * Gets currentFailureLevel.
     * @return Value of currentFailureLevel.
     */
    @ManagedAttribute
    public AtomicLong getFailureLevel() {
        return failureLevel;
    }

    /**
     * Gets countryCode.
     * @return Value of countryCode.
     */
    @ManagedAttribute
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Gets openCount.
     * @return Value of openCount.
     */
    @ManagedAttribute
    public AtomicLong getOpenCount() {
        return openCount;
    }

    /**
     * Gets lastOpenDurationInMillis.
     * @return Value of lastOpenDurationInMillis.
     */
    @ManagedAttribute
    public AtomicLong getLastOpenDurationInMillis() {
        return lastOpenDurationInMillis;
    }

    /**
     * Gets requestsRejectedDuringLastOpenPeriod.
     * @return Value of requestsRejectedDuringLastOpenPeriod.
     */
    @ManagedAttribute
    public AtomicLong getRequestsRejectedDuringLastOpenPeriod() {
        return requestsRejectedDuringLastOpenPeriod;
    }

    /**
     * Gets failureLevelLastReported.
     * @return Value of failureLevelLastReported.
     */
    @ManagedAttribute
    public Date getFailureLevelLastReported() {
        return failureLevelLastReported;
    }

    /**
     * Sets new lastOpenTime.
     * @param lastOpenTime New value of lastOpenTime.
     */
    public void setLastOpenTime(Date lastOpenTime) {
        this.lastOpenTime = lastOpenTime;
    }

    /**
     * Set the current state of the breaker.
     * @param state the state Open or Closed.
     */
    public void setCurrentState(String state) {
        this.currentState = state;
    }

    /**
     * Set the last open duration.
     * @param lastOpenDurationInMillis the duration.
     */
    public void setLastOpenDurationInMillis(long lastOpenDurationInMillis) {
        this.lastOpenDurationInMillis.set(lastOpenDurationInMillis);
    }

    /**
     * Set the failure level.
     * @param failureLevel the failure level.
     */
    public void setFailureLevel(long failureLevel) {
        this.failureLevel.set(failureLevel);
    }

    /**
     * Sets new failureLevelLastReported.
     * @param failureLevelLastReported New value of failureLevelLastReported.
     */
    public void setFailureLevelLastReported(Date failureLevelLastReported) {
        this.failureLevelLastReported = failureLevelLastReported;
    }

    /**
     * Set the message flow rate in messages per minute through the breaker.
     * @param flowRatePerMinute the rate.
     */
    public void setFlowRatePerMinute(long flowRatePerMinute) {
        this.flowRatePerMinute.set(flowRatePerMinute);
    }
}
