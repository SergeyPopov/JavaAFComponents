package af.mcsa.stat.core.circuitbreaker;

import com.netflix.hystrix.util.HystrixRollingNumber;

public class CircuitBreakerCounts {


    private String countryCode;
    private HystrixRollingNumber counts;

    /**
     * Create a new instance.
     * @param countryCode countryCode
     * @param counts      counts
     */
    public CircuitBreakerCounts(String countryCode, HystrixRollingNumber counts) {
        this.countryCode = countryCode;
        this.counts = counts;
    }


    /**
     * Gets counts.
     * @return Value of counts.
     */
    HystrixRollingNumber getCounts() {
        return counts;
    }

    /**
     * Gets countryCode.
     * @return Value of countryCode.
     */
    protected String getCountryCode() {
        return countryCode;
    }

    /**
     * Sets new countryCode.
     * @param countryCode New value of countryCode.
     */
    protected void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
