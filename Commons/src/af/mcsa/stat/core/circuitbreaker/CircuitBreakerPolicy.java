package af.mcsa.stat.core.circuitbreaker;

import af.mcsa.stat.core.circuitbreaker.RequestStatus;

public interface CircuitBreakerPolicy {

    /**
     * Does the policy adopted by the Circuit Breaker allow the next request to be handled.
     * @param countryCode the country for which the request is being handled.
     * @return true if it is, otherwise false.
     */
    boolean allowsRequest(String countryCode);

    /**
     * Update the rolling counts of successes & failures maintained in order to apply the policy.
     * @param countryCode the countryCode
     * @param status      status of the last request either success or failure.
     */
    void recordStatus(String countryCode, RequestStatus status);

    /**
     * Reset the rolling window of counts for the country concerned.
     * @param countryCode the countryCode.
     */
    void clearStatus(String countryCode);

    /**
     * Report the last calculated failure level.
     * @param countryCode the countryCode
     * @return the level as when last calculated on receipt of a status update.
     */
    long getFailureLevel(String countryCode);

    /**
     * Report the current message flow rate in messages per minute.
     * @param countryCode the country code.
     * @return the flow rate based on stats for updates.
     */
    long getFlowRate(String countryCode);
}
