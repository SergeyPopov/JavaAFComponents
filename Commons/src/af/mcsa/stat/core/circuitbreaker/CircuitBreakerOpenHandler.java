package af.mcsa.stat.core.circuitbreaker;

import org.springframework.integration.Message;

public interface CircuitBreakerOpenHandler {

    /**
     * Apply some action in response to a client request when the corresponding CircuitBreaker is open.
     * @param system  - the external system for which the handler is being invoked.
     * @param target  - the target component in the messaging chain.
     * @param message - the message concerned.
     * @return optional result or may throw a CircuitBreakerOpenException
     */
    Object handle(String system, Object target, Message<?> message);

}
