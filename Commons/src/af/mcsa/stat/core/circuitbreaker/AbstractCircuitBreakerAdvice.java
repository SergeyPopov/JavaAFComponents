package af.mcsa.stat.core.circuitbreaker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.Message;
import org.springframework.integration.handler.advice.AbstractRequestHandlerAdvice;

import af.mcsa.stat.core.config.SystemConfiguration;
import af.mcsa.stat.core.exception.MCSAException;


public abstract class AbstractCircuitBreakerAdvice extends AbstractRequestHandlerAdvice {

    protected static final String COMPONENT_NAME = "circuit_breaker";

    private String remoteSystemName;

    @Autowired
    private SystemConfiguration systemConfiguration;

    protected AbstractCircuitBreakerAdvice(String remoteSystemName) {
        this.remoteSystemName = remoteSystemName;
    }


    @Override
    protected Object doInvoke(ExecutionCallback callback, Object target, Message<?> message) throws Exception {

        if (systemConfiguration.getCircuitBreakersEnabled()) {
            return invokeWithCircuitBreakerAdvice(callback, target, message);
        }

        return callback.execute();

    }

    protected abstract Object invokeWithCircuitBreakerAdvice(ExecutionCallback callback, Object target,
                                                             Message<?> message) throws Exception;


    protected Throwable findMCSAException(Exception e) {
        Throwable t = e.getCause();
        while (t != null) {
            if (t instanceof MCSAException) {
                return t;
            }
            t = t.getCause();
        }

        return e;
    }

    /**
     * Gets systemConfiguration.
     * @return Value of systemConfiguration.
     */
    protected SystemConfiguration getSystemConfiguration() {
        return systemConfiguration;
    }

    /**
     * Gets remoteSystemName.
     * @return Value of remoteSystem.
     */
    String getRemoteSystemName() {
        return remoteSystemName;
    }
}

