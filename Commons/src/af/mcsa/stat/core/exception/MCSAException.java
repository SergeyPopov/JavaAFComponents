package af.mcsa.stat.core.exception;

public class MCSAException extends RuntimeException {

    /**
     * Creates a new instance of {@link OespException}.
     * @param message the exception message
     */
    public MCSAException(String message) {
        super(message);
    }

    /**
     * Creates a new instance of {@link OespException}.
     * @param message the exception message
     * @param cause the root cause
     */
    public MCSAException(String message, Throwable cause) {
        super(message, cause);
    }

}
