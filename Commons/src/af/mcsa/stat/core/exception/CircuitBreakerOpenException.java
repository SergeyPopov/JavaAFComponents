package af.mcsa.stat.core.exception;


@SuppressWarnings("serial")
public class CircuitBreakerOpenException extends MCSAException {

    private String system;

    /**
     * Creates a new instance of {@link com.lgi.orion.oesp.exception.CircuitBreakerOpenException}.
     * @param system  the system in error
     * @param message the exception message
     */
    public CircuitBreakerOpenException(String system, String message) {
        super(message);
        this.system = system;
    }

    /**
     * Creates a new instance of {@link com.lgi.orion.oesp.exception.CircuitBreakerOpenException}.
     * @param system  the system in error
     * @param message the exception message
     * @param cause   the root cause
     */
    public CircuitBreakerOpenException(String system, String message, Throwable cause) {
        super(message, cause);
        this.system = system;
    }

    /**
     * Gets system.
     * @return Value of system.
     */
    public String getSystem() {
        return system;
    }
}
