package af.mcsa.stat.core.config;



import java.io.IOException;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

public class SystemConfiguration {


    @Autowired
    private SystemConfigurationLoader loader;

    private String environment;

    private Integer psearchMinChars;

    private Set<String> countryCodes;


    private Map<String, String> languageCodeMap;
    private Boolean jamonEnabled;
    private boolean encodeByProgramParameter;

    private boolean tpFeedCorrelationIdEnabled;

    private boolean expiredStreamArrayCheckEnabled;
    private Integer expiredStreamArrayCheckPeriod;

    // Fault Tolerance (FT)
    private boolean circuitBreakersEnabled;

    // POMG FT
    private Integer pomgCircuitBreakerTimePeriod;
    private Integer pomgCircuitBreakerPercentageThreshold;
    private Integer pomgCircuitBreakerHalfOpenDelay;



    // Metrics
    /**
     * @deprecated - should not be required.
     */
    @Deprecated
    private Integer metricsRouterPoolSize;

    private Boolean metricsEnabled;
    private Integer metricsRouterSendTimeout;
    private Integer counterMetricsQueueSize;
    private Integer counterMetricsQueuePollInterval;
    private Integer counterMetricsQueuePollSize;
    private Integer gaugeMetricsQueueSize;
    private Integer gaugeMetricsQueuePollInterval;
    private Integer gaugeMetricsQueuePollSize;
    private Integer timerMetricsQueueSize;
    private Integer timerMetricsQueuePollInterval;
    private Integer timerMetricsQueuePollSize;

    private String statsdHostAddress;
    private Integer statsdHostPort;

    /**
     * Populates the config using the SystemConfigurationLoader.
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @PostConstruct
    public void load() throws IOException {
        loader.load(this);
    }

    /**
     * Sets the environment.
     * @param environment the new environment
     */
    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    /**
     * Gets the environment.
     * @return the environment
     */
    @ManagedAttribute
    public String getEnvironment() {
        return environment;
    }

    /**
     * Sets the countryCodes.
     * @param countryCodes comma seperated list of countryCodes
     */
    @ManagedOperation
    public void setCountryCodes(String countryCodes) {
        this.countryCodes = SetUtils.csvToSet(countryCodes);
    }

    /**
     * Returns the countryCodes.
     * @return the countryCodes
     */
    @ManagedAttribute
    public Set<String> getCountryCodes() {
        return countryCodes;
    }

    /**
     * Returns the psearchMinChars.
     * @return the psearchMinChars
     */
    @ManagedAttribute
    public Integer getPsearchMinChars() {
        return psearchMinChars;
    }

    /**
     * Sets the psearchMinChars.
     * @param psearchMinChars the psearchMinChars to set
     */
    @ManagedAttribute
    public void setPsearchMinChars(Integer psearchMinChars) {
        this.psearchMinChars = psearchMinChars;
    }

    /**
     * Gets the sql ping query.
     * @return the sql ping query
     */
    @ManagedAttribute
    public String getSqlPingQuery() {
        return sqlPingQuery;
    }

    /**
     * Sets the sql ping query.
     * @param sqlPingQuery the new sql ping query
     */
    @ManagedAttribute
    public void setSqlPingQuery(String sqlPingQuery) {
        this.sqlPingQuery = sqlPingQuery;
    }

    /**
     * Gets the solrQueryString.
     * @return the solrQueryString
     */
    @ManagedAttribute
    public String getSolrQueryString() {
        return solrQueryString;
    }

    /**
     * Sets the solrQueryString.
     * @param solrQueryString the solrQueryString
     */
    @ManagedAttribute
    public void setSolrQueryString(String solrQueryString) {
        this.solrQueryString = solrQueryString;
    }

    /**
     * Returns the flywayEnabled.
     * @return the flywayEnabled
     */
    @ManagedAttribute
    public boolean isFlywayEnabled() {
        return Boolean.TRUE.equals(flywayEnabled);
    }

    /**
     * Sets the flywayEnabled.
     * @param flywayEnabled the flywayEnabled to set
     */
    @ManagedAttribute
    public void setFlywayEnabled(Boolean flywayEnabled) {
        this.flywayEnabled = flywayEnabled;
    }

    /**
     * Returns the flywayBaseDir.
     * @return the flywayBaseDir
     */
    @ManagedAttribute
    public String getFlywayBaseDir() {
        return flywayBaseDir;
    }

    /**
     * Sets the flywayBaseDir.
     * @param flywayBaseDir the flywayBaseDir to set
     */
    @ManagedAttribute
    public void setFlywayBaseDir(String flywayBaseDir) {
        this.flywayBaseDir = flywayBaseDir;
    }

    /**
     * Returns the batchEnabled.
     * @return the batchEnabled
     */
    @ManagedAttribute
    public boolean isBatchEnabled() {
        return Boolean.TRUE.equals(batchEnabled);
    }

    /**
     * Sets the batchEnabled.
     * @param batchEnabled the batchEnabled to set
     */
    @ManagedAttribute
    public void setBatchEnabled(Boolean batchEnabled) {
        this.batchEnabled = batchEnabled;
    }

    /**
     * Get the batchCleanerDaysToRetainAllStatuses.
     * @return the batchCleanerDaysToRetainAllStatuses
     */
    @ManagedAttribute
    public Integer getBatchCleanerDaysToRetainAllStatuses() {
        return batchCleanerDaysToRetainAllStatuses;
    }

    /**
     * Sets the batchCleanerDaysToRetainAllStatuses.
     * @param batchCleanerDaysToRetainAllStatuses
     *         - the number of days to retain Spring Batch data for job executions
     *         at any status.
     */
    @ManagedAttribute
    public void setBatchCleanerDaysToRetainAllStatuses(Integer batchCleanerDaysToRetainAllStatuses) {
        this.batchCleanerDaysToRetainAllStatuses = batchCleanerDaysToRetainAllStatuses;
    }

    /**
     * Get the batchCleanerDaysToRetainCompleted.
     * @return the batchCleanerDaysToRetainCompleted
     */
    @ManagedAttribute
    public Integer getBatchCleanerDaysToRetainCompleted() {
        return batchCleanerDaysToRetainCompleted;
    }

    /**
     * Sets the batchCleanerDaysToRetainCompleted.
     * @param batchCleanerDaysToRetainCompleted
     *         - the number of days to retain Spring Batch data for job executions
     *         at COMPLETED status.
     */
    @ManagedAttribute
    public void setBatchCleanerDaysToRetainCompleted(Integer batchCleanerDaysToRetainCompleted) {
        this.batchCleanerDaysToRetainCompleted = batchCleanerDaysToRetainCompleted;
    }

    /**
     * How long will Spring Batch Admin wait for running job executions to stop.
     * @return - current timeout value.
     */
    @ManagedAttribute
    public Integer getBatchJobShutdownTimeout() {
        return this.batchJobShutdownTimeout;
    }

    /**
     * * Set how long will Spring Batch Admin wait for running job executions to stop, value in ms.
     * @param batchJobShutdownTimeout - the timeout.
     */
    @ManagedAttribute
    public void setBatchJobShutdownTimeout(Integer batchJobShutdownTimeout) {
        this.batchJobShutdownTimeout = batchJobShutdownTimeout;
    }

    /**
     * Returns the jsonPrettyPrintEnabled.
     * @return the jsonPrettyPrintEnabled
     */
    @ManagedAttribute
    public boolean isJsonPrettyPrintEnabled() {
        return Boolean.TRUE.equals(jsonPrettyPrintEnabled);
    }

    /**
     * Sets the jsonPrettyPrintEnabled.
     * @param jsonPrettyPrintEnabled the jsonPrettyPrintEnabled to set
     */
    @ManagedAttribute
    public void setJsonPrettyPrintEnabled(Boolean jsonPrettyPrintEnabled) {
        this.jsonPrettyPrintEnabled = jsonPrettyPrintEnabled;
    }

    /**
     * Returns the map of 3 character to 2 character language codes.
     * @return the languageCodeMap
     */
    @ManagedAttribute
    public Map<String, String> getLanguageCodeMap() {
        return languageCodeMap;
    }

    /**
     * Sets the map of 3 character to 2 character language codes.
     * @param languageCodeMap The languageCodeMap value to set.
     */
    public void setLanguageCodeMap(Map<String, String> languageCodeMap) {
        this.languageCodeMap = languageCodeMap;
    }

    /**
     * Puts language code mapping.
     * @param iso6392Code the iso6392 code
     * @param iso6391Code the iso6391 code
     */
    @ManagedOperation
    public void putLanguageCodeMapping(String iso6392Code, String iso6391Code) {
        languageCodeMap.put(iso6392Code, iso6391Code);
    }

    /**
     * Remove language code mapping.
     * @param iso6392Code the iso6392 code
     */
    @ManagedOperation
    public void removeLanguageCodeMapping(String iso6392Code) {
        languageCodeMap.remove(iso6392Code);
    }

    /**
     * Sets the maxBatchJobLaunchDelayInMillis.
     * @param maxBatchJobLaunchDelayInMillis the maxBatchJobLaunchDelayInMillis to set
     */
    @ManagedAttribute
    public void setMaxBatchJobLaunchDelayInMillis(Integer maxBatchJobLaunchDelayInMillis) {
        this.maxBatchJobLaunchDelayInMillis = maxBatchJobLaunchDelayInMillis;
    }

    /**
     * Returns the maxBatchJobLaunchDelayInMillis.
     * @return the maxBatchJobLaunchDelayInMillis
     */
    @ManagedAttribute
    public Integer getMaxBatchJobLaunchDelayInMillis() {
        return maxBatchJobLaunchDelayInMillis;
    }

    /**
     * Sets the blackoutControlEnabled.
     * @param blackoutControlEnabled the blackoutControlEnabled to set
     */
    @ManagedAttribute
    public void setBlackoutControlEnabled(Boolean blackoutControlEnabled) {
        this.blackoutControlEnabled = blackoutControlEnabled;
    }

    /**
     * Returns the blackoutControlEnabled.
     * @return the blackoutControlEnabled
     */
    @ManagedAttribute
    public boolean isBlackoutControlEnabled() {
        return Boolean.TRUE.equals(blackoutControlEnabled);
    }

    /**
     * Sets the completedPendingUpdatesDaysToRetain.
     * @param completedPendingUpdatesDaysToRetain
     *         the completedPendingUpdatesDaysToRetain to set
     */
    @ManagedAttribute
    public void setCompletedPendingUpdatesDaysToRetain(Integer completedPendingUpdatesDaysToRetain) {
        this.completedPendingUpdatesDaysToRetain = completedPendingUpdatesDaysToRetain;
    }

    /**
     * Returns the completedPendingUpdatesDaysToRetain.
     * @return the completedPendingUpdatesDaysToRetain
     */
    @ManagedAttribute
    public Integer getCompletedPendingUpdatesDaysToRetain() {
        return completedPendingUpdatesDaysToRetain;
    }

    /**
     * Sets the completedPendingUpdatesDeleteLimit.
     * @param completedPendingUpdatesDeleteLimit
     *         the completedPendingUpdatesDeleteLimit to set
     */
    @ManagedAttribute
    public void setCompletedPendingUpdatesDeleteLimit(Integer completedPendingUpdatesDeleteLimit) {
        this.completedPendingUpdatesDeleteLimit = completedPendingUpdatesDeleteLimit;
    }

    /**
     * Returns the completedPendingUpdatesDeleteLimit.
     * @return the completedPendingUpdatesDeleteLimit
     */
    @ManagedAttribute
    public Integer getCompletedPendingUpdatesDeleteLimit() {
        return completedPendingUpdatesDeleteLimit;
    }

    /**
     * Sets the bookmarksDaysToRetain.
     * @param bookmarksDaysToRetain the bookmarksDaysToRetain to set
     */
    public void setBookmarksDaysToRetain(Integer bookmarksDaysToRetain) {
        this.bookmarksDaysToRetain = bookmarksDaysToRetain;
    }

    /**
     * Returns the bookmarksDaysToRetain.
     * @return the bookmarksDaysToRetain
     */
    public Integer getBatchBookmarksDaysToRetain() {
        return bookmarksDaysToRetain;
    }

    /**
     * Sets the bookmarksDeleteLimit.
     * @param bookmarksDeleteLimit the bookmarksDeleteLimit to set
     */
    public void setBookmarksDeleteLimit(Integer bookmarksDeleteLimit) {
        this.bookmarksDeleteLimit = bookmarksDeleteLimit;
    }

    /**
     * Returns the bookmarksDeleteLimit.
     * @return the bookmarksDeleteLimit
     */
    public Integer getBatchBookmarksDeleteLimit() {
        return bookmarksDeleteLimit;
    }

    /**
     * Returns the jamonEnabled.
     * @return the jamonEnabled
     */
    @ManagedAttribute
    public boolean isJamonEnabled() {
        return Boolean.TRUE.equals(jamonEnabled);
    }

    /**
     * Sets the jamonEnabled.
     * @param jamonEnabled the jamonEnabled to set
     */
    @ManagedAttribute
    public void setJamonEnabled(Boolean jamonEnabled) {
        this.jamonEnabled = jamonEnabled;
    }

    /**
     * Sets the encodeByProgramParameter.
     * @param encodeByProgramParameter the encodeByProgramParameter to set
     */
    @ManagedAttribute
    public void setEncodeByProgramParameter(boolean encodeByProgramParameter) {
        this.encodeByProgramParameter = encodeByProgramParameter;
    }

    /**
     * Returns the encodeByProgramParameter.
     * @return the encodeByProgramParameter
     */
    @ManagedAttribute
    public boolean getEncodeByProgramParameter() {
        return encodeByProgramParameter;
    }

    /**
     * Gets categorySearchFetchSize.
     * @return Value of categorySearchFetchSize.
     */
    @ManagedAttribute
    public Integer getCategorySearchFetchSize() {
        return categorySearchFetchSize;
    }

    /**
     * Sets new categorySearchFetchSize.
     * @param categorySearchFetchSize New value of categorySearchFetchSize.
     */
    @ManagedAttribute
    public void setCategorySearchFetchSize(Integer categorySearchFetchSize) {
        this.categorySearchFetchSize = categorySearchFetchSize;
    }

    /**
     * Sets the tpFeedCorrelationIdEnabled.
     * @param tpFeedCorrelationIdEnabled the tpFeedCorrelationIdEnabled to set
     */
    public void setTpFeedCorrelationIdEnabled(boolean tpFeedCorrelationIdEnabled) {
        this.tpFeedCorrelationIdEnabled = tpFeedCorrelationIdEnabled;
    }

    /**
     * Returns the tpFeedCorrelationIdEnabled.
     * @return the tpFeedCorrelationIdEnabled
     */
    public boolean isTpFeedCorrelationIdEnabled() {
        return tpFeedCorrelationIdEnabled;
    }

    /**
     * Returns the widevineHeartbeatLinearChangeThreshold.
     * @return the widevineHeartbeatLinearChangeThreshold
     */
    @ManagedAttribute
    public Integer getWidevineHeartbeatLinearChangeThreshold() {
        return widevineHeartbeatLinearChangeThreshold;
    }

    /**
     * Sets the widevineHeartbeatLinearChangeThreshold.
     * @param widevineHeartbeatLinearChangeThreshold
     *         the widevineHeartbeatLinearChangeThreshold to set
     */
    @ManagedAttribute
    public void setWidevineHeartbeatLinearChangeThreshold(Integer widevineHeartbeatLinearChangeThreshold) {
        this.widevineHeartbeatLinearChangeThreshold = widevineHeartbeatLinearChangeThreshold;
    }

    /**
     * Sets the expiredStreamArrayCheckEnabled.
     * @param expiredStreamArrayCheckEnabled the expiredStreamArrayCheckEnabled to set
     */
    @ManagedAttribute
    public void setExpiredStreamArrayCheckEnabled(boolean expiredStreamArrayCheckEnabled) {
        this.expiredStreamArrayCheckEnabled = expiredStreamArrayCheckEnabled;
    }

    /**
     * Returns the expiredStreamArrayCheckEnabled.
     * @return the expiredStreamArrayCheckEnabled
     */
    @ManagedAttribute
    public boolean isExpiredStreamArrayCheckEnabled() {
        return expiredStreamArrayCheckEnabled;
    }

    /**
     * Sets the expiredStreamArrayCheckPeriod.
     * @param expiredStreamArrayCheckPeriod the expiredStreamArrayCheckPeriod to set
     */
    @ManagedAttribute
    public void setExpiredStreamArrayCheckPeriod(Integer expiredStreamArrayCheckPeriod) {
        this.expiredStreamArrayCheckPeriod = expiredStreamArrayCheckPeriod;
    }

    /**
     * Returns the expiredStreamArrayCheckPeriod.
     * @return the expiredStreamArrayCheckPeriod
     */
    @ManagedAttribute
    public Integer getExpiredStreamArrayCheckPeriod() {
        return expiredStreamArrayCheckPeriod;
    }

    /**
     * Sets new pomgCircuitBreakerHalfOpenDelay.
     * @param pomgCircuitBreakerHalfOpenDelay
     *         New value of pomgCircuitBreakerHalfOpenDelay.
     */
    @ManagedAttribute
    public void setPomgCircuitBreakerHalfOpenDelay(Integer pomgCircuitBreakerHalfOpenDelay) {
        this.pomgCircuitBreakerHalfOpenDelay = pomgCircuitBreakerHalfOpenDelay;
    }

    /**
     * Sets new pomgCircuitBreakerTimePeriod.
     * @param pomgCircuitBreakerTimePeriod New value of pomgCircuitBreakerTimePeriod.
     */
    @ManagedAttribute
    public void setPomgCircuitBreakerTimePeriod(Integer pomgCircuitBreakerTimePeriod) {
        this.pomgCircuitBreakerTimePeriod = pomgCircuitBreakerTimePeriod;
    }

    /**
     * Gets pomgCircuitBreakerTimePeriod.
     * @return Value of pomgCircuitBreakerTimePeriod.
     */
    @ManagedAttribute
    public Integer getPomgCircuitBreakerTimePeriod() {
        return pomgCircuitBreakerTimePeriod;
    }

    /**
     * Gets pomgCircuitBreakerPercentageThreshold.
     * @return Value of pomgCircuitBreakerPercentageThreshold.
     */
    @ManagedAttribute
    public Integer getPomgCircuitBreakerPercentageThreshold() {
        return pomgCircuitBreakerPercentageThreshold;
    }

    /**
     * Gets pomgCircuitBreakerHalfOpenDelay.
     * @return Value of pomgCircuitBreakerHalfOpenDelay.
     */
    @ManagedAttribute
    public Integer getPomgCircuitBreakerHalfOpenDelay() {
        return pomgCircuitBreakerHalfOpenDelay;
    }

    /**
     * Sets new pomgCircuitBreakerPercentageThreshold.
     * @param pomgCircuitBreakerPercentageThreshold
     *         New value of pomgCircuitBreakerPercentageThreshold.
     */
    @ManagedAttribute
    public void setPomgCircuitBreakerPercentageThreshold(Integer pomgCircuitBreakerPercentageThreshold) {
        this.pomgCircuitBreakerPercentageThreshold = pomgCircuitBreakerPercentageThreshold;
    }

    /**
     * Gets circuitBreakersEnabled.
     * @return Value of circuitBreakersEnabled.
     */
    @ManagedAttribute
    public boolean getCircuitBreakersEnabled() {
        return circuitBreakersEnabled;
    }

    /**
     * Sets new circuitBreakersEnabled.
     * @param circuitBreakersEnabled New value of circuitBreakersEnabled.
     */
    @ManagedAttribute
    public void setCircuitBreakersEnabled(boolean circuitBreakersEnabled) {
        this.circuitBreakersEnabled = circuitBreakersEnabled;
    }


    /**
     * Gets pealCustCircuitBreakerHalfOpenDelay.
     * @return Value of pealCustCircuitBreakerHalfOpenDelay.
     */
    @ManagedAttribute
    public Integer getPealCustCircuitBreakerHalfOpenDelay() {
        return pealCustCircuitBreakerHalfOpenDelay;
    }

    /**
     * Gets pealCustCircuitBreakerTimePeriod.
     * @return Value of pealCustCircuitBreakerTimePeriod.
     */
    @ManagedAttribute
    public Integer getPealCustCircuitBreakerTimePeriod() {
        return pealCustCircuitBreakerTimePeriod;
    }

    /**
     * Sets new pealRFSCircuitBreakerPercentageThreshold.
     * @param pealRFSCircuitBreakerPercentageThreshold
     *         New value of pealRFSCircuitBreakerPercentageThreshold.
     */
    @ManagedAttribute
    public void setPealRFSCircuitBreakerPercentageThreshold(Integer pealRFSCircuitBreakerPercentageThreshold) {
        this.pealRFSCircuitBreakerPercentageThreshold = pealRFSCircuitBreakerPercentageThreshold;
    }

    /**
     * Sets new pealCustCircuitBreakerHalfOpenDelay.
     * @param pealCustCircuitBreakerHalfOpenDelay
     *         New value of pealCustCircuitBreakerHalfOpenDelay.
     */
    @ManagedAttribute
    public void setPealCustCircuitBreakerHalfOpenDelay(Integer pealCustCircuitBreakerHalfOpenDelay) {
        this.pealCustCircuitBreakerHalfOpenDelay = pealCustCircuitBreakerHalfOpenDelay;
    }

    /**
     * Gets pealCustCircuitBreakerPercentageThreshold.
     * @return Value of pealCustCircuitBreakerPercentageThreshold.
     */
    @ManagedAttribute
    public Integer getPealCustCircuitBreakerPercentageThreshold() {
        return pealCustCircuitBreakerPercentageThreshold;
    }

    /**
     * Sets new pealSSOCircuitBreakerHalfOpenDelay.
     * @param pealAuthCircuitBreakerHalfOpenDelay
     *         New value of pealSSOCircuitBreakerHalfOpenDelay.
     */
    @ManagedAttribute
    public void setPealAuthCircuitBreakerHalfOpenDelay(Integer pealAuthCircuitBreakerHalfOpenDelay) {
        this.pealAuthCircuitBreakerHalfOpenDelay = pealAuthCircuitBreakerHalfOpenDelay;
    }

    /**
     * Gets pealSSOCircuitBreakerTimePeriod.
     * @return Value of pealSSOCircuitBreakerTimePeriod.
     */
    @ManagedAttribute
    public Integer getPealAuthCircuitBreakerTimePeriod() {
        return pealAuthCircuitBreakerTimePeriod;
    }

    /**
     * Gets pealRFSCircuitBreakerPercentageThreshold.
     * @return Value of pealRFSCircuitBreakerPercentageThreshold.
     */
    @ManagedAttribute
    public Integer getPealRFSCircuitBreakerPercentageThreshold() {
        return pealRFSCircuitBreakerPercentageThreshold;
    }

    /**
     * Sets new pealRFSCircuitBreakerTimePeriod.
     * @param pealRFSCircuitBreakerTimePeriod
     *         New value of pealRFSCircuitBreakerTimePeriod.
     */
    @ManagedAttribute
    public void setPealRFSCircuitBreakerTimePeriod(Integer pealRFSCircuitBreakerTimePeriod) {
        this.pealRFSCircuitBreakerTimePeriod = pealRFSCircuitBreakerTimePeriod;
    }

    /**
     * Sets new pealCustCircuitBreakerPercentageThreshold.
     * @param pealCustCircuitBreakerPercentageThreshold
     *         New value of pealCustCircuitBreakerPercentageThreshold.
     */
    @ManagedAttribute
    public void setPealCustCircuitBreakerPercentageThreshold(Integer pealCustCircuitBreakerPercentageThreshold) {
        this.pealCustCircuitBreakerPercentageThreshold = pealCustCircuitBreakerPercentageThreshold;
    }

    /**
     * Sets new pealSSOCircuitBreakerPercentageThreshold.
     * @param pealAuthCircuitBreakerPercentageThreshold
     *         New value of pealSSOCircuitBreakerPercentageThreshold.
     */
    @ManagedAttribute
    public void setPealAuthCircuitBreakerPercentageThreshold(Integer pealAuthCircuitBreakerPercentageThreshold) {
        this.pealAuthCircuitBreakerPercentageThreshold = pealAuthCircuitBreakerPercentageThreshold;
    }

    /**
     * Gets pealRFSCircuitBreakerHalfOpenDelay.
     * @return Value of pealRFSCircuitBreakerHalfOpenDelay.
     */
    @ManagedAttribute
    public Integer getPealRFSCircuitBreakerHalfOpenDelay() {
        return pealRFSCircuitBreakerHalfOpenDelay;
    }

    /**
     * Gets pealSSOCircuitBreakerPercentageThreshold.
     * @return Value of pealSSOCircuitBreakerPercentageThreshold.
     */
    @ManagedAttribute
    public Integer getPealAuthCircuitBreakerPercentageThreshold() {
        return pealAuthCircuitBreakerPercentageThreshold;
    }

    /**
     * Gets pealRFSCircuitBreakerTimePeriod.
     * @return Value of pealRFSCircuitBreakerTimePeriod.
     */
    @ManagedAttribute
    public Integer getPealRFSCircuitBreakerTimePeriod() {
        return pealRFSCircuitBreakerTimePeriod;
    }

    /**
     * Sets new pealCustCircuitBreakerTimePeriod.
     * @param pealCustCircuitBreakerTimePeriod
     *         New value of pealCustCircuitBreakerTimePeriod.
     */
    @ManagedAttribute
    public void setPealCustCircuitBreakerTimePeriod(Integer pealCustCircuitBreakerTimePeriod) {
        this.pealCustCircuitBreakerTimePeriod = pealCustCircuitBreakerTimePeriod;
    }

    /**
     * Sets new pealSSOCircuitBreakerTimePeriod.
     * @param pealAuthCircuitBreakerTimePeriod
     *         New value of pealSSOCircuitBreakerTimePeriod.
     */
    @ManagedAttribute
    public void setPealAuthCircuitBreakerTimePeriod(Integer pealAuthCircuitBreakerTimePeriod) {
        this.pealAuthCircuitBreakerTimePeriod = pealAuthCircuitBreakerTimePeriod;
    }

    /**
     * Sets new pealRFSCircuitBreakerHalfOpenDelay.
     * @param pealRFSCircuitBreakerHalfOpenDelay
     *         New value of pealRFSCircuitBreakerHalfOpenDelay.
     */
    @ManagedAttribute
    public void setPealRFSCircuitBreakerHalfOpenDelay(Integer pealRFSCircuitBreakerHalfOpenDelay) {
        this.pealRFSCircuitBreakerHalfOpenDelay = pealRFSCircuitBreakerHalfOpenDelay;
    }

    /**
     * Gets pealSSOCircuitBreakerHalfOpenDelay.
     * @return Value of pealSSOCircuitBreakerHalfOpenDelay.
     */
    @ManagedAttribute
    public Integer getPealAuthCircuitBreakerHalfOpenDelay() {
        return pealAuthCircuitBreakerHalfOpenDelay;
    }


    /**
     * Gets solrCircuitBreakerPercentageThreshold.
     * @return Value of solrCircuitBreakerPercentageThreshold.
     */
    @ManagedAttribute
    public Integer getSolrCircuitBreakerPercentageThreshold() {
        return solrCircuitBreakerPercentageThreshold;
    }

    /**
     * Gets solrCircuitBreakerHalfOpenDelay.
     * @return Value of solrCircuitBreakerHalfOpenDelay.
     */
    @ManagedAttribute
    public Integer getSolrCircuitBreakerHalfOpenDelay() {
        return solrCircuitBreakerHalfOpenDelay;
    }



    /**
     * Returns the metricsEnabled.
     * @return the metricsEnabled
     */
    @ManagedAttribute
    public Boolean getMetricsEnabled() {
        return metricsEnabled;
    }

    /**
     * Sets the metricsEnabled.
     * @param metricsEnabled the metricsEnabled to set
     */
    @ManagedAttribute
    public void setMetricsEnabled(Boolean metricsEnabled) {
        this.metricsEnabled = metricsEnabled;
    }

    /**
     * Gets timerMetricsQueuePollInterval.
     * @return Value of timerMetricsQueuePollInterval.
     */
    @ManagedAttribute
    public Integer getTimerMetricsQueuePollInterval() {
        return timerMetricsQueuePollInterval;
    }

    /**
     * Sets new counterMetricsQueueSize.
     * @param counterMetricsQueueSize New value of counterMetricsQueueSize.
     */
    @ManagedAttribute
    public void setCounterMetricsQueueSize(Integer counterMetricsQueueSize) {
        this.counterMetricsQueueSize = counterMetricsQueueSize;
    }

    /**
     * Sets new timerMetricsQueuePollInterval.
     * @param timerMetricsQueuePollInterval New value of timerMetricsQueuePollInterval.
     */
    @ManagedAttribute
    public void setTimerMetricsQueuePollInterval(Integer timerMetricsQueuePollInterval) {
        this.timerMetricsQueuePollInterval = timerMetricsQueuePollInterval;
    }

    /**
     * Gets gaugeMetricsQueuePollInterval.
     * @return Value of gaugeMetricsQueuePollInterval.
     */
    @ManagedAttribute
    public Integer getGaugeMetricsQueuePollInterval() {
        return gaugeMetricsQueuePollInterval;
    }

    /**
     * Gets counterMetricsQueuePollInterval.
     * @return Value of counterMetricsQueuePollInterval.
     */
    @ManagedAttribute
    public Integer getCounterMetricsQueuePollInterval() {
        return counterMetricsQueuePollInterval;
    }

    /**
     * Sets new gaugeMetricsQueuePollInterval.
     * @param gaugeMetricsQueuePollInterval New value of gaugeMetricsQueuePollInterval.
     */
    @ManagedAttribute
    public void setGaugeMetricsQueuePollInterval(Integer gaugeMetricsQueuePollInterval) {
        this.gaugeMetricsQueuePollInterval = gaugeMetricsQueuePollInterval;
    }

    /**
     * Gets timerMetricsQueueSize.
     * @return Value of timerMetricsQueueSize.
     */
    @ManagedAttribute
    public Integer getTimerMetricsQueueSize() {
        return timerMetricsQueueSize;
    }

    /**
     * Sets new counterMetricsQueuePollInterval.
     * @param counterMetricsQueuePollInterval
     *         New value of counterMetricsQueuePollInterval.
     */
    @ManagedAttribute
    public void setCounterMetricsQueuePollInterval(Integer counterMetricsQueuePollInterval) {
        this.counterMetricsQueuePollInterval = counterMetricsQueuePollInterval;
    }

    /**
     * Sets new timerMetricsQueueSize.
     * @param timerMetricsQueueSize New value of timerMetricsQueueSize.
     */
    @ManagedAttribute
    public void setTimerMetricsQueueSize(Integer timerMetricsQueueSize) {
        this.timerMetricsQueueSize = timerMetricsQueueSize;
    }

    /**
     * Gets gaugeMetricsQueueSize.
     * @return Value of gaugeMetricsQueueSize.
     */
    @ManagedAttribute
    public Integer getGaugeMetricsQueueSize() {
        return gaugeMetricsQueueSize;
    }

    /**
     * Gets counterMetricsQueueSize.
     * @return Value of counterMetricsQueueSize.
     */
    @ManagedAttribute
    public Integer getCounterMetricsQueueSize() {
        return counterMetricsQueueSize;
    }

    /**
     * Sets new gaugeMetricsQueueSize.
     * @param gaugeMetricsQueueSize New value of gaugeMetricsQueueSize.
     */
    @ManagedAttribute
    public void setGaugeMetricsQueueSize(Integer gaugeMetricsQueueSize) {
        this.gaugeMetricsQueueSize = gaugeMetricsQueueSize;
    }

    /**
     * Sets new statsdHostPort.
     * @param statsdHostPort New value of statsdHostPort.
     */
    @ManagedAttribute
    public void setStatsdHostPort(Integer statsdHostPort) {
        this.statsdHostPort = statsdHostPort;
    }

    /**
     * Gets statsdHostAddress.
     * @return Value of statsdHostAddress.
     */
    @ManagedAttribute
    public String getStatsdHostAddress() {
        return statsdHostAddress;
    }

    /**
     * Sets new statsdHostAddress.
     * @param statsdHostAddress New value of statsdHostAddress.
     */
    @ManagedAttribute
    public void setStatsdHostAddress(String statsdHostAddress) {
        this.statsdHostAddress = statsdHostAddress;
    }

    /**
     * Gets statsdHostPort.
     * @return Value of statsdHostPort.
     */
    @ManagedAttribute
    public Integer getStatsdHostPort() {
        return statsdHostPort;
    }

    /**
     * Gets gaugeMetricsQueuePollSize.
     * @return Value of gaugeMetricsQueuePollSize.
     */
    @ManagedAttribute
    public Integer getGaugeMetricsQueuePollSize() {
        return gaugeMetricsQueuePollSize;
    }

    /**
     * Gets timerMetricsQueuePollSize.
     * @return Value of timerMetricsQueuePollSize.
     */
    @ManagedAttribute
    public Integer getTimerMetricsQueuePollSize() {
        return timerMetricsQueuePollSize;
    }

    /**
     * Sets new timerMetricsQueuePollSize.
     * @param timerMetricsQueuePollSize New value of timerMetricsQueuePollSize.
     */
    @ManagedAttribute
    public void setTimerMetricsQueuePollSize(Integer timerMetricsQueuePollSize) {
        this.timerMetricsQueuePollSize = timerMetricsQueuePollSize;
    }

    /**
     * Sets new counterMetricsQueuePollSize.
     * @param counterMetricsQueuePollSize New value of counterMetricsQueuePollSize.
     */
    @ManagedAttribute
    public void setCounterMetricsQueuePollSize(Integer counterMetricsQueuePollSize) {
        this.counterMetricsQueuePollSize = counterMetricsQueuePollSize;
    }

    /**
     * Sets new gaugeMetricsQueuePollSize.
     * @param gaugeMetricsQueuePollSize New value of gaugeMetricsQueuePollSize.
     */
    @ManagedAttribute
    public void setGaugeMetricsQueuePollSize(Integer gaugeMetricsQueuePollSize) {
        this.gaugeMetricsQueuePollSize = gaugeMetricsQueuePollSize;
    }

    /**
     * Gets counterMetricsQueuePollSize.
     * @return Value of counterMetricsQueuePollSize.
     */
    @ManagedAttribute
    public Integer getCounterMetricsQueuePollSize() {
        return counterMetricsQueuePollSize;
    }

    /**
     * Sets new metricsRouterPoolSize.
     * @param metricsRouterPoolSize New value of metricsRouterPoolSize.
     * @deprecated - this property should not be required.
     */
    @Deprecated
    @ManagedAttribute
    public void setMetricsRouterPoolSize(Integer metricsRouterPoolSize) {
        this.metricsRouterPoolSize = metricsRouterPoolSize;
    }

    /**
     * Gets metricsRouterPoolSize.
     * @return Value of metricsRouterPoolSize.
     * @deprecated - this property should not be required.
     */
    @ManagedAttribute
    @Deprecated
    public Integer getMetricsRouterPoolSize() {
        return metricsRouterPoolSize;
    }

    /**
     * Sets new metricsRouterSendTimeout.
     * @param metricsRouterSendTimeout New value of metricsRouterSendTimeout.
     */
    @ManagedAttribute
    public void setMetricsRouterSendTimeout(Integer metricsRouterSendTimeout) {
        this.metricsRouterSendTimeout = metricsRouterSendTimeout;
    }

    /**
     * Gets metricsRouterSendTimeout.
     * @return Value of metricsRouterSendTimeout.
     */
    @ManagedAttribute
    public Integer getMetricsRouterSendTimeout() {
        return metricsRouterSendTimeout;
    }
}
