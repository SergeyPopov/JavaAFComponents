package af.mcsa.commons.gateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.CustomPayload;
import org.springframework.integration.Message;
import org.springframework.integration.MessageHeaders;
import org.springframework.integration.MessagingException;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.integration.support.MessageBuilder;

import af.mcsa.stat.core.config.CountryConfiguration;
import af.mcsa.stat.core.config.CountryConfigurationRegistry;
import af.mcsa.stat.core.exception.MCSAException;
import af.mcsa.stat.core.exception.UnexpectedTypeException;
import f.mcsa.stat.core.auth.Session;

public class BaseMessagingGatewayImpl {
 
	
	private static final String MSG_HEADER_TOKEN = "token";
    protected static final String MSG_HEADER_CUSTOMER_ID = "customerId";
    protected static final String MSG_HEADER_CREDENTIAL_TYPE = "credentialType";
    private static final String MSG_HEADER_COUNTRY_CODE = "countryCode";
    private static final String MSG_HEADER_LANGUAGE_CODE = "languageCode";
    private static final String MSG_HEADER_DEVICE_CODE = "deviceCode";
    private static final String MSG_HEADER_LOCATION_CODE = "locationCode";
    private static final String MSG_PARENTAL_PIN_VERIFIED = "parentalPinVerified";
    private static final String MSG_ADULT_PIN_VERIFIED = "adultPinVerified";
    private static final String MSG_HEADER_CLIENT_ID = "clientId";

    @Autowired
    private CountryConfigurationRegistry countryConfigurationRegistry;

    @Autowired
    private MessagingTemplate messagingTemplate;

    protected final MessageBuilder<CustomPayload> messageBuilderWithNoPayloadAndNoHeaders(String clientId) {
        MessageBuilder<CustomPayload> builder = MessageBuilder.withPayload(CustomPayload.NULL);
        setMessageHeaderIfNotNull(builder, MSG_HEADER_CLIENT_ID, clientId);
        return builder;
    }

    protected final MessageBuilder<?> messageBuilderWithNoPayloadAndStandardHeaders(String countryCode,
                                                                                    String languageCode,
                                                                                    String deviceCode,
                                                                                    String locationCode,
                                                                                    String clientId) {
        return messageBuilderWithPayloadAndStandardHeaders(countryCode, languageCode, deviceCode, locationCode,
                CustomPayload.NULL, clientId);
    }

    protected final <T> MessageBuilder<T> messageBuilderWithPayloadAndStandardHeaders(String countryCode,
                                                                                      String languageCode,
                                                                                      String deviceCode,
                                                                                      String locationCode, T payload,
                                                                                      String clientId) {
        MessageBuilder<T> builder = MessageBuilder.withPayload(payload);
        setMessageHeaderIfNotNull(builder, MSG_HEADER_COUNTRY_CODE, countryCode);
        setMessageHeaderIfNotNull(builder, MSG_HEADER_LANGUAGE_CODE, languageCode);
        setMessageHeaderIfNotNull(builder, MSG_HEADER_DEVICE_CODE, deviceCode);
        setMessageHeaderIfNotNull(builder, MSG_HEADER_LOCATION_CODE, locationCode);
        setMessageHeaderIfNotNull(builder, MSG_HEADER_CLIENT_ID, clientId);
        return builder;
    }

    protected final MessageBuilder<?> messageBuilderWithNoPayloadAndStandardHeaders(Session session,
                                                                                    String clientId) {
        return messageBuilderWithPayloadAndStandardHeaders(session, CustomPayload.NULL, clientId);
    }

    protected final MessageBuilder<?> messageBuilderWithNoPayloadAndStandardHeaders(MessageHeaders headers,
                                                                                    String clientId) {
        return messageBuilderWithPayloadAndStandardHeaders(headers, CustomPayload.NULL, clientId);
    }

    protected final <T> MessageBuilder<T> messageBuilderWithPayloadAndStandardHeaders(Session session, T payload,
                                                                                      String clientId) {
        MessageBuilder<T> builder = MessageBuilder.withPayload(payload);
        setStandardMessageHeaders(session, builder);
        setMessageHeaderIfNotNull(builder, MSG_HEADER_CLIENT_ID, clientId);
        return builder;
    }

    protected final <T> MessageBuilder<T>
    messageBuilderWithPayloadAndStandardHeaders(MessageHeaders headers, T payload, String clientId) {
        MessageBuilder<T> builder = MessageBuilder.withPayload(payload);
        setStandardMessageHeaders(headers, builder);
        setMessageHeaderIfNotNull(builder, MSG_HEADER_CLIENT_ID, clientId);
        return builder;
    }

    private void setStandardMessageHeaders(Session session, MessageBuilder<?> builder) {
        setMessageHeaderIfNotNull(builder, MSG_HEADER_COUNTRY_CODE, session.getCountryCode());
        setMessageHeaderIfNotNull(builder, MSG_HEADER_LANGUAGE_CODE, session.getLanguageCode());
        setMessageHeaderIfNotNull(builder, MSG_HEADER_DEVICE_CODE, session.getDeviceCode());
        setMessageHeaderIfNotNull(builder, MSG_HEADER_LOCATION_CODE, session.getLocationId());
        setMessageHeaderIfNotNull(builder, MSG_HEADER_TOKEN, session.getOespToken());
        setMessageHeaderIfNotNull(builder, MSG_HEADER_CUSTOMER_ID,
                session.isCustomer() ? session.getCustomer().getId() : "");
        setMessageHeaderIfNotNull(builder, MSG_PARENTAL_PIN_VERIFIED, session.getParentalPinVerified());
        setMessageHeaderIfNotNull(builder, MSG_ADULT_PIN_VERIFIED, session.getAdultPinVerified());
    }

    private void setStandardMessageHeaders(MessageHeaders headers, MessageBuilder<?> builder) {
        builder.copyHeaders(headers);
    }

    private void setMessageHeaderIfNotNull(MessageBuilder<?> builder, String headerName, Object headerValue) {
        if (headerValue != null) {
            builder.setHeader(headerName, headerValue);
        }
    }

    protected final void send(String channelName, Message<?> requestMessage) {
        try {
            messagingTemplate.send(channelName, requestMessage);
        } catch (MessagingException e) {
            convertAndThrow(e);
        }
    }

    protected final <T> T sendAndReceive(String channelName, Message<?> requestMessage, Class<T> payloadType) {
        Message<?> replyMessage = null;
        try {
            replyMessage = messagingTemplate.sendAndReceive(channelName, requestMessage);
        } catch (MessagingException e) {
            convertAndThrow(e);
        }
        return processReply(payloadType, replyMessage);
    }

    @SuppressWarnings("unchecked")
    private <T> T processReply(Class<T> payloadType, Message<?> replyMessage) {
        if (replyMessage == null) {
            throw new UnexpectedTypeException("replyMessage", replyMessage);
        }

        Object payload = replyMessage.getPayload();

        if (payload == null) {
            throw new UnexpectedTypeException("replyMessage.payload", payload);
        }
        if (payload == CustomPayload.NULL) {
            return null;
        }
        if (payload instanceof MessagingException) {
            convertAndThrow((MessagingException) payload);
        }
        if (!payloadType.isAssignableFrom(payload.getClass())) {
            throw new UnexpectedTypeException("replyMessage.payload", payload);
        }

        return (T) replyMessage.getPayload();
    }

    /**
     * Convert a Spring Integration MessagingException into a more expressive type.
     * <p/>
     * If a MessagingException carries an OESP exception then we extract the OESP exception and re-throw it so that the
     * ExceptionMappers can intercept it. If it represents a "response-required" violation, we convert to a more
     * expressive type. Anything else is just re-thrown as is.
     * @param e An exception to convert into more specific type where possible.
     * @throws OespException or MessagingException
     */
    private void convertAndThrow(MessagingException e) {
        if (e.getCause() instanceof OespException) {
            throw (OespException) e.getCause();
        }
        throw e;
    }

    protected CountryConfiguration getCountryConfig(String countryCode) {
        return countryConfigurationRegistry.lookup(countryCode);
    }
}

