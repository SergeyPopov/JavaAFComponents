CREATE TABLE statk_parsetrace(
 tracelog_id INT(4) AUTO_INCREMENT PRIMARY KEY,
 foldername        VARCHAR(150),
 lastprocfilename  VARCHAR(150),
 lastproc_status   INT(1),
 statustext        TEXT CHARACTER SET utf8,
 parsetimestamp    TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
)ENGINE = InnoDB;

